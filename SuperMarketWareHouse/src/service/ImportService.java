package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import dao.GoodsReceiptNoteDAO;
import model.CategoryStatistics;
import model.ImportModels;
import pojo.Detailgoodsreceiptnote;
import pojo.Goodsorder;
import pojo.Goodsreceiptnote;

public class ImportService {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private GoodsOrderService goodsOrderService;
	
	public ImportModels loadImportModels(int goodsOrderId){
		ImportModels importModels = new ImportModels();
		if(goodsOrderId > 0){
			importModels.setGoodsOrderId(goodsOrderId);
			importModels.setListProduct(productService.getListProductInGoodsOrder(goodsOrderId));
		}
		
		Goodsorder goodsOrder = goodsOrderService.getInforGoodsOrder(goodsOrderId);
		if(goodsOrder != null){
			importModels.setNameProducer(goodsOrder.getProducers().getName());
			importModels.setAddress(goodsOrder.getProducers().getAddress());
			importModels.setPhoneNumber(goodsOrder.getProducers().getPhoneNumber());
		}
		
		return importModels;
	}
	
	public Boolean addGoodsReciptNote(ImportModels importModels, 
			long totalMoney){
		return GoodsReceiptNoteDAO.addGoodsReciptNote(importModels, totalMoney);
	}
	
	public List<Goodsreceiptnote> getListGoodsReceiptNote(){
		return GoodsReceiptNoteDAO.getListGoodsReceiptNote();
	}
	
	public List<Detailgoodsreceiptnote> getDetailGoodsReceiptNoteWithId(int id){
		return GoodsReceiptNoteDAO.getDetailGoodsReceiptNoteWithId(id);
	}
	
	public List<Integer> getArrayYear(){
		return GoodsReceiptNoteDAO.getArrayYear();
	}
	
	public List<CategoryStatistics> getGoodsReceipteNoteStaistics(int yearChoose,
			int monthChoose){
		return GoodsReceiptNoteDAO.getGoodsReceipteNoteStaistics(yearChoose, monthChoose);
	}
	
	public Goodsreceiptnote getGoodsReceiptNote(int id){
		return GoodsReceiptNoteDAO.getGoodsReceiptNote(id);
	}
}
