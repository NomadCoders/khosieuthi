package service;

import java.util.List;


import dao.CategoryProductDAO;
import model.CategoryStatistics;
import pojo.Categoryproducts;

public class CategoryService {
	
	public List<Categoryproducts> loadListCategory(){
		return CategoryProductDAO.loadListCategory();
	}
	
	public List<Categoryproducts> loadListForComboBox(Boolean chooseMode){
		return CategoryProductDAO.loadListForComboBox(chooseMode);
	}
	
	public List<Categoryproducts> getListCategoryWithProducer(int producerId){
		return CategoryProductDAO.getListCategoryWithProducer(producerId);
	}
	
	public long getTotalNumberCategory(){
		return CategoryProductDAO.getTotalNumberCategory();
	}
	
	public List<CategoryStatistics> getTotalNumberProductWithEachCategory(){
		return CategoryProductDAO.getTotalNumberProductWithEachCategory();
	}
}
