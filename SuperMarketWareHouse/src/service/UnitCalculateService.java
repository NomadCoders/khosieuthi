package service;

import java.util.List;

import dao.UnitCalculateDAO;
import pojo.Unitcalculate;

public class UnitCalculateService {
	
	public List<Unitcalculate> loadListUnitCalculateForComboBox(){
		return UnitCalculateDAO.loadListUnitCalculateForComboBox();
	}
}
