package service;

import java.util.List;

import dao.GoodsOrderDAO;
import model.GoodsOrderModels;
import pojo.Accounts;
import pojo.Detailgoodorder;
import pojo.Goodsorder;

public class GoodsOrderService {
	
	public List<Goodsorder> listGoodOrder(){
		return GoodsOrderDAO.listGoodOrder();
	}
	
	public Goodsorder getInforGoodsOrder(int goodsOrderId){
		return GoodsOrderDAO.getInforGoodsOrder(goodsOrderId);
	}
	
	public Boolean addGoodsOrder(GoodsOrderModels goodsOrderModels,
			long totalMoney, Accounts account){
		return GoodsOrderDAO.addGoodsOrder(goodsOrderModels, totalMoney, account);
	}
	
	public List<Goodsorder> getListGoodsOrderIsDone(){
		return GoodsOrderDAO.getListGoodsOrderIsDone();
	}
	
	public List<Detailgoodorder> getListDetailGoodsOrderWithId(int id){
		return GoodsOrderDAO.getListDetailGoodsOrderWithId(id);
	}
}
