package service;

import java.util.ArrayList;
import java.util.List;

import dao.ProductDAO;
import model.ProductModels;
import pojo.Products;
import pojo.Viewproducts;

public class ProductService {
	
	public List<Viewproducts> getListProducts(int categoryId, int typeId){
		return ProductDAO.getListProducts(categoryId, typeId);
	}
	
	public List<ProductModels> getListForComboBox(int categoryId, int typeId){
		List<ProductModels> listProduct = new ArrayList<ProductModels>();
		List<Viewproducts> list =  ProductDAO.getListForComboBox(categoryId, typeId);
		
		if(list.size() > 0){
			for(Viewproducts product : list){
				ProductModels productModels = new ProductModels(
						product.getId().getProductId(),
						product.getId().getName(),
						product.getId().getPrice(),
						product.getId().getNameUnitCalculate(),
						0, 0);
				listProduct.add(productModels);
			}
		}
		
		
		return listProduct;
	}
	
	public Products getProductById(int productId){
		return ProductDAO.getProductById(productId);
	}
	
	public Boolean updateProduct(Products product){
		return ProductDAO.updateProduct(product);
	}
	
	public List<ProductModels> getListProductInGoodsOrder(int goodsOrderId){
		return ProductDAO.getListProductInGoodsOrder(goodsOrderId);
	}
	
	public boolean addProduct(Products product){
		return ProductDAO.addProduct(product);
	}
	
	public List<ProductModels> getListProductsWithProducer(int categoryId, 
			int typeId, int producerId){
		
		List<ProductModels> listProduct = new ArrayList<ProductModels>();
		List<Viewproducts> list =  ProductDAO.getListProductsWithProducer(
				categoryId, typeId, producerId);
		
		if(list.size() > 0){
			for(Viewproducts product : list){
				ProductModels productModels = new ProductModels(
						product.getId().getProductId(),
						product.getId().getName(),
						product.getId().getPrice(),
						product.getId().getNameUnitCalculate(),
						0, 0);
				listProduct.add(productModels);
			}
		}
		return listProduct;
	}
	
	public long getTotalNumberProduct(){
		return ProductDAO.getTotalNumberProduct();
	}
	
	public Boolean deleteProduct(int productId){
		return ProductDAO.deleteProduct(productId);
	}
	
	public List<Viewproducts> getListProductWithKeySeach(String keySearch){
		return ProductDAO.getListProductWithKeySeach(keySearch);
	}
}
