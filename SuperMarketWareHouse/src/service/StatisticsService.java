package service;

import org.springframework.beans.factory.annotation.Autowired;

import model.SuperMarketWareHouseStatistics;

public class StatisticsService {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private TypeProductService typeProductService;
	
	@Autowired
	private AccountService accountService;
	
	/**
	 * Hàm load SuperMarketWareHouseStatistics cho các thông kê kho
	 * @return
	 */
	public SuperMarketWareHouseStatistics getListStatistics(){
		SuperMarketWareHouseStatistics houseStatistics = new SuperMarketWareHouseStatistics();
		//Load tổng số lượng sản phẩm, danh mục, nhóm hàng và tài khoản
		//ở trang web
		houseStatistics.setTotalNumberProducts(productService.getTotalNumberProduct());
		houseStatistics.setTotalNumberCategorys(categoryService.getTotalNumberCategory());
		houseStatistics.setTotalNumberAccounts(accountService.getTotalNumberAccount());
		houseStatistics.setTotalNumberTypePorducts(typeProductService.getTotalNumberTypeProducts());
		//Load tổng số lượng sản phẩm tương ứng với từng danh mục
		houseStatistics.setListCategories(categoryService
				.getTotalNumberProductWithEachCategory());
		
		//Load tổng số lượng sản phẩm tương ứng với từng nhóm hàng của từng
		//danh mục
		for(int i = 0; i < houseStatistics.getListCategories().size(); i++){
			//Lấy id danh mục
			int categoryId = houseStatistics.getListCategories().get(i)
					.getCategroyId();
			//Load list nhóm hàng với tổng số lượng sản phẩm tương ứng
			houseStatistics.getListCategories().get(i)
			.setListTypeProducts(typeProductService
					.getTotalNumberProductInType(categoryId));
		}
		
		return houseStatistics;
	}
}
