package service;

import java.util.List;

import dao.GoodsIssuedNoteDAO;
import model.CategoryStatistics;
import model.ExportModels;
import pojo.Accounts;
import pojo.Detailgoodsissuednote;
import pojo.Goodsissuednote;

public class ExportService {
	public List<Goodsissuednote> getListIssuedNote(){
		return GoodsIssuedNoteDAO.getListIssuedNote();
	}
	
	public int addGoodsIssuedNote(ExportModels exportModels, 
			long totalMoney, Accounts account){
		return GoodsIssuedNoteDAO.addGoodsIssuedNote(exportModels, 
				totalMoney, account);
	}
	
	public List<Integer> getArrayYear(){
		return GoodsIssuedNoteDAO.getArrayYear();
	}
	
	public List<CategoryStatistics> getGoodsIssuedeNoteStaistics(int yearChoose,
			int monthChoose){
		return GoodsIssuedNoteDAO.getGoodsIssuedeNoteStaistics(yearChoose, monthChoose);
	}
	
	public Goodsissuednote getGoodsIssuedNote(int id){
		return GoodsIssuedNoteDAO.getGoodsIssuedNote(id);
	}
	
	public List<Detailgoodsissuednote> getDetailGoodsIssuedNoteWithId(int id){
		return GoodsIssuedNoteDAO.getDetailGoodsIssuedNoteWithId(id);
	}
}
