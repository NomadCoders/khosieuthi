package service;

import java.util.List;

import dao.ProducerDAO;
import pojo.Producers;

public class ProducerService {
	
	public List<Producers> loadListForComboBox(){
		return ProducerDAO.loadListForComboBox();
	}
	
	public Producers getInforInProducer(int producerId){
		return ProducerDAO.getInforInProducer(producerId);
	}
}
