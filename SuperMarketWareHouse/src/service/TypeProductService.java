package service;

import java.util.List;

import dao.TypeProductDAO;
import model.TypeProductStatistics;
import pojo.Typeproduct;

public class TypeProductService {
	
	public List<Typeproduct> loadListTypeProductByIdForComboBox(
			int categoryId, Boolean chooseMode){
		return TypeProductDAO.loadListTypeProductByIdForComboBox(
				categoryId, chooseMode);
	}
	
	public List<Typeproduct> loadListTypeWithProducer(int categoryId
			, int producerId){
		return TypeProductDAO.loadListTypeWithProducer(categoryId, producerId);
	}
	
	public long getTotalNumberTypeProducts(){
		return TypeProductDAO.getTotalNumberTypeProducts();
	}
	
	public List<TypeProductStatistics> getTotalNumberProductInType(
			int categoryId){
		return TypeProductDAO.getTotalNumberProductInType(categoryId);
	}
}
