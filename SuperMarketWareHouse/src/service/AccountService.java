package service;

import java.util.List;

import dao.AccountDAO;
import model.ChangePassword;
import model.RegisterModels;
import pojo.Accounts;
import utils.ManagerLogin;
import utils.WebUtils;

public class AccountService {
	
	public Boolean addAccount(RegisterModels registerUser){
		return AccountDAO.addAccount(registerUser);
	}
	
	public Accounts Login(String nameLogin, String password){
		return AccountDAO.Login(nameLogin, password);
	}
	
	public long getTotalNumberAccount(){
		return AccountDAO.getTotalNumberAccount();
	}
	
	public Boolean updateAccountInfor(Accounts account){
		//Cập nhập id và password cho object account
		account = ManagerLogin.updateInforForAccount(account);
		if(AccountDAO.updateAccountInfor(account)){
			//Cập nhập thành công update thông tin account vào session
			ManagerLogin.updateAccountToSession(account);
			return true;
		}
		return false;
	}
	
	/**
	 * Hàm cập nhập thông tin tài khoản với quyền admin
	 * @param accountUpdate
	 * @return
	 */
	public Boolean updateAccountInforWithAdmin(Accounts accountUpdate){
		Accounts account = AccountDAO.getAccountById(accountUpdate.getId());
		accountUpdate.setPassword(account.getPassword());
		accountUpdate.setIsDelete(false);
		
		return AccountDAO.updateAccountInfor(accountUpdate);
	}
	
	public List<Accounts> getListAccount(){
		return AccountDAO.getListAccount();
	}
	
	public Accounts getAccountById(int id){
		return AccountDAO.getAccountById(id);
	}
	
	public Boolean deleteAccount(int id){
		return AccountDAO.deleteAccount(id);
	}
	
	/**
	 * Hàm thay đổi password cho user
	 * @param changePassowrd
	 * @param userName
	 * @return thành công trả về true, thất bại trả về false
	 */
	public Boolean changePassowrd(ChangePassword changePassowrd,
			 String userName){
		 if(AccountDAO.changePassowrd(changePassowrd, userName)){
			 Accounts account = ManagerLogin.getAccountInLogin();
			 account.setPassword(WebUtils.cryptWithMD5(
					 changePassowrd.getNewPassword()));
			 account.setIsDelete(false);
			 ManagerLogin.updateAccountToSession(account);
			 
			 return true;
		 }
		 
		 return false;
	}
}
