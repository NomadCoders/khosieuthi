package validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class PhoneValidator implements ConstraintValidator<Phone, String>{

	@Override
	public void initialize(Phone arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isValid(String arg0, ConstraintValidatorContext arg1) {
		// TODO Auto-generated method stub
		if(arg0 == null){
			return false;
		}
		
		//Số điện thoại là số và có 11 chữ số và có số 0 ở đầu
		if(arg0.matches("0\\d{9}")){
			return true;
		}
		else{
			//Số điện thoại mở rộng, thêm 3-5 số (123-456-1235 ext123)
			if(arg0.matches("\\d{3}-\\d{3,4}-\\d{4}\\s(x|ext)\\d{3,5}")){
				return true;
			}
			else{
				//Số điện thoại bàn có mã vùng (08) 3854 8900 
				if(arg0.matches("[(]?\\d{2,4}[)]?[-\\s]?\\d{3,4}[-\\s\\.]?\\d{3,4}")){
					return true;
				}
			}
			
			return false;
		}	
	}

}
