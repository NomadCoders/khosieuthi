package validator;


import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import dao.AccountDAO;
import model.RegisterModels;

public class UserValidator implements Validator{
	
	@Autowired
	PhoneValidator phoneValidator;
	
	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return RegisterModels.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		RegisterModels registerFrom = (RegisterModels) arg0;
		
		//Kiểm tra rỗng
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "name", "NotEmpty.registerForm.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "password", "NotEmpty.registerForm.password");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "rePassword", "NotEmpty.registerForm.rePassword");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "fullName", "NotEmpty.registerForm.fullName");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "address", "NotEmpty.registerForm.address");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "email", "NotEmpty.registerForm.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "phoneNumber", "NotEmpty.registerForm.phoneNumber");
		
		//Kiểm tra user name duy nhất
		if(!AccountDAO.checkUniqueUserName(registerFrom.getName())){
			arg1.rejectValue("name", "UniqueNameLogin.registerForm.name");
		}
		
		//Kiểm tra password và repassword nhập có giống nhau không
		if(!registerFrom.getPassword().equals(registerFrom.getRePassword())){
			arg1.rejectValue("rePassword", "Diff.registerForm.confirmPassword");
		}
		
		//Kiểm tra định dạng email
		if(!EmailValidator.getInstance().isValid((registerFrom.getEmail()))){
			arg1.rejectValue("email", "Pattern.registerForm.email");
		}
		
		//Kiểm tra email có duy nhất
		if(!AccountDAO.checkUniqueEmail(registerFrom.getEmail())){
			arg1.rejectValue("email", "UniqueNameEmail.registerFrom.email");
		}
		
		//Kiểm tra số điện thoại hợp lệ
		if(!phoneValidator.isValid(registerFrom.getPhoneNumber(), null)){
			arg1.rejectValue("phoneNumber", "Pattern.registerForm.phoneNumber");
		}
	}

}
