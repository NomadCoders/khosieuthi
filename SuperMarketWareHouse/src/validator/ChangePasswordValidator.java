package validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import model.ChangePassword;

public class ChangePasswordValidator implements Validator{

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return ChangePassword.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		
		ChangePassword item = (ChangePassword)arg0;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "currentPassword", "NotEmpty.changePassword.currentPassword");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "newPassword", "NotEmpty.changePassword.newPassword");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "rePassword", "NotEmpty.changePassword.rePasswords");
		
		if(item.getNewPassword().equals(item.getCurrentPassword())){
			arg1.rejectValue("newPassword", "NotMatchOldPassword.changePassword.newPassword");
		}
		
		//Kiểm tra password và repassword nhập có giống nhau không
		if(!item.getNewPassword().equals(item.getRePassword())){
			arg1.rejectValue("rePassword", "Diff.changePassword.rePasswords");
		}
	}

}
