package validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import model.LoginModels;

public class LoginValidator implements Validator{

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return LoginModels.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		/*LoginModels loginForm = (LoginModels)arg0;*/
		
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "nameLogin", "NotEmpty.loginForm.nameLogin");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "password", "NotEmpty.loginForm.password");
	}

}
