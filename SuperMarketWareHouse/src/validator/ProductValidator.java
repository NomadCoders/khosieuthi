package validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pojo.Products;

public class ProductValidator implements Validator{

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return Products.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		Products product = (Products)arg0;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "name", "NotEmpty.product.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "price", "NotEmpty.product.price");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "stock", "NotEmpty.product.stock");
		
		if(product.getStock() < 0){
			arg1.rejectValue("stock", "NotLessThanZero.product.stock");
		}
		
		if(product.getPrice() < 0){
			arg1.rejectValue("price", "NotLessThanZero.product.price");
		}
	}

}
