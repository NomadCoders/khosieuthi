package validator;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import model.ExportModels;

public class GoodsIssuedNoteValidator implements Validator{
	@Autowired
	PhoneValidator phoneValidator;
	
	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return ExportModels.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		ExportModels item = (ExportModels)arg0;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "nameReceive", "NotEmpty.exportForm.nameReceive");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "email", "NotEmpty.exportForm.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "phoneNumber", "NotEmpty.exportForm.phoneNumber");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "addressCompany", "NotEmpty.exportForm.addressCompany");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "addressWareHouse", "NotEmpty.exportForm.addressWareHouse");
		
		if(!phoneValidator.isValid(item.getPhoneNumber(), null)){
			arg1.rejectValue("phoneNumber", "Pattern.exportForm.phoneNumber");
		}
		
		if(!EmailValidator.getInstance().isValid(item.getEmail())){
			arg1.rejectValue("email", "Pattern.exportForm.email");
		}
		
		if(item.getListProducts().size() == 0){
			arg1.rejectValue("listProducts", "NotEmptyElement.exportForm.listProducts");
		}
	}

}
