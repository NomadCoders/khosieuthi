package validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import model.ImportModels;

public class GoodsReceiptNoteValidator implements Validator{
	
	@Autowired
	PhoneValidator phoneValidator;
	
	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return ImportModels.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		ImportModels item = (ImportModels)arg0;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "nameDeliver", "NotEmpty.importForm.nameDeliver");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "nameProducer", "NotEmpty.importForm.nameProducer");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "address", "NotEmpty.importForm.address");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "phoneNumber", "NotEmpty.importForm.phoneNumber");
		
		if(!phoneValidator.isValid(item.getPhoneNumber(), null)){
			arg1.rejectValue("phoneNumber", "Pattern.importForm.phoneNumber");
		}
		
		if(item.getListProducts().size() == 0){
			arg1.rejectValue("listProducts", "NotEmptyElement.importForm.listProducts");
		}
	}

}
