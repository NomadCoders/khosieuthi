package validator;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import dao.AccountDAO;
import pojo.Accounts;

public class UserInforValidator implements Validator{

	@Autowired
	PhoneValidator phoneValidator;
	
	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return Accounts.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		Accounts account = (Accounts)arg0;
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "fullName", "NotEmpty.accountInfor.fullName");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "address", "NotEmpty.accountInfor.address");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "email", "NotEmpty.accountInfor.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "phoneNumber", "NotEmpty.accountInfor.phoneNumber");
		
		if(!phoneValidator.isValid(account.getPhoneNumber(), null)){
			arg1.rejectValue("phoneNumber", "Pattern.accountInfor.phoneNumber");
		}
		
		if(!EmailValidator.getInstance().isValid((account.getEmail()))){
			arg1.rejectValue("email", "Pattern.accountInfor.email");
		}
		
		if(!AccountDAO.checkUniqueEmail(account.getEmail(), account.getName())){
			arg1.rejectValue("email", "UniqueNameEmail.accountInfor.email");
		}
	}

}
