package validator;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import model.GoodsOrderModels;

public class GoodsOrderValidator implements Validator{
	
	@Autowired
	PhoneValidator phoneValidator;
	
	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return GoodsOrderModels.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		GoodsOrderModels item = (GoodsOrderModels)arg0;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "nameBuyer", "NotEmpty.goodsOrder.nameBuyer");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "email", "NotEmpty.goodsOrder.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "phoneNumber", "NotEmpty.goodsOrder.phoneNumber");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "address", "NotEmpty.goodsOrder.address");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "addressWareHouse", "NotEmpty.goodsOrder.addressWareHouse");
		
		if(!phoneValidator.isValid(item.getPhoneNumber(), null)){
			arg1.rejectValue("phoneNumber", "Pattern.goodsOrder.phoneNumber");
		}
		
		if(!EmailValidator.getInstance().isValid(item.getEmail())){
			arg1.rejectValue("email", "Pattern.goodsOrder.email");
		}
		
		if(item.getListProducts().size() == 0){
			arg1.rejectValue("listProducts", "NotEmptyElement.goodsOrder.listProducts");
		}
	}

}
