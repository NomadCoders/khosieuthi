package utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import dao.AccountDAO;
import pojo.Accounts;

public class ManagerLogin {
	
	private static String sessionName = "user";
	private static String cookieName = "userName";
	
	public static void setLogin(Accounts account,Boolean rememberMe){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder
				.getRequestAttributes()).getRequest();
		if(rememberMe){
			HttpServletResponse respone = ((ServletRequestAttributes)RequestContextHolder
					.getRequestAttributes()).getResponse();
			Cookie cookie = new Cookie(cookieName, account.getName());
			//Giới hạn cookie là 60 * 60 * 24 * 365 * 10 (10 năm)
			cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
			cookie.setPath("/");
			respone.addCookie(cookie);
		}
		HttpSession session = request.getSession();
		session.setAttribute(sessionName, account);
	}
	
	public static Boolean checkIsLogin(){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder
				.getRequestAttributes()).getRequest();
		HttpSession session = request.getSession();
		Accounts account = (Accounts) session.getAttribute(sessionName);
		
		if(account != null){
			return true;
		}
		
		//Nếu có cookie, tạo lại session
		Cookie cookie = getCookie(request);
		if(cookie != null){
			String userName = cookie.getValue();
			Accounts accountLogin = AccountDAO
					.getAccountWithUserName(userName);
			session.setAttribute(sessionName, accountLogin);
			
			return true;
		}
		
		return false;
	}
	
	public static Boolean getPermissionInAccount(){
		if(!checkIsLogin()){
			return null;
		}
		
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder
				.getRequestAttributes()).getRequest();
		HttpSession session = request.getSession();
		Accounts account = (Accounts) session.getAttribute(sessionName);
		
		return account.getPermission();
	}
	
	public static String getNameLogin(){
		if(!checkIsLogin()){
			return "Default Name";
		}
		
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder
				.getRequestAttributes()).getRequest();
		HttpSession session = request.getSession();
		Accounts account = (Accounts) session.getAttribute(sessionName);

		return account.getFullName();
	}
	
	/**
	 * Hàm lấy UserName đăng nhập
	 * @return trả về username, nếu chưa đăng nhập trả về ""
	 */
	public static String getUserNameLogin(){
		if(!checkIsLogin()){
			return "";
		}
		
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder
				.getRequestAttributes()).getRequest();
		HttpSession session = request.getSession();
		Accounts account = (Accounts) session.getAttribute(sessionName);

		return account.getName();
	}
	
	/**
	 * Hàm lấy thông tin tài khoản đăng nhập
	 * @return trả về Accounts
	 */
	public static Accounts getAccountInLogin(){
		if(!checkIsLogin()){
			return null;
		}
		
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder
				.getRequestAttributes()).getRequest();
		HttpSession session = request.getSession();
		Accounts account = (Accounts) session.getAttribute(sessionName);
		
		return account;
	}
	
	public static Cookie getCookie(HttpServletRequest request){
		Cookie[] cookies = request.getCookies();
		if(cookies != null){
			for(Cookie cookie : cookies){
				if(cookie.getName().equals(cookieName)){
					return cookie;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Hàm cập nhập lại id và password cho object account để có thể cập nhập
	 * thông tin
	 * @param account
	 * @retun object account
	 */
	public static Accounts updateInforForAccount(Accounts account){
		if(!checkIsLogin()){
			return null;
		}
		
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder
				.getRequestAttributes()).getRequest();
		HttpSession session = request.getSession();
		Accounts accountSession = (Accounts) session.getAttribute(sessionName);
		
		account.setId(accountSession.getId());
		account.setPassword(accountSession.getPassword());
		account.setStatus(true);
		account.setIsDelete(false);
		account.setPermission(accountSession.getPermission());
		
		return account;
	}
	
	/**
	 * Cập nhập lại thông tin account vào session 
	 * khi được thay đổi thông tin
	 * @param account
	 */
	public static void updateAccountToSession(Accounts account){
		if(account != null){
			HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder
					.getRequestAttributes()).getRequest();
			HttpSession session = request.getSession();
			//session.removeAttribute(sessionName);
			session.setAttribute(sessionName, account);
		}
	}
	
	public static void logOff(){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder
				.getRequestAttributes()).getRequest();
		HttpSession session = request.getSession(false);
		
		Cookie cookie = getCookie(request);
		//Xóa cookie userName trong trình duyệt
		if(cookie != null){
			HttpServletResponse respone = ((ServletRequestAttributes)RequestContextHolder
					.getRequestAttributes()).getResponse();
			cookie.setValue(null);
			cookie.setMaxAge(0);
			cookie.setPath("/");
			respone.addCookie(cookie);
		}
		
		//Xóa session
		if(session != null){
			session.invalidate();
		}
	}
}
