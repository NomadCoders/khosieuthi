package utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;

public class WebUtils {
	private static MessageDigest md;
	private static String pathFontName = "/resources/fonts/times.ttf";
	private static String pathFontName2 = "/resources/fonts/timesbd.ttf";
	
	public static String cryptWithMD5(String pass){
	    try {
	        md = MessageDigest.getInstance("MD5");
	        byte[] passBytes = pass.getBytes();
	        md.reset();
	        byte[] digested = md.digest(passBytes);
	        StringBuffer sb = new StringBuffer();
	        for(int i=0;i<digested.length;i++){
	            sb.append(Integer.toHexString(0xff & digested[i]));
	        }
	        	return sb.toString();
    	} 
    	catch (NoSuchAlgorithmException ex) {
        ex.printStackTrace();
    	}
        return null;
	}
	
	
	public static ByteArrayOutputStream parseXHMTLToPDF(String contentHTML, 
			HttpServletRequest request){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ServletContext servletContext = request.getSession().getServletContext();
		//Lấy đường dẫn thư mục web
		String realPath = servletContext.getRealPath("");
		
		ITextRenderer renderer = new ITextRenderer();
		try {
			//BaseFont.IDENTITY_H encode cho font utf-8
			renderer.getFontResolver().addFont(realPath + pathFontName, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
			renderer.getFontResolver().addFont(realPath + pathFontName2, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
			
			renderer.setDocumentFromString(contentHTML);
			renderer.layout();
		    renderer.createPDF(baos);
		    renderer.setPDFVersion(PdfWriter.VERSION_1_7);
		} catch (DocumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baos;
	}
}
