package model;

import java.util.ArrayList;
import java.util.List;


public class ExportModels {
	private String nameReceive;
	private String addressCompany;
	private String addressWareHouse;
	private String email;
	private String phoneNumber;
	private int categoryId;
	private String note;
	private long totalQuantity;
	private List<ProductModels> listProducts;
	
	public ExportModels() {
		// TODO Auto-generated constructor stub
		listProducts = new ArrayList<ProductModels>();
	}

	/**
	 * @param nameReceive
	 * @param addressCompany
	 * @param addressWareHouse
	 * @param email
	 * @param phoneNumber
	 * @param categorySelect
	 * @param note
	 * @param totalQuantity
	 * @param listProduct
	 */
	public ExportModels(String nameReceive, String addressCompany, String addressWareHouse, String email,
			String phoneNumber, int categoryId, String note, long totalQuantity, List<ProductModels> listProduct) {
		this.nameReceive = nameReceive;
		this.addressCompany = addressCompany;
		this.addressWareHouse = addressWareHouse;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.categoryId = categoryId;
		this.note = note;
		this.totalQuantity = totalQuantity;
		this.listProducts = listProduct;
	}

	/**
	 * @return the nameReceive
	 */
	public String getNameReceive() {
		return nameReceive;
	}

	/**
	 * @return the addressCompany
	 */
	public String getAddressCompany() {
		return addressCompany;
	}

	/**
	 * @return the addressWareHouse
	 */
	public String getAddressWareHouse() {
		return addressWareHouse;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @return the categorySelect
	 */
	public int getCategoryId() {
		return categoryId;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @return the totalQuantity
	 */
	public long getTotalQuantity() {
		if(totalQuantity == 0){
			if(listProducts.size() > 0){
				for(ProductModels productModels : listProducts){
					totalQuantity = totalQuantity + productModels.getQuantity();
				}
			}
		}
		return totalQuantity;
	}

	/**
	 * @return the listProduct
	 */
	public List<ProductModels> getListProducts() {
		return listProducts;
	}

	/**
	 * @param nameReceive the nameReceive to set
	 */
	public void setNameReceive(String nameReceive) {
		this.nameReceive = nameReceive;
	}

	/**
	 * @param addressCompany the addressCompany to set
	 */
	public void setAddressCompany(String addressCompany) {
		this.addressCompany = addressCompany;
	}

	/**
	 * @param addressWareHouse the addressWareHouse to set
	 */
	public void setAddressWareHouse(String addressWareHouse) {
		this.addressWareHouse = addressWareHouse;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @param categorySelect the categorySelect to set
	 */
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @param totalQuantity the totalQuantity to set
	 */
	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	/**
	 * @param listProduct the listProduct to set
	 */
	public void setListProducts(List<ProductModels> listProducts) {
		this.listProducts = listProducts;
	}
	
	
}
