package model;

import java.util.ArrayList;
import java.util.List;

import pojo.Categoryproducts;
import pojo.Producers;

public class GoodsOrderModels {
	private String nameBuyer;
	private int producerId;
	private String phoneNumber;
	private String email;
	private String address;
	private String addressWareHouse;
	private int categoryId;
	private int quantity;
	private String note;
	private int productId;
	private List<ProductModels> listProducts;
	private List<Producers> listProducers;
	private List<Categoryproducts> listCategory;
	
	public GoodsOrderModels() {
		// TODO Auto-generated constructor stub
		listProducts = new ArrayList<ProductModels>();
	}
	
	/**
	 * @param nameBuyer
	 * @param producerId
	 * @param phoneNumber
	 * @param email
	 * @param address
	 * @param categoryId
	 * @param quantity
	 * @param note
	 * @param productId
	 * @param listProducts
	 * @param addressWareHouse
	 * @param listProducers
	 */
	public GoodsOrderModels(String nameBuyer, int producerId, String phoneNumber, String email, String address, String addressWareHouse,
			int categoryId, int quantity, String note, int productId, List<ProductModels> listProducts, 
			List<Producers> listProducers) {
		this.nameBuyer = nameBuyer;
		this.producerId = producerId;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.address = address;
		this.categoryId = categoryId;
		this.quantity = quantity;
		this.note = note;
		this.productId = productId;
		this.listProducts = listProducts;
		this.addressWareHouse = addressWareHouse;
		this.listProducers = listProducers;
	}
	/**
	 * @return the nameBuyer
	 */
	public String getNameBuyer() {
		return nameBuyer;
	}
	/**
	 * @return the producerId
	 */
	public int getProducerId() {
		return producerId;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @return the categoryId
	 */
	public int getCategoryId() {
		return categoryId;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}
	/**
	 * @return the productId
	 */
	public int getProductId() {
		return productId;
	}
	/**
	 * @return the listProducts
	 */
	public List<ProductModels> getListProducts() {
		return listProducts;
	}
	/**
	 * @param nameBuyer the nameBuyer to set
	 */
	public void setNameBuyer(String nameBuyer) {
		this.nameBuyer = nameBuyer;
	}
	/**
	 * @param producerId the producerId to set
	 */
	public void setProducerId(int producerId) {
		this.producerId = producerId;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(int productId) {
		this.productId = productId;
	}
	/**
	 * @param listProducts the listProducts to set
	 */
	public void setListProducts(List<ProductModels> listProducts) {
		this.listProducts = listProducts;
	}

	/**
	 * @return the listProducers
	 */
	public List<Producers> getListProducers() {
		return listProducers;
	}

	/**
	 * @param listProducers the listProducers to set
	 */
	public void setListProducers(List<Producers> listProducers) {
		this.listProducers = listProducers;
	}

	/**
	 * @return the listCategory
	 */
	public List<Categoryproducts> getListCategory() {
		return listCategory;
	}

	/**
	 * @param listCategory the listCategory to set
	 */
	public void setListCategory(List<Categoryproducts> listCategory) {
		this.listCategory = listCategory;
	}

	/**
	 * @return the addressWareHouse
	 */
	public String getAddressWareHouse() {
		return addressWareHouse;
	}

	/**
	 * @param addressWareHouse the addressWareHouse to set
	 */
	public void setAddressWareHouse(String addressWareHouse) {
		this.addressWareHouse = addressWareHouse;
	}
	
	
}
