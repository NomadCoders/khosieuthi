package model;

import java.util.List;


public class SuperMarketWareHouseStatistics {
	private long totalNumberProducts;
	private long totalNumberCategorys;
	private long totalNumberTypePorducts;
	private long totalNumberAccounts;
	private List<CategoryStatistics> listCategories;
	
	public SuperMarketWareHouseStatistics() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param totalProducts
	 * @param totalCategorys
	 * @param totalTypePorducts
	 * @param totalAccounts
	 * @param listCategories
	 */
	public SuperMarketWareHouseStatistics(long totalNumberProducts, long totalNumberCategorys, long totalNumberTypePorducts, long totalNumberAccounts,
			List<CategoryStatistics> listCategories) {
		super();
		this.totalNumberProducts = totalNumberProducts;
		this.totalNumberCategorys = totalNumberCategorys;
		this.totalNumberTypePorducts = totalNumberTypePorducts;
		this.totalNumberAccounts = totalNumberAccounts;
		this.listCategories = listCategories;
	}

	/**
	 * @return the totalProducts
	 */
	public long getTotalNumberProducts() {
		return totalNumberProducts;
	}

	/**
	 * @return the totalCategorys
	 */
	public long getTotalNumberCategorys() {
		return totalNumberCategorys;
	}

	/**
	 * @return the totalTypePorducts
	 */
	public long getTotalNumberTypePorducts() {
		return totalNumberTypePorducts;
	}

	/**
	 * @return the totalAccounts
	 */
	public long getTotalNumberAccounts() {
		return totalNumberAccounts;
	}

	/**
	 * @return the listCategories
	 */
	public List<CategoryStatistics> getListCategories() {
		return listCategories;
	}

	/**
	 * @param totalProducts the totalProducts to set
	 */
	public void setTotalNumberProducts(long totalNumberProducts) {
		this.totalNumberProducts = totalNumberProducts;
	}

	/**
	 * @param totalCategorys the totalCategorys to set
	 */
	public void setTotalNumberCategorys(long totalNumberCategorys) {
		this.totalNumberCategorys = totalNumberCategorys;
	}

	/**
	 * @param totalTypePorducts the totalTypePorducts to set
	 */
	public void setTotalNumberTypePorducts(long totalNumberTypePorducts) {
		this.totalNumberTypePorducts = totalNumberTypePorducts;
	}

	/**
	 * @param totalAccounts the totalAccounts to set
	 */
	public void setTotalNumberAccounts(long totalNumberAccounts) {
		this.totalNumberAccounts = totalNumberAccounts;
	}

	/**
	 * @param listCategories the listCategories to set
	 */
	public void setListCategories(List<CategoryStatistics> listCategories) {
		this.listCategories = listCategories;
	}
	
	
}
