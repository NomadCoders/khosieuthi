package model;

import java.util.Set;

import pojo.Accounts;
import pojo.Goodsissuednote;
import pojo.Goodsorder;

public class RegisterModels extends Accounts{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String rePassword;
	
	public RegisterModels() {
		// TODO Auto-generated constructor stub
		super();
	}

	/**
	 * @param fullName
	 * @param name
	 * @param password
	 * @param email
	 * @param phoneNumber
	 * @param permission
	 * @param status
	 * @param isDelete
	 * @param goodsissuednotes
	 * @param goodsorders
	 */
	public RegisterModels(String fullName, String name, String password, String email, String phoneNumber,
			Boolean permission, Boolean status, Set<Goodsissuednote> goodsissuednotes, Set<Goodsorder> goodsorders
			, String mRePassword, String address, Boolean isDelete) {
		super(fullName, name, password, email, phoneNumber, address, permission, isDelete, status, goodsissuednotes, goodsorders);
		// TODO Auto-generated constructor stub
		this.rePassword = mRePassword;
	}

	/**
	 * @return the rePassword
	 */
	public String getRePassword() {
		return rePassword;
	}

	/**
	 * @param rePassword the rePassword to set
	 */
	public void setRePassword(String rePassword) {
		this.rePassword = rePassword;
	}
	
	
}
