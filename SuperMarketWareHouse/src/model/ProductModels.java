package model;

public class ProductModels{
	
	private int productId;
	private String name;
	private int price;
	private String nameUnitCalculate;
	private int quantity;
	private long totalMoney;
	
	/**
	 * 
	 */
	public ProductModels() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param productId
	 * @param name
	 * @param price
	 * @param nameUnitCalculate
	 * @param quantity
	 * @param totalMoney
	 */
	public ProductModels(int productId, String name, int price, String nameUnitCalculate, int quantity,
			long totalMoney) {
		super();
		this.productId = productId;
		this.name = name;
		this.price = price;
		this.nameUnitCalculate = nameUnitCalculate;
		this.quantity = quantity;
		this.totalMoney = totalMoney;
	}

	/**
	 * @return the productId
	 */
	public int getProductId() {
		return productId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @return the nameUnitCalculate
	 */
	public String getNameUnitCalculate() {
		return nameUnitCalculate;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @return the totalMoney
	 */
	public long getTotalMoney() {
		if(totalMoney == 0){
			setTotalMoney(price * quantity);
		}
		return totalMoney;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(int productId) {
		this.productId = productId;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @param nameUnitCalculate the nameUnitCalculate to set
	 */
	public void setNameUnitCalculate(String nameUnitCalculate) {
		this.nameUnitCalculate = nameUnitCalculate;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @param totalMoney the totalMoney to set
	 */
	public void setTotalMoney(long totalMoney) {
		this.totalMoney = totalMoney;
	}
	
	
}
