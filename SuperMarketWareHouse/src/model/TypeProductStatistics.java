package model;

public class TypeProductStatistics {
	private String nameTypeProducts;
	private long totalProducts;
	
	public TypeProductStatistics() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param nameTypeProducts
	 * @param totalProducts
	 */
	public TypeProductStatistics(String nameTypeProducts, long totalProducts) {
		this.nameTypeProducts = nameTypeProducts;
		this.totalProducts = totalProducts;
	}

	/**
	 * @return the nameTypeProducts
	 */
	public String getNameTypeProducts() {
		return nameTypeProducts;
	}

	/**
	 * @return the totalProducts
	 */
	public long getTotalProducts() {
		return totalProducts;
	}

	/**
	 * @param nameTypeProducts the nameTypeProducts to set
	 */
	public void setNameTypeProducts(String nameTypeProducts) {
		this.nameTypeProducts = nameTypeProducts;
	}

	/**
	 * @param totalProducts the totalProducts to set
	 */
	public void setTotalProducts(long totalProducts) {
		this.totalProducts = totalProducts;
	}
	
	
}
