package model;

import java.util.List;

public class CategoryStatistics {
	private int categroyId;
	private String nameCategory;
	private long totalProducts;
	private List<TypeProductStatistics> listTypeProducts;
	
	public CategoryStatistics() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param categroyId
	 * @param nameCategory
	 * @param totalProducts
	 * @param listTypeProducts
	 */
	public CategoryStatistics(int categroyId, String nameCategory, long totalProducts,
			List<TypeProductStatistics> listTypeProducts) {
		super();
		this.categroyId = categroyId;
		this.nameCategory = nameCategory;
		this.totalProducts = totalProducts;
		this.listTypeProducts = listTypeProducts;
	}

	/**
	 * @return the categroyId
	 */
	public int getCategroyId() {
		return categroyId;
	}

	/**
	 * @return the nameCategory
	 */
	public String getNameCategory() {
		return nameCategory;
	}

	/**
	 * @return the totalProducts
	 */
	public long getTotalProducts() {
		return totalProducts;
	}

	/**
	 * @return the listTypeProducts
	 */
	public List<TypeProductStatistics> getListTypeProducts() {
		return listTypeProducts;
	}

	/**
	 * @param categroyId the categroyId to set
	 */
	public void setCategroyId(int categroyId) {
		this.categroyId = categroyId;
	}

	/**
	 * @param nameCategory the nameCategory to set
	 */
	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}

	/**
	 * @param totalProducts the totalProducts to set
	 */
	public void setTotalProducts(long totalProducts) {
		this.totalProducts = totalProducts;
	}

	/**
	 * @param listTypeProducts the listTypeProducts to set
	 */
	public void setListTypeProducts(List<TypeProductStatistics> listTypeProducts) {
		this.listTypeProducts = listTypeProducts;
	}
	
}
