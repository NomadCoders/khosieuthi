package model;

import java.util.ArrayList;
import java.util.List;


public class ImportModels {
	private int goodsOrderId;
	private String nameDeliver;
	private String nameProducer;
	private String address;
	private String phoneNumber;
	private String note;
	private List<ProductModels> listProducts;
	
	public ImportModels() {
		// TODO Auto-generated constructor stub
		listProducts = new ArrayList<ProductModels>();
	}
	
	/**
	 * @param goodsOrderId
	 * @param nameDeliver
	 * @param nameProducer
	 * @param address
	 * @param phoneNumber
	 * @param note
	 * @param listProduct
	 */
	public ImportModels(int goodsOrderId, String nameDeliver, String nameProducer, String address,
			String phoneNumber, String note, List<ProductModels> listProducts) {
		this.setGoodsOrderId(goodsOrderId);
		this.nameDeliver = nameDeliver;
		this.nameProducer = nameProducer;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.note = note;
		this.listProducts = listProducts;
	}

	/**
	 * @return the nameDeliver
	 */
	public String getNameDeliver() {
		return nameDeliver;
	}

	/**
	 * @return the nameProducer
	 */
	public String getNameProducer() {
		return nameProducer;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @return the listProduct
	 */
	public List<ProductModels> getListProducts() {
		return listProducts;
	}

	/**
	 * @param nameDeliver the nameDeliver to set
	 */
	public void setNameDeliver(String nameDeliver) {
		this.nameDeliver = nameDeliver;
	}

	/**
	 * @param nameProducer the nameProducer to set
	 */
	public void setNameProducer(String nameProducer) {
		this.nameProducer = nameProducer;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @param listProduct the listProduct to set
	 */
	public void setListProduct(List<ProductModels> listProducts) {
		this.listProducts = listProducts;
	}

	/**
	 * @return the goodsOrderId
	 */
	public int getGoodsOrderId() {
		return goodsOrderId;
	}

	/**
	 * @param goodsOrderId the goodsOrderId to set
	 */
	public void setGoodsOrderId(int goodsOrderId) {
		this.goodsOrderId = goodsOrderId;
	}
	
	
}
