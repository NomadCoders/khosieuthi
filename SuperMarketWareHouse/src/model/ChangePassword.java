package model;

public class ChangePassword {
	private String currentPassword;
	private String newPassword;
	private String rePassword;
	
	public ChangePassword() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param currentPassword
	 * @param newPassword
	 * @param rePassword
	 */
	public ChangePassword(String currentPassword, String newPassword, String rePassword) {
		super();
		this.currentPassword = currentPassword;
		this.newPassword = newPassword;
		this.rePassword = rePassword;
	}

	/**
	 * @return the currentPassword
	 */
	public String getCurrentPassword() {
		return currentPassword;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @return the rePassword
	 */
	public String getRePassword() {
		return rePassword;
	}

	/**
	 * @param currentPassword the currentPassword to set
	 */
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @param rePassword the rePassword to set
	 */
	public void setRePassword(String rePassword) {
		this.rePassword = rePassword;
	}

	
}
