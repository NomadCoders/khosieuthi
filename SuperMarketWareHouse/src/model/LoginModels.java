package model;

public class LoginModels {
	private String nameLogin;
	private String password;
	private Boolean rememberMe;
	
	
	public LoginModels() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param nameLogin
	 * @param password
	 * @param rememberMe
	 */
	public LoginModels(String nameLogin, String password, Boolean rememberMe) {
		super();
		this.nameLogin = nameLogin;
		this.password = password;
		this.rememberMe = rememberMe;
	}
	/**
	 * @return the nameLogin
	 */
	public String getNameLogin() {
		return nameLogin;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @return the rememberMe
	 */
	public Boolean getRememberMe() {
		return rememberMe;
	}
	/**
	 * @param nameLogin the nameLogin to set
	 */
	public void setNameLogin(String nameLogin) {
		this.nameLogin = nameLogin;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @param rememberMe the rememberMe to set
	 */
	public void setRememberMe(Boolean rememberMe) {
		this.rememberMe = rememberMe;
	}
}
