package pojo;
// Generated Aug 7, 2016 10:21:29 AM by Hibernate Tools 4.3.1.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Unitcalculate generated by hbm2java
 */
@Entity
@Table(name = "unitcalculate", catalog = "warehousesupermarket")
public class Unitcalculate implements java.io.Serializable {

	private Integer id;
	private String name;
	private Boolean isDelete;
	private Set<Products> productses = new HashSet<Products>(0);

	public Unitcalculate() {
	}

	public Unitcalculate(String name, Boolean isDelete, Set<Products> productses) {
		this.name = name;
		this.isDelete = isDelete;
		this.productses = productses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "Name", length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "IsDelete")
	public Boolean getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "unitcalculate")
	public Set<Products> getProductses() {
		return this.productses;
	}

	public void setProductses(Set<Products> productses) {
		this.productses = productses;
	}

}
