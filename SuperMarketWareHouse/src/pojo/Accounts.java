package pojo;
// Generated Aug 7, 2016 10:21:29 AM by Hibernate Tools 4.3.1.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Accounts generated by hbm2java
 */
@Entity
@Table(name = "accounts", catalog = "warehousesupermarket")
public class Accounts implements java.io.Serializable {

	private Integer id;
	private String fullName;
	private String name;
	private String password;
	private String email;
	private String phoneNumber;
	private String address;
	private Boolean status;
	private Boolean permission;
	private Boolean isDelete;
	private Set<Goodsissuednote> goodsissuednotes = new HashSet<Goodsissuednote>(0);
	private Set<Goodsorder> goodsorders = new HashSet<Goodsorder>(0);

	public Accounts() {
	}

	public Accounts(String fullName, String name, String password, String email, String phoneNumber, String address,
			Boolean status, Boolean permission, Boolean isDelete, Set<Goodsissuednote> goodsissuednotes,
			Set<Goodsorder> goodsorders) {
		this.fullName = fullName;
		this.name = name;
		this.password = password;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.status = status;
		this.permission = permission;
		this.isDelete = isDelete;
		this.goodsissuednotes = goodsissuednotes;
		this.goodsorders = goodsorders;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "FullName", length = 50)
	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Column(name = "Name", length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "Password", length = 200)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "Email", length = 70)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "PhoneNumber", length = 20)
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "Address", length = 100)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "Status")
	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Column(name = "Permission")
	public Boolean getPermission() {
		return this.permission;
	}

	public void setPermission(Boolean permission) {
		this.permission = permission;
	}

	@Column(name = "isDelete")
	public Boolean getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accounts")
	public Set<Goodsissuednote> getGoodsissuednotes() {
		return this.goodsissuednotes;
	}

	public void setGoodsissuednotes(Set<Goodsissuednote> goodsissuednotes) {
		this.goodsissuednotes = goodsissuednotes;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accounts")
	public Set<Goodsorder> getGoodsorders() {
		return this.goodsorders;
	}

	public void setGoodsorders(Set<Goodsorder> goodsorders) {
		this.goodsorders = goodsorders;
	}

}
