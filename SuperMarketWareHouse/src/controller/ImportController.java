package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import model.ImportModels;
import pojo.Goodsorder;
import service.GoodsOrderService;
import service.ImportService;
import utils.ManagerLogin;
import validator.GoodsReceiptNoteValidator;

@Controller
@RequestMapping(value="/goods-receipt-note")
public class ImportController {
	
	@Autowired
	private GoodsOrderService goodsOrderService;
	
	@Autowired
	private ImportService importService;
	
	@Autowired
	private GoodsReceiptNoteValidator goodsReceiptNoteValidator;
	
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public String createImport(Model m){
		
		if(ManagerLogin.getPermissionInAccount() == null){
			return "redirect:/account/login";
		}
		else{
			//Kiểm tra quyền admin, chỉ quyền admin mới vào được
			if(ManagerLogin.getPermissionInAccount()){
				ImportModels importModels = new ImportModels();
				List<Goodsorder> listGoodsOrder = goodsOrderService.listGoodOrder();
				
				int id = 0;
				if(listGoodsOrder.size() > 0){
					id = listGoodsOrder.get(0).getGoodsOrderId();
				}
				
				importModels = importService.loadImportModels(id);
				m.addAttribute("importForm", importModels);
				m.addAttribute("goodsOrderList", listGoodsOrder);
				
				return "import";
			}
		}
		
		return "redirect:/";
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String checkCreateImport(@ModelAttribute("importForm")
	ImportModels importModels, @RequestParam("hidTotalPrice")long totalMoney,
	BindingResult result, Model m){
		
		goodsReceiptNoteValidator.validate(importModels, result);
		
		//Nếu có lỗi trở về trang lập phiếu nhập
		if(result.hasErrors()){
			return "import";
		}
		
		//Thêm thành công trở về trang chủ
		if(importService.addGoodsReciptNote(importModels, totalMoney)){
			return "redirect:/";
		}
		
		List<Goodsorder> listGoodsOrder = goodsOrderService.listGoodOrder();
		m.addAttribute("goodsOrderList", listGoodsOrder);
		return "import";
	}
}
