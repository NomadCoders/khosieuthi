package controller;



import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
/*import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;*/
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import model.ChangePassword;
import model.LoginModels;
import model.RegisterModels;
import pojo.Accounts;
import service.AccountService;
import service.CategoryService;
import utils.ManagerLogin;
import validator.ChangePasswordValidator;
import validator.LoginValidator;
import validator.UserInforValidator;
import validator.UserValidator;

@Controller
@RequestMapping(value="/account")
public class AccountController {
	
	
	@Autowired
	private UserValidator userValidtator;
	
	@Autowired
	private LoginValidator loginValidator;
	
	@Autowired
	private UserInforValidator userInforValidator;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ChangePasswordValidator changePasswordValidator;
	
	/*@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(userValidtator);
	}*/
	
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String login(Model m){
		//Nếu đăng nhập rồi đưa về trang index
		if(ManagerLogin.checkIsLogin()){
			return "redirect:/";
		}
		LoginModels loginModels = new LoginModels();
		m.addAttribute("loginFrom", loginModels);
		
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String checkLogin(@ModelAttribute("loginFrom")LoginModels loginModel,
			BindingResult result ,Model m){
		loginValidator.validate(loginModel, result);
		String msg = "";
		if(result.hasErrors()){
			return "login";
		}
		
		Accounts account = accountService.Login(loginModel.getNameLogin(), 
				loginModel.getPassword());
		
		//Đăng nhập thành công
		if(account != null){
			ManagerLogin.setLogin(account,loginModel
					.getRememberMe());
			return "redirect:/";
		}
		
		//Đăng nhập thất bại
		msg = "Đăng nhập thất bại, sai tên đăng nhập hoặc mật khẩu";
		m.addAttribute("msg", msg);
		return "login";
	}
	
	@RequestMapping(value="/register", method = RequestMethod.GET)
	public String register(Model m){
		
		//Nếu là quyền admin mới vào được trang đăng ký
		if(ManagerLogin.getPermissionInAccount()){
			RegisterModels registerUser = new RegisterModels();
			m.addAttribute("registerForm", registerUser );
			return "register";
		}

		return "redirect:/";
	}
	
	@RequestMapping(value="/register", method = RequestMethod.POST)
	public String checkRegister(HttpServletResponse respone ,@ModelAttribute("registerForm")
	RegisterModels account, BindingResult result, Model m){
		userValidtator.validate(account, result);
		
		if(result.hasErrors()){
			account.setPassword("");
			account.setRePassword("");
			
			m.addAttribute("msg", "Đăng ký thất bại");
			return "register";
		}
		
		//Đăng ký thành công
		if(accountService.addAccount(account)){
			return "redirect:/";
		}
		
		/*String mesage = "Đăng ký thất bại";*/
		m.addAttribute("msg", "Đăng ký thất bại");
		return "register";
	}
	
	//Controller cho chỉnh sửa thông tin cá nhân của tài khoản
	@RequestMapping(value="/information", method = RequestMethod.GET)
	public String editAccount(@RequestParam(value="msg", required=false)Integer msg 
		,Model m){
		
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		
		if(msg != null){
			m.addAttribute("msg", "Thay đổi mật khẩu thành công");
		}
		
		m.addAttribute("accountInfor", ManagerLogin.getAccountInLogin());
		return "editAccount";
	}
	
	@RequestMapping(value="/information", method = RequestMethod.POST)
	public String checkEditAccount(@ModelAttribute("accountInfor")
	Accounts account, BindingResult result, Model m){
		userInforValidator.validate(account, result);
		if(result.hasErrors()){
			m.addAttribute("msg", "Thay đổi thông tin thất bại!");
			return "editAccount";
		}
		
		if(accountService.updateAccountInfor(account)){
			m.addAttribute("msg","Thay đổi thông tin thành công");
		}
		else{
			m.addAttribute("msg", "Thay đổi thông tin thất bại!");
		}
		
		return "editAccount";
	}
	
	//Thay đổi mật khẩu
	@RequestMapping(value="/change-password", method = RequestMethod.GET)
	public String changePasswordUser(Model m){
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		
		ChangePassword changePassword = new ChangePassword();
		m.addAttribute("changePassword", changePassword);
		return "changePassword";
	}
	
	@RequestMapping(value="/change-password", method = RequestMethod.POST)
	public String checkChangePasswordUser(
			@ModelAttribute("changePassowrd")ChangePassword passowrd, 
			BindingResult result, Model m){
		
		changePasswordValidator.validate(passowrd, result);
		
		if(result.hasErrors()){
			return "changePassword";
		}
		
		if(accountService.changePassowrd(passowrd, ManagerLogin.getUserNameLogin())){
			
			//Thay đổi password thành công, gửi thông báo về trang information
			m.addAttribute("msg", 1);
			
			return "redirect:/account/information";
		}
		
		//Xóa các password cũ
		passowrd.setCurrentPassword("");
		passowrd.setNewPassword("");
		passowrd.setRePassword("");
		m.addAttribute("changePassword", passowrd);
		
		//Gắn thông báo lỗi thay đổi password không thành công
		m.addAttribute("msg", "Thay đổi mật khẩu thất bại!");
		return "changePassword";
	}
	
	//Trang Admin
	//Danh sách tài khoản
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public String listAccounts(@RequestParam(value="msg", required = false)Integer msg, Model m){
		if(ManagerLogin.getPermissionInAccount()){
			m.addAttribute("listAccounts", accountService.getListAccount());
			m.addAttribute("listCategory", categoryService.loadListCategory());
			if(msg != null){
				String message = "";
				if(msg == 1){
					message = "Xóa Tài Khoản Thành Công";
				}
				
				if(msg == 2){
					message = "Xóa Tài Khoản Thất Bại!";
				}
				m.addAttribute("msg", message);
			}
			
			return "listAccounts";
		}
		
		return "redirect:/";
	}
	
	//Chỉnh sửa tài khoản
	@RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editAccountWithAdmin(@PathVariable("id")int id, Model m){
		
		if(ManagerLogin.getPermissionInAccount()){
			if(id <= 0){
				return "redirect:/account/list";
			}
			
			Accounts account = accountService.getAccountById(id);
			if(account == null){
				return "redirect:/error/404";
			}
			
			m.addAttribute("userInfor", account);
			return "adminEditAccount";
		}
		
		return "redirect:/";
	}
	
	@RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String checkEditAccountWithAdmin(@PathVariable("id")int id, 
			@ModelAttribute("userInfor")Accounts user,
			BindingResult result, Model m){
		
		if(id < 0){
			return "redirect:/account/list";
		}
		
		userInforValidator.validate(user, result);
		
		if(result.hasErrors()){
			return "adminEditAccount";
		}
		
		//Cập nhập tài khoản
		if(accountService.updateAccountInforWithAdmin(user)){
			//Cập nhập thành công
			return "redirect:/account/list";
		}
		
		//cập nhập thất bại
		return "adminEditAccount";
	}
	
	//Xóa tài khoản
	@RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public String deleteAccountWithAdmin(@PathVariable("id")int id, Model m){
		int msg = 0;
		if(ManagerLogin.getPermissionInAccount()){
			if(id > 0){
				if(accountService.deleteAccount(id)){
					msg = 1;
				}
				else{
					msg = 2;
				}
				
				m.addAttribute("msg", msg);
			}
			
			return "redirect:/account/list";
		}
		
		return "redirect:/";
	}
	
	@RequestMapping(value="/logoff", method = RequestMethod.GET)
	public String logOff(){
		ManagerLogin.logOff();
		return "redirect:/";
	}
}
