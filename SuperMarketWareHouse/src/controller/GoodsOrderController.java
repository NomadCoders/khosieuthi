package controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import model.GoodsOrderModels;
import pojo.Producers;
import service.CategoryService;
import service.GoodsOrderService;
import service.ProducerService;
import utils.ManagerLogin;
import validator.GoodsOrderValidator;

@Controller
@RequestMapping(value="/goods-order")
public class GoodsOrderController {
	
	@Autowired
	private ProducerService producerSerivce;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private GoodsOrderService goodsOrderService;
	
	@Autowired
	private GoodsOrderValidator goodsOrderValidator;
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String createGoodsOrder(Model m){
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		GoodsOrderModels item = new GoodsOrderModels();
		item.setQuantity(1);
		//Load danh sách nhà sản xuất
		item.setListProducers(producerSerivce.loadListForComboBox());
		//Load danh sách danh mục sản phẩm
		item.setListCategory(categoryService.loadListForComboBox(true));
		//Lấy id nhà sản xuất đầu tiên
		int id = item.getListProducers().get(0).getProducerId();
		//Lấy địa chỉ và số điện thoại nhà sản xuất
		Producers producers = producerSerivce.getInforInProducer(id);
		
		item.setAddress(producers.getAddress());
		item.setPhoneNumber(producers.getPhoneNumber());
		
		m.addAttribute("goodsOrder", item);
		return "order";
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String checkCreateGoodsOrder(@ModelAttribute("goodsOrder")
	GoodsOrderModels item, @RequestParam("hidTotalPrice")long totalMoney, 
	BindingResult result, Model m){
		
		goodsOrderValidator.validate(item, result);
		//Load các combobox và số phone và địa chỉ nhà sản xuất cũ
		item.setListProducers(producerSerivce.loadListForComboBox());
		item.setListCategory(categoryService.loadListForComboBox(true));
		Producers producers = producerSerivce.getInforInProducer(item.getProducerId());
		
		item.setAddress(producers.getAddress());
		item.setPhoneNumber(producers.getPhoneNumber());
		
		
		/*if(item.getListProducts().size() == 0){
			m.addAttribute("msg", "Đơn Hàng phải có ít nhất 1 sản phẩm");
			return "order";
		}*/
		
		if(result.hasErrors()){
			//Nếu có lỗi đưa về trang lập đơn hàng
			return "order";
		}
		
		if(goodsOrderService.addGoodsOrder(item, totalMoney, 
				ManagerLogin.getAccountInLogin())){
			//Tạo phiếu thành công, đưa về trang chủ
			return "redirect:/";
		}
		
		return "order";
	}
}
