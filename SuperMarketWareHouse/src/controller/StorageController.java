package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pojo.Goodsissuednote;
import pojo.Goodsorder;
import pojo.Goodsreceiptnote;
import service.ExportService;
import service.GoodsOrderService;
import service.ImportService;
import utils.ManagerLogin;

@Controller
@RequestMapping(value="/storage")
public class StorageController {
	
	@Autowired
	private ImportService importService;
	
	@Autowired
	private GoodsOrderService goodsOrderSerivce;
	
	@Autowired
	private ExportService exportService;
	
	@RequestMapping(value="/goods-receipt-note", method = RequestMethod.GET)
	public String storageListGoodsReceiptNote(Model m){
		
		if(ManagerLogin.getPermissionInAccount() == null){
			return "redirect:/account/login";
		}
		else{
			if(ManagerLogin.getPermissionInAccount()){
				List<Goodsreceiptnote> listNote = 
						importService.getListGoodsReceiptNote();
				int id = 0;
				id = listNote.get(0).getId();
				m.addAttribute("detailReceiptNote", 
						listNote.get(0).getDetailgoodsreceiptnotes());
				String goodsReceiptNoteId = "";
				if(id > 0){
					goodsReceiptNoteId = goodsReceiptNoteId + id;
				}
				
				m.addAttribute("receiptNote", listNote);
				m.addAttribute("id", id);
				return "listImport";
			}
		}
		
		return "redirect:/"; 
	}
	
	@RequestMapping(value="/goods-receipt-note", method = RequestMethod.POST)
	public String storageListGoodsReceiptNote(@RequestParam("id") 
	int goodsReceiptNoteId, Model m){
		
		List<Goodsreceiptnote> listNote = 
				importService.getListGoodsReceiptNote();
		
		String id = "";
		if(goodsReceiptNoteId < 0){
			goodsReceiptNoteId = 0;
		}
		
		id = id + listNote.get(goodsReceiptNoteId).getId();
		
		m.addAttribute("receiptNote", listNote
				);
		
		m.addAttribute("detailReceiptNote", listNote
				.get(goodsReceiptNoteId).getDetailgoodsreceiptnotes());
		
		//Gửi mã phiếu để cho chi tiết phiếu biết là của phiếu nhập nào
		m.addAttribute("id", id);
		return "listImport";
	}
	
	@RequestMapping(value="/goods-order", method = RequestMethod.GET)
	public String storageListGoodsOrder(Model m){
		
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		
		List<Goodsorder> listNote = 
				goodsOrderSerivce.getListGoodsOrderIsDone();
		int id = 0;
		id = listNote.get(0).getGoodsOrderId();
		m.addAttribute("detailGoodsOrder", 
				listNote.get(0).getDetailgoodorders());
		String goodsReceiptNoteId = "";
		if(id > 0){
			goodsReceiptNoteId = goodsReceiptNoteId + id;
		}
		
		m.addAttribute("goodsOrder", listNote);
		m.addAttribute("id", id);
		return "listOrder";
	}
	
	@RequestMapping(value="/goods-order", method = RequestMethod.POST)
	public String storageListGoodsOrder(@RequestParam("id") 
	int goodsOrderId, Model m){
		
		List<Goodsorder> listNote = 
				goodsOrderSerivce.getListGoodsOrderIsDone();
		
		String id = "";
		if(goodsOrderId < 0){
			goodsOrderId = 0;
		}
		
		id = id + listNote.get(goodsOrderId).getGoodsOrderId();
		
		m.addAttribute("goodsOrder", listNote
				);
		
		m.addAttribute("detailGoodsOrder", listNote
				.get(goodsOrderId).getDetailgoodorders());
		
		//Gửi mã phiếu để cho chi tiết phiếu biết là của phiếu nhập nào
		m.addAttribute("id", id);
		return "listOrder";
	}
	
	@RequestMapping(value="/goods-issued-note", method = RequestMethod.GET)
	public String storageListGoodsIssuedNote(Model m){
		
		if(ManagerLogin.getPermissionInAccount() == null){
			return "redirect:/account/login";
		}
		else{
			if(ManagerLogin.getPermissionInAccount()){
				List<Goodsissuednote> listNote = 
						exportService.getListIssuedNote();
				int id = 0;
				id = listNote.get(0).getId();
				m.addAttribute("detailIssuedNote", 
						listNote.get(0).getDetailgoodsissuednotes());
				String goodsReceiptNoteId = "";
				if(id > 0){
					goodsReceiptNoteId = goodsReceiptNoteId + id;
				}
				
				m.addAttribute("issuedNote", listNote);
				m.addAttribute("id", id);
				return "listExport";
			}
		}
		
		return "redirect:/"; 
	}
	
	@RequestMapping(value="/goods-issued-note", method = RequestMethod.POST)
	public String storageListGoodsIssuedNote(@RequestParam("id") 
	int goodsIssuedNoteId, Model m){
		
		List<Goodsissuednote> listNote = 
				exportService.getListIssuedNote();
		
		String id = "";
		if(goodsIssuedNoteId < 0){
			goodsIssuedNoteId= 0;
		}
		
		id = id + listNote.get(goodsIssuedNoteId).getId();
		
		m.addAttribute("issuedNote", listNote);
		
		m.addAttribute("detailIssuedNote", listNote
				.get(goodsIssuedNoteId).getDetailgoodsissuednotes());
		
		//Gửi mã phiếu để cho chi tiết phiếu biết là của phiếu nhập nào
		m.addAttribute("id", id);
		return "listExport";
	}
}
