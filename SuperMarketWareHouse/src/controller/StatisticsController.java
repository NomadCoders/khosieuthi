package controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import model.CategoryStatistics;
import service.ExportService;
import service.ImportService;

@Controller
@RequestMapping(value="/statistics")
public class StatisticsController {
	
	@Autowired
	private ImportService importService;
	
	@Autowired
	private ExportService exportService;
	
	//Thống kê Phiếu Nhập
	@RequestMapping(value="/goods-receipt-note", method = RequestMethod.GET)
	public String importStatistics(Model m){
		m.addAttribute("listYear", importService.getArrayYear());
		//check = true là phiếu nhập
		m.addAttribute("check", true);
		return "importStatistics";
	}
	
	@RequestMapping(value="/goods-receipt-note", method = RequestMethod.POST)
	public String checkImportStatistics(@RequestParam("yearSelect")int year,
			@RequestParam("monthSelect")int month, Model m){
		m.addAttribute("listYear", importService.getArrayYear());
		m.addAttribute("check", true);
		
		List<CategoryStatistics> listNoteStatistics = 
				importService.
				getGoodsReceipteNoteStaistics(year, month);
		
		long totalProduct = calculateTotalProduct(listNoteStatistics);
		
		
		m.addAttribute("listNoteStatistics", listNoteStatistics);
		m.addAttribute("totalProduct", totalProduct);
		
		//Gửi lại giá trị năm và tháng đã nhận
		m.addAttribute("yearSelect", year);
		m.addAttribute("monthSelect", month);
		return "importStatistics";
	}
	
	//Thống Kê Phiếu Xuất
	@RequestMapping(value="/goods-issued-note", method = RequestMethod.GET)
	public String exportStatistics(Model m){
		m.addAttribute("listYear", exportService.getArrayYear());
		//check = false là phiếu xuất
		m.addAttribute("check", false);
		return "exportStatistics";
	}
	
	@RequestMapping(value="/goods-issued-note", method = RequestMethod.POST)
	public String checkExportStatistics(@RequestParam("yearSelect")int year,
			@RequestParam("monthSelect")int month, Model m){
		m.addAttribute("listYear", exportService.getArrayYear());
		m.addAttribute("check", false);
		
		List<CategoryStatistics> listNoteStatistics = 
				exportService.getGoodsIssuedeNoteStaistics(year, month);
		
		long totalProduct = calculateTotalProduct(listNoteStatistics);
		
		m.addAttribute("listNoteStatistics", listNoteStatistics);
		m.addAttribute("totalProduct", totalProduct);
		
		//Gửi lại giá trị năm và tháng đã nhận
		m.addAttribute("yearSelect", year);
		m.addAttribute("monthSelect", month);
		return "exportStatistics";
	}
	
	/**
	 * Hàm tính tổng số lượng sản phẩm đã nhập/xuất kho
	 * @param listNoteStatistics
	 * @return tổng số lượng sản phẩm
	 */
	private long calculateTotalProduct(List<CategoryStatistics> 
	listNoteStatistics){
		
		if(listNoteStatistics == null){
			return 0;
		}
		
		long totalProduct = 0;
		for(CategoryStatistics categoryStatistics : listNoteStatistics){
			totalProduct = totalProduct + categoryStatistics.getTotalProducts();
		}
		
		return totalProduct;
	}
}
