package controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.ExportService;
import service.GoodsOrderService;
import service.ImportService;
import utils.ManagerLogin;
import utils.WebUtils;

@Controller
@RequestMapping(value="print")
public class PrintController {
	
	@Autowired
	private ImportService importService;
	
	@Autowired
	private ExportService exportSerivce;
	
	@Autowired
	private GoodsOrderService goodsOrderSerivce;
	
	//Trang in phiếu nhập
	@RequestMapping(value="/goods-receipt-note/{id}", method = RequestMethod.GET)
	public String printGoodsReceiptNote(@PathVariable("id")int id, Model m){
		
		if(id <= 0){
			return "redirect:/error/404";
		}
		
		if(ManagerLogin.getPermissionInAccount() == null){
			return "redirect:/account/login";
		}
		else{
			if(ManagerLogin.getPermissionInAccount()){
				m.addAttribute("note", importService.getGoodsReceiptNote(id));
				m.addAttribute("detailNote", importService.getDetailGoodsReceiptNoteWithId(id));
				return "printReceiptNote";
			}
		}
		
		return "redirect:/";
	}
	
	//Trang in phiếu xuất
	@RequestMapping(value="/goods-issued-note/{id}", method = RequestMethod.GET)
	public String printGoodsIssuedNote(@PathVariable("id")int id, Model m){
		
		if(id <= 0){
			return "redirect:/error/404";
		}
		
		if(ManagerLogin.getPermissionInAccount() == null){
			return "redirect:/account/login";
		}
		else{
			if(ManagerLogin.getPermissionInAccount()){
				m.addAttribute("note", exportSerivce.getGoodsIssuedNote(id));
				m.addAttribute("detailNote", exportSerivce.getDetailGoodsIssuedNoteWithId(id));
				return "printIssuedNote";
			}
		}
		
		return "redirect:/";
	}
	
	@RequestMapping(value="/goods-order/{id}", method = RequestMethod.GET)
	public String printGoodsOrder(@PathVariable("id")int id, Model m){
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		
		if(id <= 0){
			return "redirect:/error/404";
		}
		
		m.addAttribute("note", goodsOrderSerivce.getInforGoodsOrder(id));
		m.addAttribute("detailNote", 
				goodsOrderSerivce.getListDetailGoodsOrderWithId(id));
		return "printGoodsOrder";
	}
	
	//In file pdf của 3 phiếu
	@RequestMapping(value="/download/{fileName}/{id}", method = RequestMethod.POST)
	public void saveFilePdf(@PathVariable("fileName")String fileName, 
			@PathVariable("id")int id, 
			@RequestParam("contentHTML")String contentHTML,
			HttpServletRequest request ,HttpServletResponse response) {
		
		//Nếu không nhận được dữ liệu html, thoát hàm
		if(contentHTML == null){
			return;
		}
		
		//Parse từ html sang pdf
		ByteArrayOutputStream baos = WebUtils.parseXHMTLToPDF(contentHTML, request);
		
		//Tạo tên cho file pdf
		StringBuilder fileNamePDF = new StringBuilder();
		fileNamePDF.append("document.pdf");
		
		//Nếu là phiếu nhập tên file sẽ là PhieuNhapKho1 (với 1 là mã phiếu)
		if(fileName.equals("goods-receipt-note")){
			fileNamePDF.setLength(0);
			fileNamePDF.append("PhieuNhapKho" + id + ".pdf");
		}
		
		//Nếu là phiếu xuất tên file sẽ là PhieuXuatKho1 (với 1 là mã phiếu)
		if(fileName.equals("goods-issued-note")){
			fileNamePDF.setLength(0);
			fileNamePDF.append("PhieuXuatKho" + id + ".pdf");
		}
		
		//Nếu là phiếu đặt hàng tên file sẽ là PhieuDatHang1 (với 1 là mã phiếu)
		if(fileName.equals("goods-order") ){
			fileNamePDF.setLength(0);
			fileNamePDF.append("PhieuDatHang" + id + ".pdf");
		}
		
		response.setContentLength(baos.toByteArray().length);
		response.setContentType("application/pdf");
		response.setHeader("content-disposition", "inline; filename=\""+ fileNamePDF.toString() + "\"");
		response.setCharacterEncoding("UTF-8");
		
		try {
			response.getOutputStream().write(baos.toByteArray());
			response.flushBuffer();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
