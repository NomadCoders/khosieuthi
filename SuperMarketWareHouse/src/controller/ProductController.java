package controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pojo.Categoryproducts;
import pojo.Products;
import service.CategoryService;
import service.ProducerService;
import service.ProductService;
import service.TypeProductService;
import service.UnitCalculateService;
import utils.ManagerLogin;
import validator.ProductValidator;

@Controller
@RequestMapping(value="/products")
public class ProductController {
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private TypeProductService typeProductService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProducerService producerService;
	
	@Autowired
	private UnitCalculateService unitCalculateService;
	
	@Autowired
	private ProductValidator productValidator;
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public String loadAllListProduct(@RequestParam(value="msg", required = false)Integer msg, Model m){
		
		//Kiểm tra đã login, chưa login, đưa về trang đăng nhập
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		
		if(msg != null){
			String message = "";
			if(msg == 1){
				message = "Xóa Sản Phẩm Thành Công";
			}
			
			if(msg == 2){
				message = "Xóa Sản Phẩm Thất Bại!";
			}
			m.addAttribute("msg", message);
		}
		
		m.addAttribute("listProducts", productService.getListProducts(0,0));
		//Load danh mục sản phẩm
		m.addAttribute("listCategory", categoryService.loadListCategory());
		return "product";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public String listProduct(@PathVariable("id")int id, Model m){
		
		//Kiểm tra đã login, chưa login, đưa về trang đăng nhập
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		//Load danh mục sản phẩm
		m.addAttribute("listCategory", categoryService.loadListCategory());
		//Load danh sách sản phẩm
		m.addAttribute("listProducts", productService.getListProducts(id, 0));
		return "product";
	}
	
	@RequestMapping(value="/{id}/{typeId}", method = RequestMethod.GET)
	public String listProductByType(@PathVariable("id")int id, 
			@PathVariable("typeId")int typeId ,Model m){
		//Kiểm tra đã login, chưa login, đưa về trang đăng nhập
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		m.addAttribute("listCategory", categoryService.loadListCategory());
		//Load danh sách sản phẩm
		m.addAttribute("listProducts", productService.getListProducts(id, typeId));
		return "product";
	}
	
	@RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editProduct(@PathVariable("id")int id, 
			 Model m){
		//Kiểm tra đã login, chưa login, đưa về trang đăng nhập
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		
		if(id <= 0){
			return "redirect:/"; 
		}
		
		Products product = productService.getProductById(id);
		
		if(product == null){
			return "redirect:/error/404";
		}
		
		//Load các combobox
		m.addAttribute("product", product);
		m.addAttribute("categoryProduct", categoryService.loadListForComboBox(false));
		m.addAttribute("typeProduct", typeProductService.loadListTypeProductByIdForComboBox(
				product.getCategoryproducts().getCategoryId(), false));
		m.addAttribute("listProducer", producerService.loadListForComboBox());
		m.addAttribute("listUnitCal", unitCalculateService.loadListUnitCalculateForComboBox());
		
		return "editProduct";
	}
	
	@RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String checkEditProduct(@PathVariable("id")int id, 
			@ModelAttribute("product")Products product, 
			BindingResult result, Model m){
		
		productValidator.validate(product, result);
		
		//Load các combobox
		m.addAttribute("categoryProduct", categoryService.loadListForComboBox(false));
		m.addAttribute("typeProduct", typeProductService.loadListTypeProductByIdForComboBox(
				product.getCategoryproducts().getCategoryId(), false));
		m.addAttribute("listProducer", producerService.loadListForComboBox());
		m.addAttribute("listUnitCal", unitCalculateService.loadListUnitCalculateForComboBox());
		
		//Nếu có lỗi thì trở lại trang edit product
		if(result.hasErrors()){			
			return "editProduct";
		}
		
		//Set id cho product
		product.setProductId(id);
		
		//Cập nhập sản phẩm, nếu thành công đưa về trang danh sách sản phẩm
		if(productService.updateProduct(product)){
			return "redirect:/products/" + product
					.getCategoryproducts()
					.getCategoryId() + "/" + product.getTypeproduct()
					.getTypeProductId(); 
		}
		
		//Cập nhập không thành công, có lỗi, đưa về trang edit product
		return "editProduct";
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public String createProduct(Model m){
		
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		
		Products product = new Products();
		product.setStock(0);
		product.setPrice(0);
		product.setStatus(true);
		m.addAttribute("product", product);
		List<Categoryproducts> listcategory = categoryService.loadListForComboBox(false);
		//Load combobox
		m.addAttribute("categoryProduct", listcategory);
		m.addAttribute("typeProduct", typeProductService.loadListTypeProductByIdForComboBox(
				listcategory.get(0).getCategoryId(), false));
		m.addAttribute("listProducer", producerService.loadListForComboBox());
		m.addAttribute("listUnitCal", unitCalculateService.loadListUnitCalculateForComboBox());
		
		return "createProduct";
	}
	
	
	@RequestMapping(value="/create", method = RequestMethod.POST)
	public String checkCreateProduct(@ModelAttribute("product")Products 
			product, BindingResult result, Model m){
		
		productValidator.validate(product, result);
		
		//Load combobox
		m.addAttribute("categoryProduct", categoryService.loadListForComboBox(false));
		m.addAttribute("typeProduct", typeProductService.loadListTypeProductByIdForComboBox(
				product.getCategoryproducts().getCategoryId(), false));
		m.addAttribute("listProducer", producerService.loadListForComboBox());
		m.addAttribute("listUnitCal", unitCalculateService.loadListUnitCalculateForComboBox());
		
		//Kiểm tra các lỗi về input
		if(result.hasErrors()){
			//Lỗi đưa về trang tạo sản phẩm
			return "createProduct";
		}
		
		//Thêm sản phẩm
		if(productService.addProduct(product)){
			//Thêm thành công
			return "redirect:/products/" + product
					.getCategoryproducts()
					.getCategoryId() + "/" + product.getTypeproduct()
					.getTypeProductId(); 
		}
		
		//Thêm sản phẩm thất bại, đưa về trang thêm sản phẩm
		return "createProduct";
	}
	
	@RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public String deleteProduct(@PathVariable("id")int id, Model m){
		int msg = 0;
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		
		if(id > 0){
			if(productService.deleteProduct(id)){
				msg = 1;
			}
			else{
				msg = 0;
			}
			
			m.addAttribute("msg", msg);
		}
		return "redirect:/products/list"; 
	}
	
	/**
	 * Controller cho tìm kiếm sản phẩm
	 * @param keySearch
	 * @return danh sách sản phẩm tìm được
	 */
	@RequestMapping(value="search", method = RequestMethod.GET)
	public String findProducts(@RequestParam("txtKeySearch")String keySearch, Model m){
		//Load danh mục sản phẩm
		m.addAttribute("listCategory", categoryService.loadListCategory());
		//Load danh sách sản phẩm tìm được
		m.addAttribute("listProducts", productService
				.getListProductWithKeySeach(keySearch));
		return "product";
	}
}
