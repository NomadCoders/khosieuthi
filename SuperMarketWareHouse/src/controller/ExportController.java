package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import model.ExportModels;
import model.ProductModels;
import service.CategoryService;
import service.ExportService;
import utils.ManagerLogin;
import validator.GoodsIssuedNoteValidator;

@Controller
@RequestMapping(value="goods-issue-note")
public class ExportController {
		
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ExportService exportService;
	
	@Autowired
	private GoodsIssuedNoteValidator goodsIssuedNoteValidator;
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public String createExport(Model m){
		
		if(ManagerLogin.getPermissionInAccount() == null){
			return "redirect:/account/login";
		}
		else{
			if(ManagerLogin.getPermissionInAccount()){
				ExportModels exportModels = new ExportModels();
				m.addAttribute("exportForm", exportModels);
				m.addAttribute("listCategory",categoryService.loadListForComboBox(true));
				return "export";
			}
		}
		
		return "redirect:/";
	}
	
	@RequestMapping(value="/create", method = RequestMethod.POST)
	public String checkCreateExport(@ModelAttribute("exportForm")ExportModels exportForm, 
			@RequestParam("hidTotalPrice") long totalMoney, 
			BindingResult result,  Model m){
		m.addAttribute("listCategory",categoryService.loadListForComboBox(true));
		StringBuilder msg = new StringBuilder();
		
		goodsIssuedNoteValidator.validate(exportForm, result);
		
		if(result.hasErrors()){
			return "export";
		}
		
		int resultCheck = exportService.addGoodsIssuedNote(exportForm, totalMoney,
				ManagerLogin.getAccountInLogin());
		
		//Tạo phiếu xuất kho thành công
		if(resultCheck == -1){
			return "redirect:/";
		}
		
		//Có sản phẩm không đủ số lượng để xuất kho
		if(resultCheck != -1 && resultCheck != 0){
			String name = "";
			for(ProductModels product : exportForm.getListProducts()){
				if(product.getProductId() == resultCheck){
					name = product.getName();
					break;
				}
			}
			
			msg = msg.append("Sản phẩm ").append(name)
					.append(" không đủ số lượng trong kho!");
		}
		
		m.addAttribute("msg", msg.toString());
		return "export";
	}
}
