package controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import service.CategoryService;
import service.StatisticsService;
import utils.ManagerLogin;

@Controller
@RequestMapping(value = "/")
public class HomeController {
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private StatisticsService statisticsService;
	
	@RequestMapping(value = "/", method= RequestMethod.GET)
	public String index(Model m){
		if(!ManagerLogin.checkIsLogin()){
			return "redirect:/account/login";
		}
		
		m.addAttribute("statistics", statisticsService.getListStatistics());
		m.addAttribute("listCategory", categoryService.loadListCategory());
		return "index";
	}
}
