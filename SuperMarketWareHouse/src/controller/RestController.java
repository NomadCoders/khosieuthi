package controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.ImportModels;
import model.ProductModels;
import pojo.Categoryproducts;
import pojo.Producers;
import pojo.Typeproduct;
import service.CategoryService;
import service.ImportService;
import service.ProducerService;
import service.ProductService;
import service.TypeProductService;

@Controller
@RequestMapping("/rest")
public class RestController {
	
	@Autowired
	private ProducerService producerSerivce;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private TypeProductService typeProductService;
	
	@Autowired
	private ImportService importService;
	
	@Autowired
	private CategoryService categoryService;
	
	
	//Trang GoodsOrder
		/**
		 * SuperMarketWareHouse/rest/infor-producer/1 : trả về địa chỉ và 
		 * số điện thoại có mã là 1 
		 * @param respone
		 * @param id
		 * @return trả về địa chỉ và số điện thoại nhà sản xuất
		 */
		@RequestMapping(value="/infor-producer/{id}", method = RequestMethod.GET)
		@ResponseBody
		public Producers getInforInProducer(
				HttpServletResponse respone, @PathVariable("id")int id){
			Producers producers = producerSerivce.getInforInProducer(id);
			return producers;
		}
		
		/**
		 * SuperMarketWareHouse/rest/category-api/1: trả về danh mục sản phẩm
		 * theo nhà sản xuất 1
		 * @param producerId
		 * @return trả về danh mục sản phẩm theo nhà sản xuất
		 */
		@RequestMapping(value="/category-api/{producerId}", method = RequestMethod.GET)
		@ResponseBody
		public List<Categoryproducts> getListCategoryWithProducer(
				HttpServletResponse respone,
				@PathVariable("producerId")int producerId){
			return categoryService.getListCategoryWithProducer(producerId);
		}
		
		/**
		 * SuperMarketWareHouse/rest/type-products-api/4/5:
		 * Trả về danh sách nhóm hàng theo danh mục 4 và nhà sản xuất 5
		 * @param categoryId
		 * @param producerId
		 * @return trả về danh sách nhóm hàng theo danh mục và nhà sản xuất tương ứng
		 */
		@RequestMapping(value="/type-products-api/{categoryId}/{producerId}", 
				method = RequestMethod.GET)
		@ResponseBody
		public List<Typeproduct> getListTypeProductWithProducer(
				HttpServletResponse respone,
				@PathVariable("categoryId")int categoryId,
				@PathVariable("producerId")int producerId){
			return typeProductService.loadListTypeWithProducer(categoryId, producerId);
		}
		
		@RequestMapping(value="/products-api/{categoryId}/{typeId}/{producerId}", 
				method = RequestMethod.GET)
		@ResponseBody
		public List<ProductModels> getProductWithProducer(HttpServletResponse respone,
				@PathVariable("categoryId")int categoryId,
				@PathVariable("typeId")int typeId,
				@PathVariable("producerId")int producerId){
			List<ProductModels> listProduct = 
					productService.getListProductsWithProducer(categoryId, typeId, producerId);
			return listProduct;
		}
	
	//Trang Export
	/**
	 * SuperMarketWareHouse/rest/type-products-api/1 : trả về 
	 * danh sách loại sản phẩm của danh mục có mã là 1
	 * @param respone
	 * @param categoryId
	 * @return trả về danh sách nhóm hàng theo danh mục cho tìm kiếm
	 */
	@RequestMapping(value="/type-products-api/{categoryId}", method = RequestMethod.GET)
	@ResponseBody
	public List<Typeproduct> getTypeProductInComboBox(
			HttpServletResponse respone,
			@PathVariable("categoryId")int categoryId){
		List<Typeproduct> listType = typeProductService.loadListTypeProductByIdForComboBox(
				categoryId, true);
		return listType;
	}
	
	/**
	 * SuperMarketWareHouse/rest/products-api/0/0 : lấy tất cả sản phẩm
	 * SuperMarketWareHouse/rest/products-api/1/0 : lấy tất cả sản phẩm theo
	 * 												danh mục số 1
	 * SuperMarketWareHouse/rest/products-api/1/1: lấy tất cả sản phẩm theo
	 * 												danh mục số 1 và loại 1
	 * @param respone
	 * @param categoryId
	 * @param typeId
	 * @return trả về danh sách sản phẩm theo danh mục và loại(gồm id và tên)
	 */
	@RequestMapping(value="/products-api/{categoryId}/{typeId}", method = RequestMethod.GET)
	@ResponseBody
	public List<ProductModels> getProductInComboBox(
			HttpServletResponse respone,
			@PathVariable("categoryId")int categoryId,
			@PathVariable("typeId")int typeId){
		List<ProductModels> listProduct = productService.getListForComboBox(categoryId, typeId);
		return listProduct;
	}
	
	
	
	//Trang import
	/**
	 * SuperMarketWareHouse/rest/importmodels-api/1: trả về thông tin đơn hàng
	 * và sản phẩm có trong đơn hàng số 1
	 * @param goodsOrderId
	 * @return trả về thông tin đơn hàng với sản phẩm tương ứng với mã đơn hàng
	 */
	@RequestMapping(value="/importmodels-api/{goodsOrderId}", method = RequestMethod.GET)
	@ResponseBody
	public ImportModels getImportModelWithGoodsOrderId(
			HttpServletResponse respone,
			@PathVariable("goodsOrderId")int goodsOrderId){
		ImportModels importModels = new ImportModels();
		importModels = importService.loadImportModels(goodsOrderId);
		return importModels;
	}
	
	
	//Trang Thêm và chỉnh sửa sản phẩm
	/**
	 * SuperMarketWareHouse/rest/type-products-api/products/1 : trả về 
	 * danh sách loại sản phẩm của danh mục có mã là 1 
	 * @param respone
	 * @param categoryId
	 * @return trả về danh sách loại sản phẩm theo danh mục
	 */
	@RequestMapping(value="/type-products-api/products/{categoryId}", method = RequestMethod.GET)
	@ResponseBody
	public List<Typeproduct> getTypeProductIncomBoxForProduct(
			HttpServletResponse respone,
			@PathVariable("categoryId")int categoryId){
		List<Typeproduct> listType = typeProductService.loadListTypeProductByIdForComboBox(
				categoryId, false);
		return listType;
	}
}
