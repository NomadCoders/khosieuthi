package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

//Controller cho trang lỗi 404
@Controller
@RequestMapping(value="/error")
public class HTTPErrorHandler {
	
	@RequestMapping(value="/404", method = RequestMethod.GET)
	public String error404(){
		return "error404";
	}
}
