package dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.GoodsOrderModels;
import model.ProductModels;
import pojo.Accounts;
import pojo.Detailgoodorder;
import pojo.DetailgoodorderId;
import pojo.Goodsorder;
import pojo.Producers;
import pojo.Products;
import utils.DataProvider;

public class GoodsOrderDAO {
	
	/**
	 * Hàm lấy danh sách đơn hàng chưa hoàn thành
	 * @return danh sách đơn hàng
	 */
	@SuppressWarnings("unchecked")
	public static List<Goodsorder> listGoodOrder(){
		String hql = "Select go.goodsOrderId from Goodsorder go where go.isDone = 0";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Goodsorder> listGoodsOrder = new ArrayList<Goodsorder>();
		List<Object> list = (List<Object>)session.createQuery(hql)
				.getResultList();
		session.getTransaction().commit();
		
		for(Object item : list){
			Goodsorder order = new Goodsorder();
			order.setGoodsOrderId((Integer)item);
			listGoodsOrder.add(order);
		}
		
		return listGoodsOrder;
	}
	
	
	/**
	 * Hàm lấy dan sách đơn hàng đã hoàn thành
	 * @return trả về danh sách đơn hàng
	 */
	public static List<Goodsorder> getListGoodsOrderIsDone(){
		String hql = "Select go from Goodsorder go "
				+ "where go.isDone = 1";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Goodsorder> listGoodsOrder = session.createQuery(hql,
				Goodsorder.class).getResultList();
		session.getTransaction().commit();
		
		return listGoodsOrder;
	}
	
	/**
	 * Hàm lấy chi tiết phiếu đặt hàng với mã đặt hàng
	 * @param id
	 * @return trả về danh sách chi tiết phiếu đặt hàng
	 */
	public static List<Detailgoodorder> getListDetailGoodsOrderWithId(int id){
		String hql = "Select dgo from Detailgoodorder dgo "
				+ "where dgo.id.goodsOrderId = :id";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Detailgoodorder> listNote = session.createQuery(hql,
				Detailgoodorder.class)
				.setParameter("id", id)
				.getResultList();
		session.getTransaction().commit();
		
		return listNote;
	}
	
	/**
	 * Hàm lấy phiếu đặt hàng với mã đặt hàng tương ứng
	 * @param goodsOrderId
	 * @return trả về phiếu đặt hàng
	 */
	public static Goodsorder getInforGoodsOrder(int goodsOrderId ){
		if(goodsOrderId == 0){
			return null;
		}
		
		String hql = "Select go from Goodsorder go where go.goodsOrderId"
				+ " =:goodsOrderId";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Goodsorder> listGoodsOrder = session.createQuery(hql,Goodsorder.class)
				.setParameter("goodsOrderId", goodsOrderId).getResultList();
		
		if(listGoodsOrder != null){
			return listGoodsOrder.get(0);
		}
		session.getTransaction().commit();
		
		return null;
	}
	
	/**
	 * Hàm cập nhập tình trạng phiếu đặt hàng thành hoàn thành
	 * @param goodsOrderId
	 * @return
	 */
	public static Boolean updateGoodsOrderToDone(int goodsOrderId){
		String hql = "Update Goodsorder go set isDone = 1 "
				+ "where goodsOrderId = :goodsOrderId";
		
		Session session = DataProvider.getSessionFactory().openSession();
		try {
			session.getTransaction().begin();
			session.createQuery(hql)
			.setParameter("goodsOrderId", goodsOrderId).executeUpdate();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		}finally {
			session.close();
		}
		
		return true;
	}
	
	
	/**
	 * Hàm insert phiếu đặt hàng
	 * @param goodsOrderModels
	 * @param totalMoney
	 * @param account
	 * @return trả về true khi thành công, false khi thất bại
	 */
	public static Boolean addGoodsOrder(GoodsOrderModels goodsOrderModels,
			long totalMoney, Accounts account){
		if(goodsOrderModels == null){
			return false;
		}
		
		Producers producers = new Producers();
		producers.setProducerId(goodsOrderModels.getProducerId());
		
		//Insert phiếu đặt hàng
		Goodsorder goodsorder = new Goodsorder();
		goodsorder.setProducers(producers);
		goodsorder.setAccounts(account);
		goodsorder.setAddressWareHouse(goodsOrderModels.getAddressWareHouse());
		goodsorder.setDateCreate(new Date());
		goodsorder.setEmail(goodsOrderModels.getEmail());
		goodsorder.setTotalMoney(totalMoney);
		goodsorder.setNote(goodsorder.getNote());
		goodsorder.setIsDone(false);
		
		Session session = DataProvider.getSessionFactory().openSession();
		try {
			session.getTransaction().begin();
			session.save(goodsorder);
			int id = goodsorder.getGoodsOrderId();
			long totalQuantity = 0;
			
			//Insert chi tiết phiếu đặt hàng
			for(ProductModels product : goodsOrderModels.getListProducts()){
				int quantity = product.getQuantity();
				totalQuantity = totalQuantity + quantity;
				//Tạo id cho detail goods order
				DetailgoodorderId detailgoodorderId = new DetailgoodorderId(
						id, product.getProductId());
				
				Products productOrder = new Products();
				productOrder.setProductId(product.getProductId());
				Detailgoodorder detailgoodorder = new Detailgoodorder();
				detailgoodorder.setId(detailgoodorderId);
				detailgoodorder.setProducts(productOrder);
				detailgoodorder.setQuantity(quantity);
				detailgoodorder.setPrice(product.getPrice());
				detailgoodorder.setTotalMoney(product.getTotalMoney());
				
				session.save(detailgoodorder);
			}
			goodsorder.setTotalQuantity(totalQuantity);
			session.update(goodsorder);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		}finally {
			session.close();
		}
		return true;
	}
}
