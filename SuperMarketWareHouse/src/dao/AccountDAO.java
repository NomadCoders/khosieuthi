package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.ChangePassword;
import model.RegisterModels;
import pojo.Accounts;
import utils.DataProvider;
import utils.WebUtils;

public class AccountDAO {
	
	/**
	 * Hàm thêm tài khoản
	 * @param registerUser
	 * @return trả về true nếu thêm thành công, false nếu thất bại
	 */
	public static Boolean addAccount(RegisterModels registerUser){
		if(registerUser != null){
			Accounts user = new Accounts();
			user.setName(registerUser.getName());
			user.setPassword(registerUser.getPassword());
			user.setFullName(registerUser.getFullName());
			user.setAddress(registerUser.getAddress());
			user.setEmail(registerUser.getEmail());
			user.setPhoneNumber(registerUser.getPhoneNumber());
			user.setPermission(false);
			user.setStatus(true);
			//Mã hóa code với md5
			user.setPassword(WebUtils.cryptWithMD5(user.getPassword()));
			
			Session session = DataProvider.getSessionFactory().openSession();
			try {
				session.getTransaction().begin();
				session.save(user);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				session.getTransaction().rollback();
			}
			finally {
				session.close();
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Hàm lấy thông tin account với userName
	 * @param userName
	 * @return trả về Accounts nếu có giá trị, trả về null nếu không tìm thấy
	 */
	public static Accounts getAccountWithUserName(String userName){
		if(userName == null){
			return null;
		}
		
		String hql = "Select a from Accounts a where a.name =:nameLogin";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Accounts> accounts = session.createQuery(hql, Accounts.class)
				.setParameter("nameLogin", userName).getResultList();
		session.getTransaction().commit();
		if(accounts.isEmpty()){
			return null;
		}
		return accounts.get(0);
	}
	
	/**
	 * Hàm kiểm tra login tài khoản
	 * @param nameLogin
	 * @param password
	 * @return trả về accounts nếu login thành công, thất bại trả về null
	 */
	public static Accounts Login(String nameLogin, String password){
		if(!password.equals("") && !nameLogin.equals("")){
			String hql = "Select a from Accounts a where a.name =:nameLogin And "
					+ "a.password =:passwordHash";
			
			Session session = DataProvider.getSessionFactory().openSession();
			session.getTransaction().begin();
			
			List<Accounts> accounts = session.createQuery(hql, Accounts.class)
					.setParameter("nameLogin", nameLogin)
					.setParameter("passwordHash", WebUtils.
							cryptWithMD5(password)).getResultList();
			
			session.getTransaction().commit();
			
			if(accounts.isEmpty()){
				return null;
			}
			return accounts.get(0);
		}
		return null;
	}
	
	/**
	 *  Hàm kiểm tra username có đăng ký rồi hay không
	 * @param userName
	 * @return trả về true, false
	 */
	public static Boolean checkUniqueUserName(String userName){
		if(userName == null){
			return null;
		}
		
		String hql = "Select count(*) from Accounts a where "
				+ "a.name =:nameLogin";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		
		int count = ((Long)session.createQuery(hql)
				.setParameter("nameLogin", userName)
				.getSingleResult()).intValue();
		session.getTransaction().commit();
		if(count == 0){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Hàm kiểm tra email đã có được đăng ký hay chưa
	 * @param email
	 * @return trả về true nếu chưa đăng ký, trả về false email đã được
	 * đang ký
	 */
	public static Boolean checkUniqueEmail(String email){
		if(email == null){
			return null;
		}
		
		String hql = "Select count(*) from Accounts a where "
				+ "a.email =:email";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		
		int count = ((Long)session.createQuery(hql)
				.setParameter("email", email)
				.getSingleResult()).intValue();
		session.getTransaction().commit();
		if(count == 0){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Hàm check email đã được tài khoản khác sử dụng chưa (dùng 
	 * khi user cập nhập thông tin)
	 * @param email
	 * @param idAccount
	 * @return trả về true nếu chưa đăng ký, trả về false email đã được
	 * đang ký
	 */
	public static Boolean checkUniqueEmail(String email, String userName){
		if(email == null){
			return null;
		}
		
		String hql = "Select count(*) from Accounts a where "
				+ "a.email =:email And a.name <> :userName";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		
		int count = ((Long)session.createQuery(hql)
				.setParameter("email", email)
				.setParameter("userName", userName)
				.getSingleResult()).intValue();
		session.getTransaction().commit();
		if(count == 0){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Hàm cập nhập thông tin tài khoản
	 * @param account
	 * @return trả về true nếu cập nhập thành công, trả về false, cập nhập
	 * thất bại
	 */
	public static Boolean updateAccountInfor(Accounts account){
		Session session = DataProvider.getSessionFactory().openSession();
		try {
			session.getTransaction().begin();
			session.update(account);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		}finally {
			session.close();
		}
		return true;
	}
	
	public static long getTotalNumberAccount(){
		String hql = "Select COUNT(*) from Accounts";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		long totalNumberAccount = ((Long)session.createQuery(hql)
				.getSingleResult()).longValue();
		session.getTransaction().commit();
		
		return totalNumberAccount;
	}
	
	/**
	 * Hàm lấy danh sách tài khoản
	 * @return
	 */
	public static List<Accounts> getListAccount(){
		String hql = "Select a From Accounts a where a.isDelete = 0";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Accounts> listAccount = session.createQuery(hql, Accounts.class)
				.getResultList();
		session.getTransaction().commit();
		
		return listAccount;
	}
	
	/**
	 * Hàm lấy thông tin tài khoản bằng id
	 * @param id
	 * @return thông tin tài khoản
	 */
	public static Accounts getAccountById(int id){
		String hql = "Select a from Accounts a where a.id =:id "
				+ "And a.isDelete = 0";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Accounts> listAccounts = session.createQuery(hql, Accounts.class)
				.setParameter("id", id).getResultList();
		session.getTransaction().commit();
		if(listAccounts.size() != 0){
			return listAccounts.get(0);
		}
		return null;
	}
	
	public static Boolean deleteAccount(int id){
		String hql = "Update Accounts a set a.isDelete = 1"
				+ " where a.id = :id";
		
		Session session = DataProvider.getSessionFactory().openSession();
		try {
			session.getTransaction().begin();
			int count = session.createQuery(hql)
			.setParameter("id", id)
			.executeUpdate();
			session.getTransaction().commit();
			
			if(count > 0){
				return true;
			}
			
			return false;
			
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		}finally {
			session.close();
		}
	}
	
	
	/**
	 * Hàm thay đổi mật khẩu
	 * @param changePassowrd
	 * @param userName
	 * @return
	 */
	public static Boolean changePassowrd(ChangePassword changePassowrd,
			 String userName){
		String hql = "Update Accounts a set a.password = :newPassword "
				+ "where a.password = :password And a.name = :userName";
		
		Session session = DataProvider.getSessionFactory().openSession();
		
		try {
			session.getTransaction().begin();
			int count = session.createQuery(hql)
			.setParameter("newPassword", WebUtils.cryptWithMD5(
					changePassowrd.getNewPassword()))
			.setParameter("password", WebUtils.cryptWithMD5(
					changePassowrd.getCurrentPassword()))
			.setParameter("userName", userName)
			.executeUpdate();
			session.getTransaction().commit();
			if(count > 0){
				return true;
			}
			
			return false;
			
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		}finally {
			session.close();
		}
	}
}
