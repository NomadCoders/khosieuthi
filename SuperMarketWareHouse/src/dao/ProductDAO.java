package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.ProductModels;
import pojo.Products;
import pojo.Viewproducts;
import pojo.ViewproductsId;
import utils.DataProvider;

public class ProductDAO {
	/**
	 * 
	 * @param categoryId 
	 * @param typeProductId = 0 sẽ lấy sản phẩm không phân biết loại hàng
	 * @return danh sách sản phẩm
	 */
	public static List<Viewproducts> getListProducts(int categoryId, int typeId){
		
		String hql;
		List<Viewproducts> listProduct = null;
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		
		if(categoryId == 0){
			hql = "Select p From Viewproducts p ";
			listProduct = session.createQuery(hql, Viewproducts.class).getResultList();
		}
		
		if(typeId == 0 && categoryId != 0){
			hql = "Select p From Viewproducts p where p.id.categoryId = :categoryId";
			listProduct = session.createQuery(hql, Viewproducts.class)
					.setParameter("categoryId", categoryId).getResultList();
		}
		
		if(typeId != 0 && categoryId != 0){
			hql = "Select p From Viewproducts p where p.id.typeProductId =:typeId"
					+ " And p.id.categoryId =:categoryId";
			listProduct = session.createQuery(hql, Viewproducts.class)
					.setParameter("typeId", typeId)
					.setParameter("categoryId", categoryId).getResultList();
		}
		
		session.getTransaction().commit();
		return listProduct;
	}
	
	/**
	 * Hàm để load combo box
	 * @param categoryId
	 * @param typeId
	 * @return trả về danh sách sản phẩm theo danh mục và loại
	 */
	@SuppressWarnings("unchecked")
	public static List<Viewproducts> getListForComboBox(int categoryId, int typeId){
		String hql;
		List<Object[]> listProduct = null;
		List<Viewproducts> list = new ArrayList<Viewproducts>();
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		if(categoryId == 0){
			hql = "Select p.id.productId, p.id.name, p.id.price, "
					+ "p.id.nameUnitCalculate From Viewproducts p";
			listProduct = session.createQuery(hql).getResultList();
		}
		
		if(typeId == 0 && categoryId != 0){
			hql = "Select p.id.productId, p.id.name, p.id.price, "
					+ "p.id.nameUnitCalculate From Viewproducts p where "
					+ "p.id.categoryId = :categoryId";
			listProduct = session.createQuery(hql)
					.setParameter("categoryId", categoryId).getResultList();
		}
		
		if(typeId != 0 && categoryId != 0){
			hql = "Select p.id.productId, p.id.name, p.id.price, "
					+ "p.id.nameUnitCalculate From Viewproducts p where "
					+ "p.id.typeProductId =:typeId"
					+ " And p.id.categoryId =:categoryId";
			listProduct = session.createQuery(hql)
					.setParameter("typeId", typeId)
					.setParameter("categoryId", categoryId).getResultList();
		}
		
		session.getTransaction().commit();
		
		for(Object[]item : listProduct){
			Viewproducts product = new Viewproducts();
			ViewproductsId id = new ViewproductsId();
			id.setProductId((Integer)item[0]);
			id.setName((String)item[1]);
			id.setPrice((Integer)item[2]);
			id.setNameUnitCalculate((String)item[3]);
			product.setId(id);
			list.add(product);
		}
		return list;
	}
	
	/**
	 * 
	 * @param categoryId
	 * @param typeId
	 * @param producerId
	 * @return trả về danh sách sản phẩm theo danh mục, nhóm và nhà
	 * sản xuất tương ứng
	 */
	@SuppressWarnings("unchecked")
	public static List<Viewproducts> getListProductsWithProducer(
			int categoryId, int typeId, int producerId){
		
		if(producerId == 0){
			return new ArrayList<Viewproducts>();
		}
		
		String hql;
		List<Object[]> listProduct = null;
		List<Viewproducts> list = new ArrayList<Viewproducts>();
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		if(categoryId == 0){
			hql = "Select p.id.productId, p.id.name, p.id.price, "
					+ "p.id.nameUnitCalculate From Viewproducts p"
					+ " where p.id.producerId = :producerId";
			listProduct = (List<Object[]>) session.createQuery(hql)
					.setParameter("producerId", producerId)
					.getResultList();
		}
		
		if(typeId == 0 && categoryId != 0){
			hql = "Select p.id.productId, p.id.name, p.id.price, "
					+ "p.id.nameUnitCalculate From Viewproducts p where "
					+ "p.id.categoryId = :categoryId and p.id.producerId"
					+ " = :producerId";
			listProduct = session.createQuery(hql)
					.setParameter("categoryId", categoryId)
					.setParameter("producerId", producerId)
					.getResultList();
		}
		
		if(typeId != 0 && categoryId != 0){
			hql = "Select p.id.productId, p.id.name, p.id.price, "
					+ "p.id.nameUnitCalculate From Viewproducts p where "
					+ "p.id.typeProductId =:typeId"
					+ " And p.id.categoryId =:categoryId"
					+ " And p.id.producerId = :producerId";
			listProduct = session.createQuery(hql)
					.setParameter("typeId", typeId)
					.setParameter("categoryId", categoryId)
					.setParameter("producerId", producerId)
					.getResultList();
		}
		
		session.getTransaction().commit();
		
		for(Object[]item : listProduct){
			Viewproducts product = new Viewproducts();
			ViewproductsId id = new ViewproductsId();
			id.setProductId((Integer)item[0]);
			id.setName((String)item[1]);
			id.setPrice((Integer)item[2]);
			id.setNameUnitCalculate((String)item[3]);
			product.setId(id);
			list.add(product);
		}
		return list;
	}
	
	/**
	 * 
	 * @param goodsOrderId
	 * @return trả về tổng số lượng sản phẩm của từng danh mục
	 */
	@SuppressWarnings("unchecked")
	public static List<ProductModels> getListProductInGoodsOrder(int goodsOrderId){
		String hql = "SELECT p.ProductId, p.Name, p.Price, dg.Quantity, "
				+ "uc.Name as 'nameUnitCalculate' "
				+ "From detailgoodorder dg, products p, "
				+ "unitcalculate uc where dg.GoodsOrderId = :goodsOrderId "
				+ "And dg.ProductId = p.ProductId And p.UnitCalculateId = uc.id";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		
		List<Object[]> list = session.createNativeQuery(hql)
				.setParameter("goodsOrderId", goodsOrderId)
				.getResultList();
		session.getTransaction().commit();
		
		List<ProductModels> listProduct = new ArrayList<ProductModels>();
		
		for(Object[] item : list){
			ProductModels productModel = new ProductModels();
			productModel.setProductId((Integer)item[0]);
			productModel.setName((String)item[1]);
			productModel.setPrice((Integer)item[2]);
			productModel.setQuantity((Integer)item[3]);
			productModel.setNameUnitCalculate((String)item[4]);
			productModel.setTotalMoney(productModel.getPrice() * productModel.getQuantity());
			
			listProduct.add(productModel);
		}
		return listProduct;
	}
	
	public static Products getProductById(int productId){
		if(productId == 0){
			return null;
		}
		
		String hql = "Select p from Products p "
				+ "where p.productId = :productId And p.isDelete = 0";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Products> listProduct = session.createQuery(hql,Products.class)
				.setParameter("productId", productId).getResultList();
		session.getTransaction().commit();
		
		if(listProduct != null){
			return listProduct.get(0);
		}
		
		return null;
	}
	
	public static Boolean updateProduct(Products product){
		if(product == null){
			return false;
		}
		product.setIsDelete(false);
		Session session = DataProvider.getSessionFactory().openSession();
		try {
			session.getTransaction().begin();
			session.update(product);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		}finally {
			session.close();
		}
		
		return true;
	}
	
	/**
	 * Hàm thêm sản phẩm
	 * @param product
	 * @return true, false
	 */
	public static boolean addProduct(Products product){
		if(product == null){
			return false;
		}
		product.setIsDelete(false);
		Session session = DataProvider.getSessionFactory().openSession();
		try {
			session.getTransaction().begin();
			session.save(product);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		}finally {
			session.close();
		}
		
		return true;
	}
	
	/**
	 * Hàm xóa sản phẩm
	 * @param productId
	 * @return true nếu thành công, false xóa thất bại
	 */
	public static Boolean deleteProduct(int productId){
		String hql = "Update Products p set p.isDelete = 1 "
				+ "where p.productId = :productId";
		Session session = DataProvider.getSessionFactory().openSession();
		try {
			session.getTransaction().begin();
			int count = session.createQuery(hql)
			.setParameter("productId", productId)
			.executeUpdate();
			session.getTransaction().commit();
			
			if(count > 0){
				return true;
			}
			
			return false;
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		}finally {
			session.close();
		}
	}
	
	/**
	 * Hàm đếm số lượng sản phẩm
	 * @return trả về tổng số lượng sản phẩm có trong kho
	 */
	public static long getTotalNumberProduct(){
		String hql = "Select SUM(p.stock) from Products p where "
				+ "p.isDelete = 0";
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		long totalProducts = ((Long)session.createQuery(hql)
				.getSingleResult()).longValue();
		session.getTransaction().commit();
		
		return totalProducts;
	}
	
	/**
	 * Hàm cho tìm kiếm sản phẩm
	 * @param keySearch
	 * @return
	 */
	public static List<Viewproducts> getListProductWithKeySeach(String keySearch){
		if(keySearch.trim() == ""){
			return new ArrayList<Viewproducts>();
		}
		String hql = "Select p from Viewproducts p where "
				+ "p.id.name like :name or p.id.nameTypeProduct "
				+ "like :typeproduct or p.id.nameCategoryProduct "
				+ "like :category or p.id.nameUnitCalculate like :unit "
				+ "or p.id.price <= :price";
		int price = 0;
		try {
			price = Integer.parseInt(keySearch);
		} catch (NumberFormatException e) {
			// TODO: handle exception
		}
		
		keySearch = "%" + keySearch + "%";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Viewproducts> listProduct = session.createQuery(hql, Viewproducts.class)
				.setParameter("name", keySearch)
				.setParameter("typeproduct", keySearch)
				.setParameter("category", keySearch)
				.setParameter("unit", keySearch)
				.setParameter("price", price).getResultList();
		session.getTransaction().commit();
		
		return listProduct;
	}
	
	
	public static Products findProductById(int productId){
		String hql = "Select p from Products p where p.productId = :id";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Products> listProduct = session.createQuery(hql, Products.class)
				.setParameter("id", productId).getResultList();
		session.getTransaction().commit();
		if(listProduct.size() > 0){
			session.close();
			return listProduct.get(0);
		}
		
		return null;
		
	}
	
	/**
	 * Hàm tăng số lượng sản phẩm theo phiếu nhập kho
	 * @param listProducts
	 * @param session
	 * @throws HibernateException
	 */
	public static void updateIncreaseProductWithGoodsReceiptNote(
			List<ProductModels> listProducts, Session session)
	throws HibernateException{
		
		for(ProductModels productModel : listProducts){
			Products product = findProductById(productModel.getProductId());
			if(product != null){
				product.setStock(product.getStock() + productModel.getQuantity());
				session.update(product);
			}
			
		}
		
	}
	
	/**
	 * Hàm giảm số lượng sản phẩm theo phiếu xuất kho
	 * @param productModel
	 * @param session
	 * @return thành công trả về true, thất bại trả về false
	 * @throws HibernateException
	 */
	public static Boolean updateDecreaseProductWithGoodsIssuedNote(
			ProductModels productModel, Session session)
		throws HibernateException{
		
		Products product = findProductById(productModel.getProductId());
		if(product == null){
			return false;
		}
		int stock = product.getStock() - productModel.getQuantity();
		
		//Nếu số lượng <0, số lượng xuất kho nhiều hơn số lượng tồn
		if(stock < 0){
			return false;
		}
		
		product.setStock(stock);
		session.update(product);
		return true;
	}
}
