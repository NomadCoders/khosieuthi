package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;


import pojo.Unitcalculate;
import utils.DataProvider;

public class UnitCalculateDAO {
	
	/**
	 * Hàm dùng để load combobox
	 * @return danh sách các loại đơn vị (gồm id và tên)
	 */
	@SuppressWarnings("unchecked")
	public static List<Unitcalculate> loadListUnitCalculateForComboBox(){
		String hql = "Select u.id, u.name From Unitcalculate u "
				+ "where u.isDelete = 0";
		List<Unitcalculate> list = new ArrayList<Unitcalculate>();
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		
		List<Object[]> listUnitCalculate = (List<Object[]>)session
				.createQuery(hql).getResultList();
		
		session.getTransaction().commit();
		
		for(Object[] item : listUnitCalculate ){
			Unitcalculate unitCalculate = new Unitcalculate();
			unitCalculate.setId((Integer)item[0]);
			unitCalculate.setName((String)item[1]);
			
			list.add(unitCalculate);
		}
		
		return list;
	}
}
