package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import pojo.Producers;
import utils.DataProvider;

public class ProducerDAO {
	
	/**
	 * Hàm để load combo box
	 * @return Danh sách danh mục sản phẩm
	 */
	public static List<Producers> loadListForComboBox(){
		String hql = "Select p.producerId, p.name from Producers as p"
				+ " where p.isDelete = 0 And p.producersstatus.name like :name";
		String name = "%Đang hoạt động%";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		@SuppressWarnings("unchecked")
		List<Object[]> listProducer = (List<Object[]>)session.createQuery(hql)
				.setParameter("name", name).getResultList();
		
		session.getTransaction().commit();
		
		List<Producers> list = new ArrayList<Producers>();
		for(Object[] item : listProducer){
			Producers producer = new Producers();
			producer.setProducerId((Integer)item[0]);
			producer.setName((String)item[1]);
			
			list.add(producer);
		}
		return list;
	}
	
	
	/**
	 * 
	 * @param producerId
	 * @return Trả về địa chỉ, số điện nhà sản xuất
	 */
	@SuppressWarnings("unchecked")
	public static Producers getInforInProducer(int producerId){
		String hql = "Select p.address, p.phoneNumber from Producers p "
				+ "where p.producerId = :producerId And p.isDelete = 0";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		Producers producer = new Producers();
		List<Object[]> listInfor =  (List<Object[]>)session.createQuery(hql)
				.setParameter("producerId", producerId).getResultList();
		session.getTransaction().commit();
		
		if(listInfor != null){
			producer.setAddress((String)listInfor.get(0)[0]);
			producer.setPhoneNumber((String)listInfor.get(0)[1]);
		}
		
		return producer;
	}
}
