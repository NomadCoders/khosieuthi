package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import model.CategoryStatistics;
import pojo.Categoryproducts;
import utils.DataProvider;

public class CategoryProductDAO {
	
	/**
	 * 
	 * @return Danh sách danh mục và loại sản phẩm tương ứng danh mục đó
	 */
	public static List<Categoryproducts> loadListCategory(){
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		String hql = "Select cp from Categoryproducts as cp"
				+ " where cp.isDelete = 0";
	
		List<Categoryproducts> listCategory = session.createQuery(hql, 
				Categoryproducts.class).getResultList();
		
		session.getTransaction().commit();
		return listCategory;
	}
	
	/**
	 * Hàm để load combobox khi tìm kiếm
	 * @param chooseMode: true: lấy danh sách cho tìm kiếm
	 * 					  false: lấy danh sách cho thêm sản phẩm 
	 * @return Danh sách danh mục sản phẩm
	 */
	@SuppressWarnings("unchecked")
	public static List<Categoryproducts> loadListForComboBox(Boolean chooseMode){
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		String hql = "Select cp.categoryId, cp.name from Categoryproducts as cp"
				+ " where cp.isDelete = 0";
	
		
		List<Object[]> listCategory = (List<Object[]>)session.createQuery(hql)
				.getResultList();
		
		session.getTransaction().commit();
		
		List<Categoryproducts> list = new ArrayList<Categoryproducts>();
		
		//Thêm thành phần tất cả nếu chooseMode = true
		if(chooseMode){
			Categoryproducts categroy = new Categoryproducts();
			categroy.setCategoryId(0);
			categroy.setName("Tất cả");
			list.add(categroy);
		}
		
		for(Object[] item : listCategory){
			Categoryproducts category = new Categoryproducts();
			category.setCategoryId((Integer)item[0]);
			category.setName((String)item[1]);
			
			list.add(category);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Categoryproducts> getListCategoryWithProducer(int producerId){
		
		if(producerId == 0){
			return new ArrayList<Categoryproducts>();
		}
		
		List<Categoryproducts> list = new ArrayList<Categoryproducts>();
		
		Categoryproducts category = new Categoryproducts();
		category.setCategoryId(0);
		category.setName("Tất Cả");
		list.add(category);
		
		String hql = "Select c.categoryId, c.name from Categoryproducts c"
				+ " where c.isDelete = 0 And c.categoryId in ("
				+ "Select p.categoryproducts.categoryId "
				+ "from c.productses p where p.categoryproducts.categoryId"
				+ " = c.categoryId And p.producers.producerId = :producerId"
				+ " And p.isDelete = 0)";
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		
		List<Object[]> listCategory = (List<Object[]>)session.createQuery(hql)
				.setParameter("producerId", producerId)
				.getResultList();
		session.getTransaction().commit();
		
		for(Object[] item : listCategory){
			Categoryproducts categoryproducts = new Categoryproducts();
			categoryproducts.setCategoryId((Integer)item[0]);
			categoryproducts.setName((String)item[1]);
			
			list.add(categoryproducts);
		}
		
		return list;
	}
	
	/**
	 * 
	 * @return trả về tổng số lượng danh mục có trong kho
	 */
	public static long getTotalNumberCategory(){
		String hql = "Select COUNT(*) From Categoryproducts cp where"
				+ " cp.isDelete = 0";
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		long totalCategory = ((Long)session.createQuery(hql)
				.getSingleResult()).longValue();
		session.getTransaction().commit();
		
		return totalCategory;
	}
	
	@SuppressWarnings("unchecked")
	public static List<CategoryStatistics> getTotalNumberProductWithEachCategory(){
		String hql = "Select cp.categoryId, cp.name, SUM(p.stock) "
				+ "From Categoryproducts cp join Products p "
				+ "on cp.categoryId = p.categoryproducts.categoryId where "
				+ "cp.isDelete = 0 And p.isDelete = 0"
				+ "group by cp.categoryId, cp.name";
		
		List<CategoryStatistics> listcategory = new ArrayList<CategoryStatistics>();
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Object[]> list = (List<Object[]>)session.createQuery(hql)
				.getResultList();
		session.getTransaction().commit();
		
		for(Object[] item : list){
			CategoryStatistics categoryStatistics = new CategoryStatistics();
			categoryStatistics.setCategroyId((Integer)item[0]);
			categoryStatistics.setNameCategory((String)item[1]);
			categoryStatistics.setTotalProducts((Long)item[2]);
			
			listcategory.add(categoryStatistics);
		}
		
		return listcategory;
	}
	
	/**
	 * Hàm lấy danh sách loại sản phẩm có trong phiếu nhập kho
	 * @param yearChoose
	 * @param monthChoose
	 * @return trả về danh sách loại sản phẩm (gồm id và tên)
	 */
	@SuppressWarnings("unchecked")
	public static List<Categoryproducts> getListCategoryInGoodsIssuedNote(
			int yearChoose, int monthChoose){
		StringBuilder hql = new StringBuilder();
		hql.append("Select DISTINCT p.categoryproducts.categoryId, p.categoryproducts.name "
				+ "from Products p where p.productId in "
				+ "(Select dgin.id.productId from p.detailgoodsissuednotes dgin "
				+ "where YEAR(dgin.goodsissuednote.dateCreate) = :yearChoose");
		
		if(monthChoose > 0){
			hql.append(" And MONTH(dgin.goodsissuednote.dateCreate) = :monthChoose)");
		}
		else{
			//Truy vấn lấy tất cả danh sách loại, đóng ngoặc câu truy vấn lồng
			hql.append(")");
		}
		
		Session session =  DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		
		Query<Object[]> query = session.createQuery(hql.toString())
				.setParameter("yearChoose", yearChoose);
		
		if(monthChoose > 0){
			query.setParameter("monthChoose", monthChoose);
		}
		
		List<Object[]> listObject = query.getResultList();
		List<Categoryproducts> listCategory = new ArrayList<Categoryproducts>();
		
		for(Object[] item : listObject){
			Categoryproducts categoryproducts = new Categoryproducts();
			categoryproducts.setCategoryId((Integer)item[0]);
			categoryproducts.setName((String)item[1]);
			
			listCategory.add(categoryproducts);
		}
		
		session.getTransaction().commit();
		
		return listCategory;
	}
	
	
	@SuppressWarnings("unchecked")
	public static List<Categoryproducts> getListCategoryInGoodsReceiptNote(
			int yearChoose, int monthChoose){
		StringBuilder hql = new StringBuilder();
		hql.append("Select DISTINCT p.categoryproducts.categoryId, p.categoryproducts.name "
				+ "from Products p where p.productId in "
				+ "(Select dgo.id.productId from p.detailgoodorders dgo "
				+ "where dgo.id.productId in "
				+ "(Select dgo.id.productId from dgo.detailgoodsreceiptnotes dgrn"
				+ " where YEAR(dgrn.goodsreceiptnote.dateCreate) = :yearChoose");
		
		if(monthChoose > 0){
			hql.append(" And MONTH(dgrn.goodsreceiptnote.dateCreate) = :monthChoose))");
		}
		else{
			//Truy vấn lấy tất cả danh sách loại, đóng ngoặc câu truy vấn lồng
			hql.append("))");
		}
		
		Session session =  DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		
		Query<Object[]> query = session.createQuery(hql.toString())
				.setParameter("yearChoose", yearChoose);
		
		if(monthChoose > 0){
			query.setParameter("monthChoose", monthChoose);
		}
		
		List<Object[]> listObject = query.getResultList();
		List<Categoryproducts> listCategory = new ArrayList<Categoryproducts>();
		
		for(Object[] item : listObject){
			Categoryproducts categoryproducts = new Categoryproducts();
			categoryproducts.setCategoryId((Integer)item[0]);
			categoryproducts.setName((String)item[1]);
			
			listCategory.add(categoryproducts);
		}
		
		session.getTransaction().commit();
		
		return listCategory;
	}
}
