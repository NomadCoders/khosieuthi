package dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.CategoryStatistics;
import model.ExportModels;
import model.ProductModels;
import model.TypeProductStatistics;
import pojo.Accounts;
import pojo.Categoryproducts;
import pojo.Detailgoodsissuednote;
import pojo.DetailgoodsissuednoteId;
import pojo.Goodsissuednote;
import pojo.Products;
import utils.DataProvider;

public class GoodsIssuedNoteDAO {
	
	/**
	 * Hàm lấy danh sách phiếu xuất kho
	 * @return
	 */
	public static List<Goodsissuednote> getListIssuedNote(){
		String hql = "Select gsn from Goodsissuednote gsn";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Goodsissuednote> listNote = session.createQuery(hql,
				Goodsissuednote.class).getResultList();
		session.getTransaction().commit();
		
		return listNote;
	}
	
	/**
	 * Hàm lấy phiếu xuất kho với mã phiếu xuất kho tương ứng
	 * @param id
	 * @return
	 */
	public static Goodsissuednote getGoodsIssuedNote(int id){
		String hql = "Select gsn from Goodsissuednote gsn "
				+ "where gsn.id = :id";
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Goodsissuednote> listNote = session.createQuery(hql, 
				Goodsissuednote.class)
				.setParameter("id", id).getResultList();
		session.getTransaction().commit();
		
		if(listNote.size() > 0){
			return listNote.get(0);
		}
		
		return null;
	}
	
	/**
	 * Hàm lấy chi tiết phiếu xuất kho với mã phiếu xuất kho tương ứng
	 * @param id
	 * @return
	 */
	public static List<Detailgoodsissuednote> getDetailGoodsIssuedNoteWithId(int id){
		if(id == 0){
			return new ArrayList<Detailgoodsissuednote>();
		}
		
		String hql = "Select dgin from Detailgoodsissuednote dgin where "
				+ "dgin.id.id = :id";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		
		List<Detailgoodsissuednote> listDetial = session.createQuery(hql,
				Detailgoodsissuednote.class)
				.setParameter("id", id).getResultList();
		
		return listDetial;
		
	}
	
	/**
	 * Hàm insert phiếu xuất kho
	 * @param exportModels
	 * @param totalMoney
	 * @return 
	 * 	 -1: thêm phiếu nhập kho thành công
	 * 	 0: lỗi tạo phiếu
	 * 	 Khác -1 và 0: trả về mã sản phẩm lấy quá số lượng trong kho
	 */
	public static int addGoodsIssuedNote(ExportModels exportModels, 
			long totalMoney, Accounts account){
		
		if(exportModels == null){
			return 0;
		}
		
		//Insert phiếu xuất kho
		Goodsissuednote goodsissuednote = new Goodsissuednote();
		goodsissuednote.setAddressCompany(exportModels.getAddressCompany());
		goodsissuednote.setAddressWareHouse(exportModels.getAddressWareHouse());
		goodsissuednote.setReceiverName(exportModels.getNameReceive());
		goodsissuednote.setTotalMoney(totalMoney);
		goodsissuednote.setTotalQuantity(exportModels.getTotalQuantity());
		goodsissuednote.setAccounts(account);
		goodsissuednote.setDateCreate(new Date());
		goodsissuednote.setNote(exportModels.getNote());
		
		Session session = DataProvider.getSessionFactory().openSession();
		try {
			session.getTransaction().begin();
			session.save(goodsissuednote);
			//Lấy id của phiếu xuất kho mới nhận được
			int id = goodsissuednote.getId();
			//Tổng số lượng hàng hóa đã xuất kho
			
			//Insert chi tiết phiếu xuất kho
			for(ProductModels product : exportModels.getListProducts()){
				
				//Giảm số lượng sản phẩm
				if(!ProductDAO.updateDecreaseProductWithGoodsIssuedNote(
						product, session)){
					session.getTransaction().rollback();
					return product.getProductId();
				}
				
				Detailgoodsissuednote detailgoodsissuednote = 
						new Detailgoodsissuednote();
				
				detailgoodsissuednote.setId(new DetailgoodsissuednoteId(
						product.getProductId(), id));
				//Tạo biến sản phẩm để insert vào chi tiết
				Products productIssuedNote = new Products();
				productIssuedNote.setProductId(product.getProductId());

				detailgoodsissuednote.setProducts(productIssuedNote);
				detailgoodsissuednote.setPrice(product.getPrice());
				detailgoodsissuednote.setQuantity(product.getQuantity());
				detailgoodsissuednote.setTotalMoney(product.getTotalMoney());
				
				session.save(detailgoodsissuednote);
			}
			
			session.getTransaction().commit();
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
			//Xóa mọi dữ liệu mới insert, trở về trạng thái trước khi insert
			session.getTransaction().rollback();
			return 0;
		}finally {
			session.close();
		}
		
		return -1;
	}
	
	/**
	 * Hàm trả về danh sách những năm có phiếu xuất kho
	 * @return trả về danh sách năm
	 */
	@SuppressWarnings("unchecked")
	public static List<Integer> getArrayYear(){
		String hql = "Select DISTINCT YEAR(gsn.dateCreate) "
				+ "from Goodsissuednote gsn";
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Object> list = session.createQuery(hql).getResultList();
		session.getTransaction().commit();
		
		List<Integer> listYear = new ArrayList<Integer>();
		
		for(Object year : list){
			listYear.add((Integer)year);
		}
		
		return listYear;
	}
	
	/**
	 * Hàm thống kê phiếu xuất kho với năm và tháng tương ứng
	 * @param yearChoose
	 * @param monthChoose
	 * @return trả về danh sách thống kê phiếu xuât kho
	 */
	@SuppressWarnings("unchecked")
	public static List<CategoryStatistics> getGoodsIssuedeNoteStaistics(int yearChoose,
			int monthChoose){
		
		List<CategoryStatistics> listStatistics = new ArrayList<CategoryStatistics>();
		List<Categoryproducts> listCategory = CategoryProductDAO
				.getListCategoryInGoodsIssuedNote(yearChoose, monthChoose);
		
		Session session = DataProvider.getSessionFactory().openSession();
		
		for(Categoryproducts categoryproducts : listCategory){
			
			int categoryId = categoryproducts.getCategoryId();
			
			CategoryStatistics categoryStatistics = new CategoryStatistics();
			
			categoryStatistics.setCategroyId(categoryId);
			categoryStatistics.setNameCategory(categoryproducts.getName());
			
			session.getTransaction().begin();
			
			//Thực thi stored procedure statisticsProductInGoodsIssuedNote
			//để lấy các nhóm sản phẩm có trong phiếu xuất kho cùng 1 loại
			//sản phẩm và tổng số lượng sản phẩm tương ứng với nhóm đó
			//Từng vòng lặp phải định nghĩa stored procedure muốn sử dụng
			//để nhận các kết quả khác nhau từ transaction
			StoredProcedureQuery query = session.createStoredProcedureCall("statisticsProductInGoodsIssuedNote")
					.registerStoredProcedureParameter("yearChoose", Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter("monthChoose", Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter("categoryId", Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter("totalProduct", Long.class, ParameterMode.OUT);
			//Set giá trị cho stored procedure
			query.setParameter("yearChoose", yearChoose);
			query.setParameter("monthChoose", monthChoose);
			query.setParameter("categoryId", categoryId);
			List<Object[]> listObject = query.getResultList();
			categoryStatistics.setTotalProducts((long)query.getOutputParameterValue("totalProduct"));
			session.getTransaction().commit();
			
			List<TypeProductStatistics> listType = new ArrayList<TypeProductStatistics>();
			
			for(Object[] item : listObject){
				TypeProductStatistics productStatistics = new TypeProductStatistics();
				productStatistics.setNameTypeProducts((String)item[0]);
				productStatistics.setTotalProducts(((BigDecimal)item[1]).longValue());
				
				listType.add(productStatistics);
			}
			//Add danh sách nhóm sản phẩm vào loại sản phẩm (nhóm sản phẩm cùng loại)
			categoryStatistics.setListTypeProducts(listType);
			//Add loại sản phẩm vào danh sách loại sản phẩm
			listStatistics.add(categoryStatistics);
		}
		
		return listStatistics;
	}
}
