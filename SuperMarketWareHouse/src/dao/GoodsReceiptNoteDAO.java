package dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import model.CategoryStatistics;
import model.ImportModels;
import model.ProductModels;
import model.TypeProductStatistics;
import pojo.Categoryproducts;
import pojo.Detailgoodsreceiptnote;
import pojo.Goodsorder;
import pojo.Goodsreceiptnote;
import utils.DataProvider;

public class GoodsReceiptNoteDAO {
	
	/**
	 * Hàm lấy danh sách phiếu nhập kho
	 * @return trả về danh sách phiếu nhập kho
	 */
	public static List<Goodsreceiptnote> getListGoodsReceiptNote(){
		String hql = "Select gr from Goodsreceiptnote gr";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Goodsreceiptnote> listNote = session.createQuery(hql, 
				Goodsreceiptnote.class).getResultList();
		session.getTransaction().commit();
		return listNote;
	}
	
	/**
	 * Trả về thông tin phiếu xuất kho với mã phiếu tương ứng
	 * @param id
	 * @return trả về thông tin phiếu xuất
	 */
	public static Goodsreceiptnote getGoodsReceiptNote(int id){
		String hql = "Select gr from Goodsreceiptnote gr "
				+ "where gr.id = :id";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Goodsreceiptnote> listNote = session.createQuery(hql,
				Goodsreceiptnote.class)
				.setParameter("id", id).getResultList();
		session.getTransaction().commit();
		
		if(listNote.size() > 0){
			return listNote.get(0);
		}
		
		return null;
	}
	
	/**
	 * Hàm lấy chi tiết phiếu nhập kho
	 * @param id
	 * @return trả về danh sách chi tiết phiếu nhâp
	 */
	public static List<Detailgoodsreceiptnote> getDetailGoodsReceiptNoteWithId(
			int id){
		if(id == 0){
			return new ArrayList<Detailgoodsreceiptnote>();
		}
		
		String hql = "Select dgr from Detailgoodsreceiptnote dgr where "
				+ "dgr.id.id = :id";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Detailgoodsreceiptnote> listDetail = session.createQuery(hql,
				Detailgoodsreceiptnote.class)
				.setParameter("id", id).getResultList();
		session.getTransaction().commit();
		
		return listDetail;
	}
	
	public static Boolean addGoodsReciptNote(ImportModels importModels, long totalMoney){
		
		if(importModels == null){
			return false;
		}
		
		int goodsOrderId = importModels.getGoodsOrderId();
		
		//Insert phiếu nhập
		Goodsreceiptnote goodsreceiptnote = new Goodsreceiptnote();
		goodsreceiptnote.setDateCreate(new Date());
		goodsreceiptnote.setNameDeliver(importModels.getNameDeliver());
		goodsreceiptnote.setTotalMoney(totalMoney);
		goodsreceiptnote.setNote(importModels.getNote());
		Goodsorder goodsorder = new Goodsorder();
		goodsorder.setGoodsOrderId(goodsOrderId);
		
		goodsreceiptnote.setGoodsorder(goodsorder);
		Session session = DataProvider.getSessionFactory().openSession();
		try {
			session.getTransaction().begin();
			session.save(goodsreceiptnote);
			int id = goodsreceiptnote.getId();
			
			long totalQuantity = 0;
			//Insert chi tiết phiếu nhập
			totalQuantity = addDetailGoodsReceiptNote(importModels.getListProducts(),
					goodsOrderId, id, session);
			
			if(totalQuantity == 0){
				session.getTransaction().rollback();
				return false;
			}
			
			//Cập nhập số lượng sản phẩm
			ProductDAO.updateIncreaseProductWithGoodsReceiptNote(
					importModels.getListProducts(), session);
			
			goodsreceiptnote.setTotalQuantity(totalQuantity);
			session.update(goodsreceiptnote);
			
			session.getTransaction().commit();
			GoodsOrderDAO.updateGoodsOrderToDone(goodsOrderId);
		} catch (HibernateException e) {
			// TODO: handle exception
			e.printStackTrace();
			session.getTransaction().rollback();
			return false;
		}finally {
			session.close();
		}
		
		return true;
	}
	
	/**
	 * Hàm insert chi tiết phiếu nhập
	 * @param listProducts
	 * @param goodsOrderId
	 * @param goodsReceiptNoteId
	 * @param session
	 * @return trả về tổng số lượng sản phẩm
	 */
	private static long addDetailGoodsReceiptNote(List<ProductModels> listProducts, 
			int goodsOrderId, int goodsReceiptNoteId, Session session) throws HibernateException{
		long totalQuantity = 0;
		String hql = "Insert into detailgoodsreceiptnote(id, ProductId, "
				+ "Quantity, Note, GoodsOrderId) values(:id, :productId,"
				+ ":quantity, :note, :goodsOrderId)";
		for(ProductModels productModels : listProducts){
			session.createNativeQuery(hql)
			.setParameter("id", goodsReceiptNoteId)
			.setParameter("productId", productModels.getProductId())
			.setParameter("quantity", productModels.getQuantity())
			.setParameter("note", null)
			.setParameter("goodsOrderId", goodsOrderId)
			.executeUpdate();
			totalQuantity = totalQuantity + productModels.getQuantity();
		}
			
		return totalQuantity;
	}
	
	/**
	 * Hàm trả về danh sách những năm có phiếu nhập kho
	 * @return trả về danh sách năm
	 */
	@SuppressWarnings("unchecked")
	public static List<Integer> getArrayYear(){
		String hql = "Select DISTINCT YEAR(grn.dateCreate) "
				+ "from Goodsreceiptnote grn";
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Object> list = session.createQuery(hql).getResultList();
		session.getTransaction().commit();
		
		List<Integer> listYear = new ArrayList<Integer>();
		
		for(Object year : list){
			listYear.add((Integer)year);
		}
		
		return listYear;
	}
	
	/**
	 * Hàm thông kê phiếu nhập kho
	 * @param yearChoose
	 * @param monthChoose
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<CategoryStatistics> getGoodsReceipteNoteStaistics(int yearChoose,
			int monthChoose){
		List<CategoryStatistics> listStatistics = new ArrayList<CategoryStatistics>();
		List<Categoryproducts> listCategory = CategoryProductDAO
				.getListCategoryInGoodsReceiptNote(yearChoose, monthChoose);
		
		Session session = DataProvider.getSessionFactory().openSession();
		
		for(Categoryproducts categoryproducts : listCategory){
			int categoryId = categoryproducts.getCategoryId();
			
			CategoryStatistics categoryStatistics = new CategoryStatistics();
			
			categoryStatistics.setCategroyId(categoryId);
			categoryStatistics.setNameCategory(categoryproducts.getName());
			
			session.getTransaction().begin();
			
			//Thực thi stored procedure statisticsProductInGoodsIssuedNote
			//để lấy các nhóm sản phẩm có trong phiếu nhập kho cùng 1 loại
			//sản phẩm và tổng số lượng sản phẩm tương ứng với nhóm đó
			//Từng vòng lặp phải định nghĩa stored procedure muốn sử dụng
			//để nhận các kết quả khác nhau từ transaction
			StoredProcedureQuery query = session.createStoredProcedureCall("statisticsProductInGoodsReceiptNote")
					.registerStoredProcedureParameter("yearChoose", Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter("monthChoose", Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter("categoryId", Integer.class, ParameterMode.IN)
					.registerStoredProcedureParameter("totalProduct", Long.class, ParameterMode.OUT);
			
			query.setParameter("yearChoose", yearChoose);
			query.setParameter("monthChoose", monthChoose);
			query.setParameter("categoryId", categoryId);
			
			List<Object[]> listObject = query.getResultList();
			categoryStatistics.setTotalProducts((long)query.getOutputParameterValue("totalProduct"));
			session.getTransaction().commit();
			
			List<TypeProductStatistics> listType = new ArrayList<TypeProductStatistics>();
			
			//Gán giá trị cho biến listTypeProducts
			for(Object[] item : listObject){
				TypeProductStatistics productStatistics = new TypeProductStatistics();
				productStatistics.setNameTypeProducts((String)item[0]);
				productStatistics.setTotalProducts(((BigDecimal)item[1]).longValue());
				
				listType.add(productStatistics);
			}
			//Add danh sách nhóm sản phẩm vào loại sản phẩm (nhóm sản phẩm cùng loại)
			categoryStatistics.setListTypeProducts(listType);
			//Add loại sản phẩm vào danh sách loại sản phẩm
			listStatistics.add(categoryStatistics);
		}
		
		return listStatistics;
	}
}
