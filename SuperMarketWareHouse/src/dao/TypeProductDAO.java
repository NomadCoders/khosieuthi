package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.TypeProductStatistics;
import pojo.Typeproduct;
import utils.DataProvider;

public class TypeProductDAO {

	/**
	 * Hàm dùng để load combobox cho tìm kiếm
	 * @param categoryId
	 * @param chooseMode: true: lấy danh sách cho tìm kiếm
	 * 					  false: lấy danh sách cho thêm sản phẩm
	 * @return trả về danh sách loại sản phẩm (gồm typeProductId và name)
	 */
	@SuppressWarnings("unchecked")
	public static List<Typeproduct> loadListTypeProductByIdForComboBox(
			int categoryId, Boolean chooseMode){
		List<Typeproduct> listType = new ArrayList<Typeproduct>();
		
		//Nếu chooseMode = true thêm loại tất cả
		if(chooseMode){
			Typeproduct type = new Typeproduct();
			type.setTypeProductId(0);
			type.setName("Tất Cả");
			
			listType.add(type);
		}
		
		String hql = "Select tp.typeProductId, tp.name from Typeproduct tp"
				+ " where tp.categoryproducts.categoryId "
				+ "= :categoryId and tp.isDelete = 0";
		Session session = DataProvider.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		List<Object[]> list = session.createQuery(hql)
				.setParameter("categoryId", categoryId).getResultList();
		for(Object[] item : list){
			Typeproduct typeProduct = new Typeproduct();
			typeProduct.setTypeProductId((Integer)item[0]);
			typeProduct.setName((String)item[1]);
			
			listType.add(typeProduct);
		}
		
		transaction.commit();
		return listType;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Typeproduct> loadListTypeWithProducer(int categoryId
			, int producerId){
		if(producerId == 0){
			return new ArrayList<Typeproduct>();
		}
		List<Typeproduct> listType = new ArrayList<Typeproduct>();
		Typeproduct type = new Typeproduct();
		type.setTypeProductId(0);
		type.setName("Tất Cả");
		
		listType.add(type);
		
		String hql = "Select tp.typeProductId, tp.name from Typeproduct tp"
				+ " where tp.isDelete = 0 And "
				+ "tp.categoryproducts.categoryId = :categoryId And "
				+ "tp.typeProductId in (Select p.typeproduct.typeProductId "
				+ "from tp.productses p where p.typeproduct.typeProductId"
				+ " = tp.typeProductId And "
				+ "p.producers.producerId = :producerId And p.isDelete = 0))";
		
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Object[]> list = session.createQuery(hql)
				.setParameter("categoryId", categoryId)
				.setParameter("producerId", producerId)
				.getResultList();
		session.getTransaction().commit();
		
		for(Object[] item : list){
			Typeproduct typeproduct = new Typeproduct();
			typeproduct.setTypeProductId((Integer)item[0]);
			typeproduct.setName((String)item[1]);
			
			listType.add(typeproduct);
		}
		
		return listType;
	}
	
	public static long getTotalNumberTypeProducts(){
		String hql = "Select COUNT(*) from Typeproduct tp where "
				+ "tp.isDelete = 0";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		long totalTypeProducts = ((Long)session.createQuery(hql)
				.getSingleResult()).longValue();
		session.getTransaction().commit();
		
		return totalTypeProducts;
	}
	
	@SuppressWarnings("unchecked")
	public static List<TypeProductStatistics> getTotalNumberProductInType(int categoryId){
		String hql = "Select tp.name, SUM(p.stock) from "
				+ "Typeproduct tp join Products p on tp.typeProductId"
				+ " = p.typeproduct.typeProductId "
				+ "where tp.isDelete = 0 And p.isDelete = 0 "
				+ "And tp.categoryproducts.categoryId = :categoryId "
				+ "group by tp.typeProductId, tp.name";
		Session session = DataProvider.getSessionFactory().openSession();
		session.getTransaction().begin();
		List<Object[]> list = session.createQuery(hql)
				.setParameter("categoryId", categoryId)
				.getResultList();
		session.getTransaction().commit();
		
		List<TypeProductStatistics> productStatistics = 
				new ArrayList<TypeProductStatistics>();
		
		for(Object[] item : list){
			TypeProductStatistics typeProductStatistics = new TypeProductStatistics();
			typeProductStatistics.setNameTypeProducts((String)item[0]);
			typeProductStatistics.setTotalProducts((long)item[1]);
			
			productStatistics.add(typeProductStatistics);
		}
		
		return productStatistics;
	}
}
