﻿//-------------------- Kiểm lỗi/ Hiện thông báo --------------------

// Hiện thông báo lỗi, nếu không có lỗi thì ẩn.
function checkError() {
	var errors = $('.error-summary');
	var errorsSize = errors.size();
	for (i = 0; i < errorsSize; i++) {
		var current = $($('.error-summary').get(i));
		if (current.text().trim() != "") {
			current.slideDown('slow');
		} else {
			current.slideUp('slow');
		}
	}
};

// Kiểm tra hợp lệ khi bấm submit
// Trả về true: hợp lệ, trang sẽ tiến hành submit
// Trả về false: không hợp lệ, trang sẽ không tiến hành submit
function checkFormValid() {
	var isNotEmpty = true;
	var input = $('#buyer-info input, #product input, #registerForm input, .log-form input, #import-info input,' 
		+ '#export-info input, #accountInfor input, #user-info input, #userInfor input');
	var size = input.size();
	// Kiểm tra value các input có rỗng hay không, nếu có trả về giá trị
	// isNotEmpty là false
	for (i = 0; i < size; i++) {		
		if ($($(input).get(i)).attr('name') == 'note'){
			continue;
		}
		var currentText = $($(input).get(i)).val();
		if (currentText === "") {
			isNotEmpty = false;
			break;
		}
	}
	// Kiểm tra hợp lệ:
            // Nếu không có thông báo lỗi (các thẻ có class error-summary có
			// thuộc tính
	// text là rỗng)
            // và giá trị các thẻ input không rỗng (biến isNotEmpty bằng false)
			// thì trả
	// về true
	if ($('.error-summary').text().trim() === "" && isNotEmpty) {
		var textPrice = $('#summary').find('span > span').text();
		while (textPrice.indexOf(',') !== -1) {
			textPrice = textPrice.replace(',', '');
		}
		$('#summary').find('input').val(textPrice);
		return true;
	}
	$('.h3').text('Thông tin không hợp lệ!');
	setTimeout(function(){
		$('.h3').text('');
		checkError();
	}, 5000);
	changeClass();
	checkError();
	return false;
};

// Định dạng số điện thoại thành dạng 999-9999-9999
function formatPhoneNum() {
	var number = $(this).val();
	number = number.replace(/-/g, '');
	number = number.replace(/(\d{3,})(\d{3})(\d{4})/, '$1-$2-$3');
	$(this).val(number);
};

// Hàm kiểm tra chỉ được nhập số
function checkNumberOnly(e) {
	// Allow: backspace, delete, tab, escape, enter and .
	if ($.inArray(e.keyCode, [ 46, 8, 9, 27, 13, 110, 190 ]) !== -1 ||
	// Allow: Ctrl+A, Command+A
	(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
	// Allow: home, end, left, right, down, up
	(e.keyCode >= 35 && e.keyCode <= 40)) {
		// let it happen, don't do anything
		return;
	}
	// Ensure that it is a number and stop the keypress
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
};

// Hàm kiểm tra định dạng email
function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
	return pattern.test(emailAddress);
};

// Hàm kiểm tra định dạng số điện thoại
function isPhoneNumber(inputVal)  
{ 
// Số điện thoại là số và có 10-11 chữ số và có số 0 ở đầu
	if(inputVal.match("(\\b0\\d{9,10}\\b)")){
		return true;
	}
	else{
		// Số điện thoại mở rộng, thêm 3-5 số (123-456-1235 ext123)
		if(inputVal.match("\\d{3}-\\d{3,4}-\\d{4}\\s(x|ext)\\d{3,5}")){
			return true;
		}
		else{
			// Số điện thoại bàn có mã vùng (08) 3854 8900
			// (066) 3640 001
			// (066)3640001
			// 123-345.7891
			if(inputVal.match("\\b[(]?\\d{2,3}[)]?[-\\s]?\\d{4}[-\\s\\.]?\\d{3,4}\\b") && inputVal.length < 12){
				return true;
			}
		}	
	}	
	return false;
};

// Kiểm tra giá trị input không rỗng khi các input xảy ra sự kiện blur(rời khỏi input)
$('#buyer-info input, #export-info input, #import-info input, #product input,'
	+ ' #registerForm input, .log-form input, #accountInfor input, #user-info input, #userInfor input').on('blur', function() {
	var total = $(this).size();
	var fieldName = $(this).attr('name');
	var ErrorBox = $(this).next();
	// Kiểm tra nếu thuộc tính value của input rỗng sẽ hiện thông báo lỗi
	if ($(this).val().trim() === '') {
		// Tùy vào name của mỗi input sẽ hiện những thông báo lỗi thích hợp:
		switch (fieldName) {
		case 'name':
			ErrorBox.text('Tên không được rỗng');
			break;
		case 'nameLogin':
			ErrorBox.text('Tên đăng nhập không được rỗng');
			break;
		case 'fullName':
			ErrorBox.text('Họ tên không được rỗng');
			break;
		case 'nameBuyer':
			ErrorBox.text('Tên Người Mua không được rỗng');
			break;
		case 'nameDeliver':
			ErrorBox.text('Tên Người Giao không được rỗng');
			break;
		case 'nameReceive':
			ErrorBox.text('Tên Người Nhận không được rỗng');
			break;
		case 'txtBuyerTax':
			ErrorBox.text('Mã số thuế không được rỗng');
			break;
		case 'phoneNumber':
			ErrorBox.text('Số điện thoại không được rỗng');
			break;
		case 'txtBuyerFax':
			ErrorBox.text('Fax không được rỗng');
			break;
		case 'email':
			ErrorBox.text('Email không được rỗng');
			break;
		case 'address':
			ErrorBox.text('Địa chỉ không được rỗng');
			break;
		case 'addressWareHouse':
			ErrorBox.text('Địa chỉ kho xuất không được rỗng');
			break;
		case 'addressCompany':
			ErrorBox.text('Địa chỉ nơi nhận không được rỗng');
			break;
		case 'price':
			ErrorBox.text('Giá không được rỗng');
			break;
		case 'stock':
			ErrorBox.text('Số lượng tồn không được rỗng');
			break;
		case 'password':
			ErrorBox.text('Mật khẩu không được rỗng');
			break;
		case 'currentPassword':
			ErrorBox.text('Mật khẩu không được rỗng');
			break;
		case 'newPassword':
			ErrorBox.text('Mật khẩu không được rỗng');
			break;
		case 'rePassword':
			ErrorBox.text('Nhập lại mật khẩu không được rỗng');
			break;
		}
	} else {
		switch (fieldName) {
		case 'email':
			// Kiểm tra email hợp lệ bằng hàm isValidEmailAddress
			var check = isValidEmailAddress($(this).val());
			var ErrorBox = $(this).next();
			if (check === false) {
				// Thông báo email không hợp lệ
				ErrorBox.text('Email không hợp lệ');
			} else {
				// Set giá trị text của thông báo lỗi về rỗng
				ErrorBox.text('');
			}
			break;
		case 'phoneNumber':
			// Kiểm tra số điện thoại hợp lệ bằng hàm isValidEmailAddress
			var check = isPhoneNumber($(this).val());
			var ErrorBox = $(this).next();
			if (check === false) {
				// Thông báo số điện thoại không hợp lệ
				ErrorBox.text('Số điện thoại không hợp lệ');
			} else {
				// Set giá trị text của thông báo lỗi về rỗng
				ErrorBox.text('');
			}
			break;
		case 'newPassword':
			// Kiểm tra rePassword trùng với Password
			var curPass = $('#currentPassword').val();
			var ErrorBox = $(this).next();
			if ($(this).val() === curPass) {
				// Thông báo mật khẩu mới không được trùng mật khẩu cũ
				ErrorBox.text('Mật khẩu mới không được trùng mật khẩu cũ');
			} else {
				// Set giá trị text của thông báo lỗi về rỗng
				ErrorBox.text('');
			}
			break;
		case 'rePassword':
			// Kiểm tra rePassword trùng với Password
			var newPass = $('#newPassword').val();
			var pass = $('#password').val();
			var ErrorBox = $(this).next();
			if ( ($(this).val() !== newPass && newPass !== '' && newPass.length !== 0) || ($(this).val() !== pass && pass !== '' && pass.length !== 0)) {
				// Thông báo rePassword không khớp
				ErrorBox.text('Mật khẩu nhập lại không trùng khớp');
			} else {
				// Set giá trị text của thông báo lỗi về rỗng
				ErrorBox.text('');
			}
			break;
		case 'status':
			break;
		case 'permission':
			break;
		// Nếu giá trị thuộc tính value của input không rỗng thì xóa hết các thông báo lỗi
		default:
			// set giá trị text của thông báo lỗi về rỗng
			$(this).next().text('');
			break;
		}
	}
	checkError();
});

// Xóa thông báo lỗi khi focus lại input
$('#buyer-info input, #export-info input, #import-info input, #product input, #registerForm input,'
	+ '.log-form input, #accountInfor input, #user-info input').on('focus', function(){
	if ($(this).attr('type') == 'radio'){
		return;
	}
	$(this).next().text('');
	checkError();
});

// Xóa dòng thông báo lỗi khi nhấn reset
$('#product input').on('click', function(){
	switch($(this).attr('type')){
		case 'reset':
			$('span.error-summary').text('');
			break;
	};
});

// Thay class thông báo success hoặc danger
function changeClass(){
	if ($('.h3').text().indexOf('!') == -1){
		$('.h3').removeClass('alert-danger');
		$('.h3').addClass('alert-success');
	}else{
		$('.h3').removeClass('alert-success');
		$('.h3').addClass('alert-danger');
	}
};

// -------------------- Chức năng cho trang --------------------

// Thêm mới dòng review sản phẩm khi bấm nút thêm (dùng cho trang order, export)
$('#order,#export').find('div button.btn-success').on('click',function() {
	var i =  $('#review-order tbody tr').size();
	var currentProduct = $('#goodsOrder > .row:eq(1),#exportForm > .row:eq(1)').find('select option:selected');
	var productId = currentProduct.val();
	var productName = currentProduct.text();
	var unit = currentProduct.attr('data-unit');
	var quantity = $('#quantity').val();
	var price = currentProduct.attr('data-price');
	var totalMoney = Number(price) * Number(quantity);
	// kiểm tra giá trị input số lượng, giá hợp lệ
	if (quantity > 0 && !isNaN(price)) {
		// Kiểm xem sản phẩm vừa thêm đã có sẵn trong danh sách chưa, nếu có thì cộng dồn vào, không thêm mới.
		for (j = 0; j < i; j++) {
			var currentLine = $($('tbody tr').get(j));
			if (currentLine.find('td:eq(0)').text() == productId) {
				var currentQuantity = currentLine.find('td:eq(3)').text().trim();
				var input = currentLine.find('td:eq(3)').find('input');
				input.val(Number(currentQuantity) + Number(quantity));
				currentLine.find('td:eq(3)').text(Number(currentQuantity) + Number(quantity));
				currentLine.find('td:eq(3)').append(input);
				setAmountMoney($(currentLine).find('button:eq(0)'));
				setTotalMoney();
				return;
			}
		}
		var tr = "<tr style='display:none'><td>"+ productId
				+ "<input name='listProducts["+i+"].productId' type='hidden' value='"+productId+"'/></td><td>"
				+ productName
				+ "<input name='listProducts["+i+"].name' type='hidden' value='"+productName+"'/></td><td>"
				+ unit
				+ "<input name='listProducts["+i+"].nameUnitCalculate' type='hidden' value='"+unit+"'/></td><td>"
				+ quantity
				+ "<input name='listProducts["+i+"].quantity' type='hidden' value='"+quantity+"'/></td><td class='money'>"
				+ price
				+ "<input name='listProducts["+i+"].price' type='hidden' value='"+price+"'/></td><td class='money'>"
				+ totalMoney
				+ "<input name='listProducts["+i+ "].totalMoney' type='hidden' value='"+totalMoney+"'/></td><td>" 
				+ "<div id='delete-message' style='display:none'><button type='button' class='btn btn-danger' name='tnConfirm'>Xóa Sản Phẩm</button><button type='button' class='btn btn-warning' name='btnCancel'>Hủy</button></div><div><button type='button' class='btn btn-primary' name='btnIncrease'><span class='glyphicon glyphicon-plus'></span></button><button type='button' class='btn btn-warning' name='btnDecrease'><span class='glyphicon glyphicon-minus'></span></button><button type='button' class='btn btn-danger' name='btnDelete'><span class='glyphicon glyphicon-remove'></span></button></div></td></tr>";
		$('tbody').append(tr);
		$('tbody tr:last').fadeIn('fast');
		setAmountMoney($('#review-order tr:last button'));
		setTotalMoney();
		bindingOrderButton();
		deleteServerError();
	} else {
		return;
	}
});

// Hàm tính tổng tiền
function setTotalMoney() {
	var total = 0;
	var tableRow = $('tbody tr');
	// tổng tiền bằng tổng các ô thành tiền
	for (i = 0; i < tableRow.length; i++) {
		var textPrice = $(tableRow.get(i)).find('td:eq(5)').text().trim();
		while (textPrice.indexOf(',') !== -1) {
			textPrice = textPrice.replace(',', '');
		}
		total += Number(textPrice);
	}
	$('#summary span > span').text(total);
	formatMoney();
};

// Hàm cập nhật cột thành tiền khi nhấn nút +,-,x
function bindingOrderButton() {
	$('#review-order button').on('click', function() {
		var name = $(this).attr('name');
		switch (name) {
		case 'btnIncrease':
			// chạy hàm cập nhạt số lượng
			updateQuantity($(this), 1);
			// tính lại thành tiền
			setAmountMoney($(this));
			break;
		case 'btnDecrease':
			updateQuantity($(this), -1);
			setAmountMoney($(this));
			break;
		case 'btnDelete':
			deleteQuantity($(this));
			break;
		}
		// Tính lại tổng tiền sau khi thành tiền thay đổi số lượng
		setTotalMoney();
	});
};

// Hàm tính thành tiền của sản phẩm trong đơn hàng
// element: đối tượng button vừa nhấn (ví dụ nút +,-,x)
function setAmountMoney(element) {
	var quantity = Number(element.parent().parent().parent().find('td:eq(3)').text());

	var textPrice = element.parent().parent().parent().find('td:eq(4)').text().trim();
	while (textPrice.indexOf(',') !== -1) {
		textPrice = textPrice.replace(',', '');
	}

	var price = Number(textPrice);
	var input = element.parent().parent().prev().find('input');
	input.val(quantity * price);
	element.parent().parent().prev().text(quantity * price);
	element.parent().parent().prev().append(input);
	formatMoney();
	// tính lại tổng tiền sau khi thành tiền thay đổi
	setTotalMoney();
};

// Hàm tăng giảm số lượng khi nhấn nút +, -
// element: đối tượng button vừa nhấn
// a: giá trị để xác định hành động cập nhật:
// a < 0: xóa sản phẩm
// a > 0: thêm, bớt số lượng bằng a
function updateQuantity(element, a) {
	var val = Number(element.parent().parent().parent().find('td:eq(3)').text());
	if (val === 1 && a < 0) {
		deleteQuantity(element);
	} else {
		var input = element.parent().parent().parent().find('td:eq(3)').find('input');
		input.val(val+a);
		element.parent().parent().parent().find('td:eq(3)').text(val + a);
		element.parent().parent().parent().find('td:eq(3)').append(input);
	}
}

// Hàm xóa dòng sản phẩm khi nhấn nút x
function deleteQuantity(element) {
	// Hiện bảng xác nhận xóa khi nhấn nút x
	$(element).parent().slideUp('fast', function() {
		$(element).parent().parent().find('#delete-message').slideDown('fast');
	});
	// Xóa dòng sản phẩm
	$(element).parent().parent().find('#delete-message button:eq(0)').click(function() {
		element.parent().parent().parent().fadeOut('fast', function() {
			element.parent().parent().parent().remove();
		});
	});
	$(element).parent().parent().find('#delete-message button:eq(1)').click(function() {
		$(element).parent().parent().find('#delete-message').slideUp('fast', function() {
			$(element).parent().slideDown('fast');
		});
	});
}

// Hàm lấy chi tiết phiếu cho trang Lưu Trữ Danh Sách Phiếu
function getListDetail(id){
	var form = "list_"+id;
	document.getElementById(form).submit();
};

// Xóa sản phẩm trong table khi chọn nhà sản xuất khác
$('#producerId').on('change',function(){
	$('tbody tr').remove();
});

// Hàm sự kiện in cho trang Lưu/In Phiếu
function printContent() {
	var restorepage = $('body').html();
    var printcontent = $('.reverse-content').clone();
	$('body').empty().html(printcontent);
	window.print();
	$('body').html(restorepage);
}
      
// Chuyển nội dung cần in thành XHTML, chuyển server xuất file PDF
function savePDF(){
	var html = $('html').clone();
	$(html).find('.action').remove();
	$(html).find('br').replaceWith('<br\/>');
	
	$(html).find('.reverse').css({'background-color':'#fff'});
	$(html).find('.reverse-content').css({'border':'0px'});
	$(html).find('.reverse-content>div').css({'margin':'0','min-height': '0'});
	
	var data ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><html>"
	         + $(html).html().trim().replace("↵", "").replace(/(\r\n|\n|\r)/gm,"")+"</html>";
	while(data.indexOf('<br>') != -1){
	data = data.replace("<br>","<br/>")
	}	
	$('.save').parent().find('input').val(data);
}


// -------------------- Cài đặt Ajax --------------------

// Lấy dữ liệu sản phẩm dựa trên Danh Mục và Loại Sản Phẩm hiện tại.
// id: Mã danh mục
// type: Mã loại
// manufactureId: Mã Nhà Sản xuất
function getProductAPI(id, type, manufactureId) {
	var Url = '';
	if (type == null) {
		type = 0;
	}
	if (manufactureId == undefined) {
		Url = '/SuperMarketWareHouse/rest/products-api/' + id + '/' + type;
	} else {
		Url = '/SuperMarketWareHouse/rest/products-api/' + id + '/' + type + '/' + manufactureId;
	}
	$.getJSON(Url, function(data) {
		$('#goodsOrder > .row:eq(1),#exportForm > .row:eq(1)').find('select').empty().val('');
		$.each(data, function(i, item) {
			var option = "<option value='" + data[i].productId + "' data-unit='" + data[i].nameUnitCalculate + "' data-price='" + data[i].price + "'>" + data[i].name + "</option>";
			$('#goodsOrder > .row:eq(1),#exportForm > .row:eq(1)').find('select').append(option);
		});
	});
}
// Lấy thông tin nhà cung cấp.
// id: Mã nhà cung cấp
function getManufactureInfo(id) {
	var Url = '/SuperMarketWareHouse/rest/infor-producer/' + id;
	$.getJSON(Url, function(data) {
		$('#phoneNumber').val(data.phoneNumber);
		$('#address').val(data.address);
	});
}

// Lấy thông tin sản phẩm theo nhà cung cấp.
// id: Mã nhà cung cấp
function getManufactureProductCategory(id) {
	var Url = '/SuperMarketWareHouse/rest/category-api/' + id;
	$.getJSON(Url, function(data) {
		// Làm trống thẻ Select trước.
		$('#categoryId').empty();
		$('#goodsOrder > .row:eq(1),#exportForm > .row:eq(1)').find('select').empty();
		$.each(data, function(i, item) {
			var option = "<option value='" + data[i].categoryId + "'>" + data[i].name + "</option>";
			$('#categoryId').append(option);
		});
	});
}
// Lấy dữ liệu loại sản phẩm dựa trên danh mục.
// id: Mã danh mục
// isDetail: Cho biết trang hiện tại có phải là trang Sửa/Thêm Sản Phẩm hay
// không
// manufactureId: Mã Nhà Sản xuất
function getProductType(id, isDetail, manufactureId) {
	var Url = '';
	// Đường dẫn API cho 2 trang riêng biệt.
	if (isDetail) {
		Url = '/SuperMarketWareHouse/rest/type-products-api/products/';
	} else {
		Url = '/SuperMarketWareHouse/rest/type-products-api/';
	}
	if (manufactureId == undefined) {
		Url += id;
	} else {
		Url += id + "/" + manufactureId;
	}

	$.getJSON(Url, function(data) {
		// Làm trống thẻ Select trước.
		$('#productType').empty();
		$.each(data, function(i, item) {
			var option = "<option value='" + data[i].typeProductId + "'>" + data[i].name + "</option>";
			$('#productType').append(option);
		});
	});
}

// Lấy dữ liệu Chi Tiết Phiếu dựa trên Mã Phiếu.
// id: Mã Phiếu
function getDetailOrderForImport(id) {
	if (id == null){
		return;
	}
	var Url = '/SuperMarketWareHouse/rest/importmodels-api/' + id;
	$.getJSON(Url, function(data) {
		// Làm trống thẻ Select trước.
		$('tbody').empty();
		$.each(data.listProducts, function(i, item) {
			var raw = data.listProducts;
			var tr = "<tr><td>"	+ raw[i].productId 
					+ "<input name='listProducts[" + i + "].productId' type='hidden' value='" + raw[i].productId + "'/></td><td>" 
					+ raw[i].name
					+ "<input name='listProducts[" + i + "].name' type='hidden' value='" + raw[i].name + "'/></td><td>" 
					+ raw[i].nameUnitCalculate 
					+ "<input name='listProducts[" + i + "].nameUnitCalculate' type='hidden' value='" + raw[i].nameUnitCalculate + "'/></td><td>" 
					+ raw[i].quantity 
					+ "<input name='listProducts[" + i + "].quantity' type='hidden' value='" + raw[i].quantity + "'/></td><td class='money'>" 
					+ raw[i].price 
					+ "<input name='listProducts[" + i + "].price' type='hidden' value='" + raw[i].price + "'/></td><td class='money'></td><td>"
					+ "<div id='delete-message' style='display:none'><button type='button' class='btn btn-danger' name='tnConfirm'>Xóa Sản Phẩm</button><button type='button' class='btn btn-warning' name='btnCancel'>Hủy</button></div><div><button type='button' class='btn btn-primary' name='btnIncrease'><span class='glyphicon glyphicon-plus'></span></button><button type='button' class='btn btn-warning' name='btnDecrease'><span class='glyphicon glyphicon-minus'></span></button><button type='button' class='btn btn-danger' name='btnDelete'><span class='glyphicon glyphicon-remove'></span></button></div></td></tr>";
			$('tbody').append(tr);
			setAmountMoney($('#review-order tr:last button'));
		});
		// Gán tên người giao 
		$('#nameDeliver').val(data.nameDeliver);
		// Gán số điện thoại
		$('#phoneNumber').val(data.phoneNumber);
		// Gán địa chỉ
		$('#address').val(data.address);
		// Gắn tên Nhà Sản Xuất
		$('#nameProducer').val(data.nameProducer);
		// Gán ghi chú
		$('#note').val(data.note);
		// Tính tổng tiền, gán chức năng cho nút mới.
		setTotalMoney();
		bindingOrderButton();
	});
}

// -------------------- Hiệu Ứng Animation --------------------

// Animation biểu đồ trang Dashboard
function drawDashboard(){
	var panel = $('#dashboard .panel-info');
	var j = 0;
	for (i = 0; i < panel.size(); i++) {
		var listRow = $(panel.get(i)).find('.row');
		var sum = Number($(panel.get(i)).find('.row:eq(0) div:eq(1)').text());
		for (j = 0; j < listRow.size(); j++) {
                    var currentRow = $(listRow.get(j));
			var progressBar = currentRow.find('.progress-bar');
			var width = Number(currentRow.find('div:eq(1) span').text());
			var percent = (width / sum) * 100;
			progressBar.css('width', percent + "%");
			progressBar.find('span').text(Math.round(percent) + "%");
			if (percent < 10) {
				progressBar.addClass('progress-bar-danger');
				continue;
			}
			if (percent < 50) {
				progressBar.addClass('progress-bar-warning');
				continue;
			}
			if (percent < 80) {
				progressBar.addClass('progress-bar-success');
				continue;
			}
		}
	}
}

// Animation cho sidebar
$('.sidebar .collapse-custom').click(function () {
    if ($(this).parent().parent().find('.child-index').is(':visible')) {
         $(this).parent().parent().find('.child-index').slideToggle('200');
         $(this).removeClass('glyphicon-minus-sign');
         $(this).addClass('glyphicon-plus-sign');
	} else {
         $(this).parent().parent().find('.child-index').slideToggle('200');
         $(this).removeClass('glyphicon-plus-sign');
         $(this).addClass('glyphicon-minus-sign');
	}
});
// Animation cho khi Mouse Over (rê chuột vào)
$('#user-panel').hover(function() {
 if($('#user-panel').hasClass('side-hide')){
		$('#user-panel').removeClass('side-hide');
		$('#user-panel').addClass('side-show');
         } else {
		$('#user-panel').removeClass('side-show');
		$('#user-panel').addClass('side-hide');
	}
});

// Animation cho Navigation Bar
$(function(){
	// menu animation
	var menu = $('#main .navbar-nav li');
	// animation cho sự kiện mouseenter
	menu.on('mouseenter',function(){
		// đổi màu chữ sang màu vàng
		$(this).find('>a').css({"color":"#ff0"});
		// mở dropdown-list nếu có
		if (menu.hasClass('dropdown')){
			$(this).addClass('open');
		}
	});
	// animation cho sự kiện mouseleave
	menu.on('mouseleave',function(){
		// đổi màu chữ về màu đen (nếu là link trên dropdown-list)
		if ($(this).parent().hasClass('dropdown-menu')){			
			$(this).find('>a').css({"color":"black"});
		// đổi màu chữ về màu trắng (nếu là link trên menu)
		}else{
			$(this).find('>a').css({"color":"#fff"});
		}
		// đóng dropdown-list nếu có
		if (menu.hasClass('dropdown')){
			$(this).removeClass('open');
		}
	});
});

// Tắt readonly khi bấm update form thông tin người dùng
// riêng username vẫn giữ readonly
$('#user-info .btn-info').click(function() {
	$('#user-info input').removeAttr('readonly');
	$('#user-info #name').attr('readonly', 'readonly');
	$(this).hide();
	$('#user-info .btn-success').removeClass('hide');
});

// Thay class active trên thanh Tabs của trang Thống Kê
$('#summary ul li').on('click', function(){
	var name = $(this).attr('name');
	switch(name){
		case 'tabImport':
			$('#summary button').attr('name','import');
			$('#summary ul li').attr('class','');
			$(this).attr('class','active');
			break;
		case 'tabExport':
			$('#summary button').attr('name','export');
			$('#summary ul li').attr('class','');
			$(this).attr('class','active');
			break;
	}
});
// -------------------- Tiện ích --------------------

// Định dạng tiền.
function formatMoney() {
	var unformatted = $('.money');
	var total = unformatted.size();
	for (i = 0; i < total; i++) {
		var oldPrice = $($(unformatted).get(i)).text();
		var input = $($(unformatted).get(i)).find('input');
		$($(unformatted).get(i)).text(oldPrice.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
		$($(unformatted).get(i)).append(input);
	}
};

// Hàm chuyển từ số thành chữ (chỉ chuyển được từ 1 -> 999,999,999)
// vd: 1234 -> một ngàn hai trăm ba mươi bốn
// biến num: số cần đổi
// biến str: số trả về
function DigitIntoWords (num) {
		// kiểm tra độ dài chuỗi
         if ((num = num.toString()).length > 12){
             return 'overflow';
         }
         
         // cắt số cần đổi thành nhiều phần:
         // vd: 123123456789 -> 123123456789,1,23,1,23,4,56,7,89
         n = ('000000000000' + num).substr(-12).match(/^(\d{1})(\d{2})(\d{1})(\d{2})(\d{1})(\d{2})(\d{1})(\d{2})$/);
	
         if (!n){
             return;
         }
         var str = '';
         var donvi = ['','một ','hai ','ba ','bốn ', 'năm ','sáu ','bảy ','tám ','chín ','mười '];
         var chuc = ['', 'mười', 'hai mươi','ba mươi','bốn mươi','năm mươi', 'sáu mươi','bảy mươi','tám mươi','chín mươi'];
	
		// chuyển chữ số hàng trăm tỷ (nếu có)
        str += (n[1] != 0) ? (donvi[Number(n[1])]) + 'trăm ' : '';
		if (n[1] != 0 && [n[2][0]] == 0 && n[2][1] != 0){
			str += 'linh';
		}
		// chuyển chữ số hàng chục tỷ (nếu có)
		str += (n[2] != 0) ? (chuc[n[2][0]] + ' ' + donvi[n[2][1]]) + 'tỷ ' : '';
		if ((n[1] != 0 || n[2] != 0) && n[3] == 0 && n[4] != 0){
			str += 'không trăm ';
		}
		// chuyển chữ số hàng trăm triệu (nếu có)
		str += (n[3] != 0) ? (donvi[Number(n[3])]) + 'trăm ' : '';
		if ((n[1] != 0 || n[2] != 0 || n[3] != 0) && n[4][0] == 0 && n[4][1] != 0){
			str += 'linh';
		}
		// chuyển chữ số hàng chục triệu (nếu có)
		str += (n[4] != 0) ? (chuc[n[4][0]] + ' ' + donvi[n[4][1]]) + 'triệu ' : '';
		if ((n[1] != 0 || n[2] != 0 || n[3] != 0 || n[4] != 0) && n[5] == 0 && n[6] != 0){
			str += 'không trăm ';
		}
		// chuyển chữ số hàng trăm ngàn (nếu có)
		str += (n[5] != 0) ? (donvi[Number(n[5])]) + 'trăm ' : '';
		if ((n[1] != 0 || n[2] != 0 || n[3] != 0 || n[4] != 0 || n[5] != 0) && n[6][0] == 0 && n[6][1] != 0){
			str += 'linh';
		}
		// chuyển chữ số hàng chục ngàn (nếu có)
		str += (n[6] != 0) ? (chuc[n[6][0]] + ' ' + donvi[n[6][1]]) + 'ngàn ' : '';
		if ((n[1] != 0 || n[2] != 0 || n[3] != 0 || n[4] != 0 || n[5] != 0 || n[6] != 0) && n[7] == 0 && n[8] != 0){
			str += 'không trăm ';
		}
		// chuyển chữ số hàng trăm (nếu có)
		str += (n[7] != 0) ? (donvi[Number(n[7])]) + 'trăm ' : '';
		if ((n[1] != 0 || n[2] != 0 || n[3] != 0 || n[4] != 0 || n[5] != 0 || n[6] != 0 || n[7] != 0) && n[8][0] == 0 && n[8][1] != 0){
			str += 'linh';
		}
		// chuyển chữ số hàng chục và hàng đơn vị(nếu có)
		str += (n[8] != 0) ? (chuc[n[8][0]] + ' ' + donvi[n[8][1]]) + 'đồng ' : 'đồng ';	
	 return str;
};

//-------------------- Hàm chính - Chạy khi load trang --------------------

$(document).ready(function() {
	// Kiểm tra dữ liệu số
	$(".number").keydown(checkNumberOnly);
	
	// Thêm class success cho các thông báo thành công từ server và error cho
	// thất bại.
	changeClass();
	
	// Xóa thông báo từ server sau 5s hiển thị
	if($('.h3').text().trim() != ""){
		setTimeout(function(){
			$('.h3').text('');
			checkError();
		}, 5000);
	}
	
	// Định dạng tiền.
	formatMoney();
	
    // Tính tổng tiền, gán chức năng thêm - giảm số lượng và xóa cho các nút.
	if ($('#order,#import,#export').length > 0) {
		bindingOrderButton();
		setTotalMoney();
	}
	
	// Vẽ biểu đồ thống kê cho trang Dashboard
	if ($('#dashboard').length > 0) {
		drawDashboard();
	}
	// Ajax lấy dữ liệu cho trang Đặt Hàng và Xuất Kho.
	if ($('#order,#export').length > 0) {
		var producerId = $('#producerId').val();
		var categoryId = $('#categoryId').val();
		var productType = $('#productType').val();
		
		if ($('#order').length > 0) {
		// Lấy dữ liệu khi lần đầu load trang.
			getManufactureInfo(producerId);
			getManufactureProductCategory(producerId);
			getProductType(categoryId, false, producerId);
			getProductAPI(categoryId, productType, producerId);
                    // Gán sự kiện khi thay đổi Nhà Cung Cấp, Ajax sẽ lấy danh
					// mục,
			// loại,
			// sản phẩm mới.
			$('#producerId').on('change', function() {
				getManufactureProductCategory($('#producerId').val());
				getManufactureInfo($('#producerId').val());
				getProductType($('#categoryId').val(), false, $('#producerId').val());
				getProductAPI($('#categoryId').val(), $('#productType').val(), $('#producerId').val());
			});
		}
		// Lấy dữ liệu khi lần đầu load trang.
		getProductType(categoryId, false, producerId);
		getProductAPI(categoryId, productType, producerId);

                // Gán sự kiện khi thay đổi danh mục, Ajax sẽ lấy dữ liệu Loại
				// Sản Phẩm
		// và Tên Sản Phẩm sau đó load lên thẻ Select.
		$('#categoryId').on('change', function() {
			getProductType( $('#categoryId').val(), false,  $('#producerId').val());
			getProductAPI( $('#categoryId').val(), $('#productType').val(),  $('#producerId').val());
		});
                // Gán sự kiện khi thay đổi loại sản phẩm, Ajax sẽ lấy dữ liệu
				// Tên Sản
		// Phẩm sau đó load lên thẻ Select.
		$('#productType').on('change', function() {
			getProductAPI($('#categoryId').val(), $('#productType').val(),  $('#producerId').val());
		});
	}
	// Ajax lấy dữ liệu cho trang Sửa Sản Phẩm
	if ($('#detail').length > 0) {
                // Gán sự kiện khi thay đổi danh mục, Ajax sẽ lấy dữ liệu Loại
				// Sản Phẩm
		// sau đó load lên thẻ Select.
		$('#detail .info > div:eq(4)').find('select').on('change', function() {
			getProductType($(this).val(), true);
		});
	}
	// Ajax lấy dữ liệu cho trang Thêm Sản Phẩm
	if ($('#create').length > 0) {
		// Lấy dữ liệu khi lần đầu load trang.
		getProductType($('#create .info > div:eq(4)').find('select').val(), true, $('#producerId').val());
                // Gán sự kiện khi thay đổi danh mục, Ajax sẽ lấy dữ liệu Loại
				// Sản Phẩm
		// sau đó load lên thẻ Select.
		$('#create .info > div:eq(4)').find('select').on('change', function() {
			getProductType($(this).val(), true);
		})
	}
	// Ajax lấy dữ liệu cho trang Nhập Kho.
	if (($('#import').length > 0)) {
		// Lấy dữ liệu khi lần đầu load trang.
		getDetailOrderForImport($('#goodsOrderId').val());
                // Gán sự kiện khi thay đổi Mã Phiếu, Ajax sẽ lấy dữ liệu Chi
				// Tiết Phiếu
		// sau đó load lên Table.
		$('#goodsOrderId').on('change', function() {
			getDetailOrderForImport($(this).val());
		})
	}
	if($('.reverse').length > 0){
		var total = $('.total').text();
		while(total.indexOf(',') != -1){
			total = total.replace(',','');
		}
		var str = DigitIntoWords(Number(total));	
		$('.number-to-text').text(str);

		// Ngăn hành động Ctrl + P mặc định khi in trang.
		jQuery(document).bind("keyup keydown", function(e){
		    if(e.ctrlKey && e.keyCode == 80){
		    	printContent();
		        return false;
		    }
		});
	}
});