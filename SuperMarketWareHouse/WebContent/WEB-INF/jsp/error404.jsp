<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Quản lý kho siêu thị</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/jquery-ui.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body class="bg-primary">
	<div id="error-404" class="container-fluid">
		<div class="col-xl-2 col-xl-offset-4 col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
			<div class="row text-center">
				<span class="h1">404</span>
			</div>
			<div class="row text-center">
				<span class="h4">Trang Không Tìm Thấy</span>
			</div>
			<div class="row">
				<a href='<c:url value="/"/>' class="btn btn-info col-xs-2 col-xs-offset-3">
					<span class="glyphicon glyphicon-home"></span>
				</a>
				<a onclick="history.go(-1);" class="btn btn-default col-xs-2 col-xs-offset-2">
					<span class="glyphicon glyphicon-arrow-left"></span>
				</a>
			</div>
		</div>
	</div>
	<script src="<c:url value="/resources/js/jquery.js" />"></script>
	<script src="<c:url value="/resources/js/jquery-ui.min.js" />"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/functions.js" />"></script>
</body>
</html>