<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div id="export" class="container">
	<h1 class="title">Phiếu Xuất Kho</h1>
	<form:form method="post" modelAttribute="exportForm" action="create" onsubmit="return checkFormValid()">
		<div id="export-info">
			<div class="alert alert-danger h3 error-summary" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				<form:errors path="listProducts" /> ${msg}
			</div>
			<div class="row form-group">
				<div class="col-sm-12 col-md-12">
					<label>
						Họ Tên Người Nhận(
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="nameReceive" type="text" class="form-control" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="nameReceive" />
						</div>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-3 col-md-4">
					<label>
						Email(
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="email" type="text" class="form-control" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="email" />
						</div>
					</div>
				</div>
				<div class="col-sm-3 col-md-4">
					<label>
						Số Điện Thoại (
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="phoneNumber" type="text" class="form-control phone number" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="phoneNumber" />
						</div>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-12 col-md-12">
					<label>
						Địa Chỉ Kho Xuất(
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="addressWareHouse" type="text" class="form-control" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="addressWareHouse" />
						</div>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-12 col-md-12">
					<label>
						Địa Chỉ Nơi Nhận(
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="addressCompany" type="text" class="form-control" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="addressCompany" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row form-group">
			<div class="col-sm-8 col-md-4">
				<label>Danh Mục</label>
				<div class="col-sm-12 col-md-12">
					<form:select path="categoryId" items="${listCategory}" itemValue="categoryId" itemLabel="name" class="form-control" />
				</div>
			</div>
			<div class="col-sm-8 col-md-4">
				<label>Nhóm Hàng</label>
				<div class="col-sm-12 col-md-12">
					<select class="form-control" id="productType">
					</select>
				</div>
			</div>
			<div class="col-sm-4 col-md-4">
				<label>
					Số Lượng Sản Phẩm(
					<span class="text-danger"> * </span>
					)
				</label>
				<div class="col-sm-12 col-md-12">
					<input id="quantity" type="number" min="1" value="1" name="txtSearchProduct" class="form-control" />
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-sm-8 col-md-8">
				<label>
					Tên Sản Phẩm (
					<span class="text-danger"> * </span>
					)
				</label>
				<div class="col-sm-12 col-md-12 form-group">
					<select class="form-control">

					</select>
				</div>
			</div>
			<div class="col-sm-4 col-md-4">
				<label>Ghi chú</label>
				<div class="col-sm-12 col-md-12">
					<input type="text" name="txtSearchProduct" class="form-control" />
				</div>
			</div>
		</div>
		<div class="row form-group">
			<button type="button" class="btn btn-success" style="width: 100%">Thêm</button>
		</div>
		<table id="review-order" class="table table-bordered table-responsive">
			<thead>
				<tr>
					<td>Mã Hàng</td>
					<td>Diễn Giải</td>
					<td>Đơn Vị Tính</td>
					<td>Số Lượng</td>
					<td>Đơn Giá</td>
					<td>Thành Tiền</td>
					<td></td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<div id="summary">
			<span>
				Tổng Tiền
				<span class="money"></span>
				Đồng
			</span>
			<input name="hidTotalPrice" type="hidden" />
		</div>
		<div class="row">
			<button type="submit" class="btn btn-info" style="width: 100%">Hoàn thành</button>
		</div>
	</form:form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		checkError();
	});
</script>