<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:url value="/products/edit/${product.productId}" var="productActionUrl" />
<form:form method="post"  class="container" modelAttribute="product" action="${productActionUrl}" onsubmit="return checkFormValid()">
	<div id="detail" class="container-fluid">
		<h1 class="title text-center">Cập Nhật Sản Phẩm</h1>
		<div class="info col-sm-6 col-md-6 form-group">
			<div class="alert alert-danger h3 error-summary" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				${msg}
			</div>
		</div>
		<div class="info col-sm-6 col-md-6 form-group">
			<div class="row form-group">
				<span class="col-xs-12 h4">Tên Sản Phẩm</span>
				<div class="col-md-8 col-sm-8">
					<form:input path="name" type="text" class="form-control" />
					<span class="error text-danger h5 error-summary"><form:errors path="name"/></span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-xs-12 h4">Giá</span>
				<div class="col-md-8 col-sm-8">
					<form:input path="price" type="number" min="0" class="form-control" />
					<span class="error text-danger h5 error-summary"><form:errors path="price"/></span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-xs-12 h4">Số Lượng Tồn</span>
				<div class="col-md-8 col-sm-8">
					<form:input path="stock" type="number" min="0" class="form-control" />
					<span class="error text-danger h5 error-summary"><form:errors path="stock"/> </span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-xs-12 h4">Nhà Sản Xuất</span>
				<div class="col-md-8 col-sm-8">
					<form:select path="producers.producerId" items="${listProducer}" itemValue="producerId" itemLabel="name" class="form-control" />
					<span class="error text-danger"> </span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-xs-12 h4">Danh Mục</span>
				<div class="col-md-8 col-sm-8">
					<form:select path="categoryproducts.categoryId" items="${categoryProduct}" itemValue="categoryId" itemLabel="name" class="form-control" />
					<span class="error text-danger"> </span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-xs-12 h4">Nhóm Hàng</span>
				<div class="col-md-8 col-sm-8">
					<!-- <select class="form-control" name="typeproduct.typeProductId" id="productType">
            	</select> -->
					<form:select path="typeproduct.typeProductId" items="${typeProduct}" itemValue="typeProductId" itemLabel="name" id="productType" class="form-control" />
					<span class="error text-danger"> </span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-xs-12 h4">Tình Trạng</span>
				<div class="col-md-8 col-sm-8">
					<span>Còn Hàng</span>
					<form:radiobutton path="status" value="true" />
					&nbsp;&nbsp;&nbsp;
					<span>Hết Hàng</span>
					<form:radiobutton path="status" value="false" />
					<span class="error text-danger"> </span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-xs-12 h4">Đơn Vị Tính</span>
				<div class="col-md-8 col-sm-8">
					<!-- <input name="txtProductUnit" type="text" value="" class="form-control"/> -->
					<form:select path="unitcalculate.id" items="${listUnitCal}" itemValue="id" itemLabel="name" class="form-control" />
					<span class="error text-danger"> </span>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-8 col-sm-8">
					<input type="submit" class="btn btn-info" value="Lưu" />
					<input name="" type="reset" class="btn btn-danger" value="Trống Thông Tin" />
				</div>
			</div>
		</div>
	</div>
</form:form>