<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div id="summary">
	<div class="alert alert-info">
		<h1 class="title">Thống Kê Sản Phẩm</h1>
	</div>
	<ul class="nav nav-tabs nav-justified">
		<c:choose>
			<c:when test="${check == true }">
				<li name="tabImport" class="active">
					<a href='<c:url value="/statistics/goods-receipt-note"/>'>Nhập Kho</a>
				</li>
				<li name="tabExport">
					<a href='<c:url value="/statistics/goods-issued-note"/>'>Xuất Kho</a>
				</li>
			</c:when>

			<c:when test="${check == false }">
				<li name="tabImport">
					<a href='<c:url value="/statistics/goods-receipt-note"/>'>Nhập Kho</a>
				</li>
				<li name="tabExport" class="active">
					<a href='<c:url value="/statistics/goods-issued-note"/>'>Xuất Kho</a>
				</li>
			</c:when>
		</c:choose>
	</ul>
	<div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
		<!-- Kiểm tra biến điều kiện để chuyển đổi giá trị action ở form
		sẽ chuyển đến trang phiếu nhập hay phiếu xuất -->
		<c:choose>
			<c:when test="${check == true }">
				<c:url value="goods-receipt-note" var="url" />
			</c:when>

			<c:when test="${check == false }">
				<c:url value="goods-issued-note" var="url" />
			</c:when>
		</c:choose>
		<form method="post" action="${url}">
			<div class="row form-group">
				<div class="col-sm-6 col-md-6">
					<span>Năm</span>
					<select name="yearSelect" class="form-control">
						<c:forEach var="item" items="${listYear }">
							<c:choose>
								<c:when test="${item == yearSelect }">
									<option selected="selected">${item }</option>
								</c:when>

								<c:when test="${item != yearSelect }">
									<option>${item }</option>
								</c:when>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="col-sm-6 col-md-6">
					<span>Tháng</span>
					<select name="monthSelect" class="form-control">
						<option value="0">Tất Cả</option>
						<c:forEach var="i" begin="1" end="12">
							<c:choose>
								<c:when test="${i == monthSelect}">
									<option selected="selected" value="${i }">${i }</option>
								</c:when>

								<c:when test="${i != monthSelect}">
									<option value="${i }">${i }</option>
								</c:when>
							</c:choose>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="row form-group">
				<button type="submit" class="btn btn-info col-xs-12">
					<span class="glyphicon glyphicon-list-alt"></span>
					&nbsp;Thống Kê
				</button>
			</div>
		</form>
	</div>
	<c:choose>
		<c:when test="${listNoteStatistics.size() == 0}">
			<div class="col-xs-12 text-center">
				<h3>Không có phiếu</h3>
			</div>
		</c:when>
		<c:when test="${listNoteStatistics.size() > 0 }">
			<div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
				<hr>
				<div class="table-responsive">
					<h3>Tổng số lượng sản phẩm: ${totalProduct}</h3>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Ngành Hàng</th>
								<th>Nhóm Hàng</th>
								<th>Số Lượng</th>
								<th>Tổng</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="noteStatistics" items="${listNoteStatistics }">
								<tr>
									<td rowspan="${noteStatistics.listTypeProducts.size() }">${noteStatistics.nameCategory }</td>
									<td>${noteStatistics.listTypeProducts[0].nameTypeProducts }</td>
									<td>${noteStatistics.listTypeProducts[0].totalProducts }</td>
									<td rowspan="${noteStatistics.listTypeProducts.size() }">${noteStatistics.totalProducts}</td>
								</tr>
								<c:forEach var="i" begin="1" end="${noteStatistics.listTypeProducts.size() - 1 }">
									<tr>
										<td>${noteStatistics.listTypeProducts[i].nameTypeProducts }</td>
										<td>${noteStatistics.listTypeProducts[i].totalProducts }</td>
									</tr>
								</c:forEach>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
	</c:choose>
</div>