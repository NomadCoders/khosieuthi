<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Đăng Nhập</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body class="log-form">
	<div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 panel panel-primary">
		<div class="container-fluid">
			<h1>Đăng Nhập</h1>
			<form:form method="post" modelAttribute="loginFrom" action="login" onsubmit="return checkFormValid()">
				<div class="alert alert-danger h3 error-summary" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					${msg}
				</div>
				<div class="row">
					<span>Tên Đăng Nhập</span>
					<!-- <input type="text" class="form-control"/> -->
					<form:input path="nameLogin" type="text" class="form-control form-group" />
					<div class="alert alert-danger error-summary" role="alert">
						<form:errors path="nameLogin" />
					</div>
				</div>
				<div class="row">
					<span>Mật Khẩu</span>
					<form:input path="password" type="password" class="form-control  form-group" />
					<div class="alert alert-danger error-summary" role="alert">
						<form:errors path="password" class="h5" />
					</div>
				</div>
				<div class="row">
					<span>Ghi Nhớ Đăng Nhập</span>
					<form:checkbox path="rememberMe" />
				</div>
				<div class="row">
					<button name="btnLogin" class="btn btn-primary" type="submit">Đăng Nhập</button>
				</div>
				<div class="row">
					<a href="#">Quên Mật Khẩu?</a>
					<!-- <a href="#" style="float: right">Đăng Ký</a> -->
				</div>
			</form:form>
		</div>
	</div>
	<script src="<c:url value="/resources/js/jquery.js" />"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/functions.js" />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			checkError();
		});
	</script>
</body>
</html>