<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="t" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Xuất Phiếu Đặt Hàng</title>
<style type="text/css">
* {
	font-family: 'Times New Roman';
}

span, strong {
	line-height: 20px;
}

.action {
	margin-left: auto;
	margin-right: auto;
	margin-top: 20px;
	margin-bottom: 30px;
	height: 50px;
	width: 50%;
	clear: both;
}

.action>div {
	width: 200px;
	padding-left: 10px;
	height: 50px;
}

.print {
	background: url(../../resources/img/print.png);
}

.save {
	background: url(../../resources/img/pdf.png);
}

.action button {
	border-radius: 50%;
	display: block;
	height: 50px;
	width: 50px;
	background-size: cover;
	cursor: pointer;
}

.reverse {
	background-color: #999999;
	height: max-content;
}

.title {
	clear: both;
	text-align: center;
}

.reverse-content {
	border: 2px #000000 solid;
	background-color: #ffffff;
	margin-left: auto;
	margin-right: auto;
	width: 200mm;
	font-family: time !important;
	clear: both;
}

.reverse-content>div {
	margin: 20mm;
	font-family: time;
	font-size: 12px;
	display: block;
	min-height: 297mm;
}

.reverse-content table, td, th {
	border: 1px solid #000000;
	text-align: center;
}

.reverse-content table {
	page-break-after: auto;
	border-collapse: collapse;
	width: 100%;
}

.reverse-content thead {
	display: table-header-group
}

.reverse-content th {
	font-weight: 200;
	height: 50px;
}

.reverse-content tr, .reverse-summary {
	page-break-inside: avoid;
	page-break-after: auto
}

.reverse-content td {
	page-break-inside: avoid;
	page-break-after: auto;
	width: max-content;
	font-size: 12px;
}

.reverse-content tfoot {
	display: table-footer-group
}

.left-side-panel {
	float: left;
}

.right-side-panel {
	float: right;
}

.reverse-content table td {
	width: max-content;
	font-size: 12px;
}

.col-3 {
	width: 24%;
	display: block;
	float: left;
}

.col-6 {
	width: 49%;
	display: block;
	float: left;
}

.reverse-confirm {
	margin-top: 10px;
	text-align: center;
}

.text-center {
	text-align: center;
}

.form-group {
	margin-bottom: 25px;
}

.name {
	text-align: left;
}

.money {
	text-align: right;
}

.number-to-text {
	text-transform: capitalize;
}

@page {
	margin: 0;
}

@media print {
	@page {
		margin: 20mm;
		width: auto;
		height: auto;
	}
	.reverse-content {
		border: 0px;
	}
	.reverse-content>div {
		margin: 0;
	}
}
</style>
</head>
<body class="reverse">
	<div class="action">
		<div class="left-side-panel">
			<button style="float: right" class="print" onclick="printContent();"></button>
		</div>
		<div class="right-side-panel">
			<form method="post" action='<c:url value="/print/download/goods-order/${note.goodsOrderId}"/>'>
				<input type="hidden" name="contentHTML" />
				<button type="submit" style="float: left" onclick="savePDF()" class="save"></button>
			</form>
		</div>
	</div>
	<div class="reverse-content" id="reverse-content">
		<div class="container-fluid">
			<div class="title text-center form-group">
				<h1>Đơn Đặt Hàng</h1>
			</div>
			<div class="info form-group">
				<span>Tên người mua hàng: ${note.accounts.fullName }</span>
				<br />
				<span>Tên đơn vị: ${note.producers.name }</span>
				<br />
				<span>Địa chỉ: ${note.producers.address }</span>
				<br />
				<div class="col-3">
					<span>Điện thoại: ${note.producers.phoneNumber }</span>
				</div>
				<div class="col-3">
					<span> Email: ${note.email }</span>
				</div>
				<div style="clear: both">Ghi Chú: ${note.note}</div>
			</div>
			<div class="print-content form-group">
				<table class="page-break">
					<thead>
						<tr>
							<th>Mã hàng</th>
							<th>Tên mặt hàng</th>
							<th>Đơn vị tính</th>
							<th>Số Lượng</th>
							<th>Đơn Giá</th>
							<th>Thành Tiền</th>
							<th>Ghi Chú</th>
						</tr>
					</thead>
					<tbody>
						<t:eval expression="0" var="totalPrice" />
						<c:forEach var="item" items="${detailNote}">
							<tr>
								<td>${item.products.productId }</td>
								<td class="name">${item.products.name }</td>
								<td>${item.products.unitcalculate.name }</td>
								<td class="money">${item.quantity }</td>
								<td class="money">${item.products.price }</td>
								<t:eval expression="${totalPrice + item.products.price }" var="totalPrice" />
								<td class="money">${item.products.price  * item.quantity }</td>
								<td>${item.note }</td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3">Tổng Tiền Hàng</td>
							<td class="money">${note.totalQuantity }</td>
							<td class="money">${totalPrice }</td>
							<td class="money total">${note.totalMoney }</td>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="row reverse-summary form-group">
				<strong>
					- Tổng số tiền (viết bằng chữ):
					<strong class="number-to-text"></strong>
				</strong>
				<div class="reverse-confirm">
					<div class="col-6">
						<strong>Nhân Viên Kinh Doanh</strong>
						<br />
						(Ký, họ tên)
					</div>
					<div class="col-6">
						<strong>Người Mua Hàng</strong>
						<br />
						(Ký, họ tên)
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<c:url value="/resources/js/jquery.js" />"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/jquery-ui.min.js" />"></script>
	<script src="<c:url value="/resources/js/functions.js" />"></script>
</body>
</html>