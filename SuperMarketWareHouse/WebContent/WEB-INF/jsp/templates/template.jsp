<%@page import="utils.ManagerLogin"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><tiles:getAsString name="title"></tiles:getAsString></title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/jquery-ui.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/jquery.js" />"></script>
<script src="<c:url value="/resources/js/jquery-ui.min.js" />"></script>
</head>
<body class="scrollbar">
	<div id="header" class="nav navbar-default">
		<h1 class="title">ABC Super Market</h1>
	</div>
	<div id="user-panel" class="side-hide">
		<div>
			<a href="#">
				<%=ManagerLogin.getNameLogin()%></a>
			<span class="seperator"></span>
		</div>
		<div>
			<a href='<c:url value="/account/information"/>'>
				<span class="glyphicon glyphicon-exclamation-sign"></span>
				Thông Tin
			</a>
			<span class="seperator"></span>
		</div>
		<div>
			<a href='<c:url value="/account/logoff"/>'>
				<span class="glyphicon glyphicon-off"></span>
				Đăng Xuất
			</a>
		</div>
	</div>
	<div id="main" class="container-fluid col-xl-8 col-xl-offset-2 col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
		<div id="navigation-bar" class="nav navbar-default">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li>
						<a href='<c:url value="/"/>'>Trang Chủ</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Quản Lý Sản Phẩm</a>
						<ul class="dropdown-menu">
							<li>
								<a href='<c:url value="/products/list"/>'>Danh Sách Sản Phẩm</a>
							</li>
							<c:if test="<%=ManagerLogin.getPermissionInAccount()%>">
								<li>
									<a href='<c:url value="/products/create"/>'>Thêm Sản Phẩm</a>
								</li>
							</c:if>
							<li>
								<a href='<c:url value="/goods-order/create"/>'>Lập Đơn Hàng</a>
							</li>
						</ul>
					</li>
					<c:if test="<%=ManagerLogin.getPermissionInAccount()%>">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Lập Phiếu</a>
							<ul class="dropdown-menu">
								<li>
									<a href='<c:url value="/goods-receipt-note/create"/>'>Nhập Kho</a>
								</li>
								<li>
									<a href='<c:url value="/goods-issue-note/create"/>'>Xuất Kho</a>
								</li>
							</ul>
						</li>
					</c:if>
					<c:if test="<%=ManagerLogin.getPermissionInAccount()%>">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Quản Lý Tài Khoản</a>
							<ul class="dropdown-menu">
								<li>
									<a href='<c:url value="/account/register"/>'>Tạo Tài Khoản</a>
								</li>
								<li>
									<a href='<c:url value="/account/list"/>'>Danh Sách Tài Khoản</a>
								</li>
							</ul>
						</li>
					</c:if>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Lưu Trữ</a>
						<ul class="dropdown-menu">
							<li>
								<a href='<c:url value="/storage/goods-order"/>'>Danh Sách Đơn Hàng</a>
							</li>
							<c:if test="<%=ManagerLogin.getPermissionInAccount()%>">
								<li>
									<a href='<c:url value="/storage/goods-receipt-note"/>'>Danh Sách Phiếu Nhập</a>
								</li>
								<li>
									<a href='<c:url value="/storage/goods-issued-note"/>'>Danh Sách Phiếu Xuất</a>
								</li>
								<li>
									<a href='<c:url value="/statistics/goods-receipt-note"/>'>Thống Kê</a>
								</li>
							</c:if>
						</ul>
					</li>
				</ul>
				<c:url value="/products/search" var="searchUrl" />
				<form class="navbar-form navbar-right" role="search" method="get" action="${searchUrl}">
					<div class="form-group">
						<input type="text" placeholder="Tìm Kiếm" name="txtKeySearch" class="form-control">
					</div>
					<button type="submit" class="btn btn-default">Tìm kiếm</button>
				</form>
			</div>
		</div>
		<div id="content" class="container-fluid">
			<tiles:insertAttribute name="left_side_bar"></tiles:insertAttribute>
			<tiles:insertAttribute name="content"></tiles:insertAttribute>
		</div>
	</div>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/functions.js" />"></script>
</body>
</html>