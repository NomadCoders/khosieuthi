<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id="list-import" class="col-xs-12">
	<div class="alert alert-info">
		<h1 class="title">Lưu Trữ Phiếu Nhập</h1>
	</div>
	<div>
		<c:choose>
			<c:when test="${receiptNote.size() == 0 }">
				<h2>Danh sách phiếu nhập trống</h2>
			</c:when>
			<c:when test="${receiptNote.size() > 0 }">
				<div class="panel panel-primary" id="list">
					<div class="panel-heading">
						<h2 class="text-center title">Danh Sách Phiếu</h2>
					</div>
					<div class="panel-body table-responsive scrollbar">
						<table class="table table-condensed table-hover table-bordered">
							<thead>
								<tr>
									<th>Mã Phiếu</th>
									<th>Đơn Vị</th>
									<th>Địa Chỉ Kho</th>
									<th>Ngày</th>
									<th>Người Giao</th>
									<th>Tổng Tiền</th>
									<th>Ghi Chú</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="item" items="${receiptNote}" varStatus="i">
									<tr>
										<td style="display: none">
											<form id="list_${i.index }" method="POST" action="goods-receipt-note">
												<input type="hidden" name="id" value="${i.index }" />
											</form>
										</td>
										<td>${item.id }</td>
										<td>${item.goodsorder.producers.name }</td>
										<td>${item.goodsorder.addressWareHouse }</td>
										<td>
											<fmt:formatDate value="${item.dateCreate}" type="both" pattern="MM-dd-yyyy dd:mm:ss" />
										</td>
										<td>${item.nameDeliver}</td>
										<td class="money">${item.totalMoney}</td>
										<td>${item.note}</td>
										<td class="actions-cell">
											<a href='<c:url value="/print/goods-receipt-note/${item.id }"/>' class="btn btn-success">
												<span class="glyphicon glyphicon-print"></span>
											</a>
											<button class="btn btn-default" onclick="getListDetail(${i.index })">
												<span class="glyphicon glyphicon-th-list"></span>
											</button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div class="panel panel-primary" id="detail">
					<div class="panel-heading">
						<h2 class="text-center title">Chi Tiết Phiếu ${id}</h2>
					</div>
					<div class="panel-body table-responsive scrollbar">
						<a href='<c:url value="/print/goods-receipt-note/${id}"/>' class="btn btn-success">
							<span class="glyphicon glyphicon-print"></span>
						</a>
						<table class="table table-condensed table-hover table-bordered">
							<thead>
								<tr>
									<th>Sản Phẩm</th>
									<th>Số Lượng</th>
									<th>Đơn Giá</th>
									<th>Thành Tiền</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="detail" items="${detailReceiptNote }">
									<tr>
										<td>${detail.detailgoodorder.products.name}</td>
										<td>${detail.quantity}</td>
										<td class="money">${detail.detailgoodorder.products.price}</td>
										<td class="money">${detail.detailgoodorder.products.price * detail.quantity}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</c:when>
		</c:choose>
	</div>
</div>