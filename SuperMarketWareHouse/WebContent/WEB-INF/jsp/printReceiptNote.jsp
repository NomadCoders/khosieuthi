<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="t" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Xuất Phiếu Nhập</title>

<style type="text/css">
* {
	font-family: 'Times New Roman';
}

span, strong {
	line-height: 20px;
}

.reverse {
	background-color: #999999;
	height: max-content;
}

.action {
	margin-left: auto;
	margin-right: auto;
	margin-top: 20px;
	margin-bottom: 30px;
	height: 50px;
	width: 50%;
	clear: both;
}

.action button {
	border-radius: 50%;
	display: block;
	height: 50px;
	width: 50px;
	background-size: cover;
	cursor: pointer;
}

.action>div {
	width: 200px;
	padding-left: 10px;
	height: 50px;
}

.print {
	background: url(../../resources/img/print.png);
}

.save {
	background: url(../../resources/img/pdf.png);
}

.header {
	margin-bottom: 25px;
	margin-left: 5px;
	margin-right: 5px;
	overflow: auto;
}

.reverse-content {
	border: 2px #000000 solid;
	background-color: #ffffff;
	margin-left: auto;
	margin-right: auto;
	width: 200mm;
	font-family: time !important;
	clear: both;
}

.reverse-content>div {
	margin: 20mm;
	font-family: time;
	font-size: 12px;
	display: block;
	min-height: 297mm;
}

.reverse-content table, td, th {
	border: 1px solid #000000;
	text-align: center;
}

.reverse-content table {
	page-break-after: auto;
	border-collapse: collapse;
	width: 100%;
}

.reverse-content thead {
	display: table-header-group
}

.reverse-content th {
	font-weight: 200;
	height: 50px;
}

.reverse-content tr, .reverse-summary {
	page-break-inside: avoid;
	page-break-after: auto
}

.reverse-content td {
	page-break-inside: avoid;
	page-break-after: auto;
	width: max-content;
	font-size: 12px;
}

.reverse-content tfoot {
	display: table-footer-group
}

.title {
	clear: both;
	text-align: center;
}

.left-side-panel {
	float: left;
}

.right-side-panel {
	float: right;
}

.reverse-confirm {
	margin-top: 10px;
	text-align: center;
}

.text-center {
	text-align: center;
}

.form-group {
	margin-bottom: 25px;
}

.col-3 {
	width: 24%;
	display: block;
	float: left;
}

.col-6 {
	width: 50%;
	display: block;
	float: left;
}

.name {
	text-align: left;
}

.money {
	text-align: right;
}

.number-to-text {
	text-transform: capitalize;
}

@page {
	margin: 0;
}

@media print {
	@page {
		margin: 20mm;
		width: auto;
		height: auto;
	}
	.reverse-content {
		border: 0px;
	}
	.reverse-content>div {
		margin: 0;
	}
}
</style>
</head>
<body class="reverse">
	<div class="action">
		<div class="left-side-panel">
			<button style="float: right" class="print" onclick="printContent();"></button>
		</div>
		<div class="right-side-panel">
			<form method="post" action='<c:url value="/print/download/goods-receipt-note/${note.id}"/>'>
				<input type="hidden" name="contentHTML" />
				<button type="submit" style="float: left" onclick="savePDF()" class="save"></button>
			</form>
		</div>
	</div>
	<div class="reverse-content" id="reverse-content">
		<div class="container-fluid">
			<div class="header">
				<div class="col-6">
					<strong>
						Đơn Vị:....
						<br />
						Bộ phận:...
					</strong>
				</div>
				<div class="col-6 text-center">
					<span>Mẫu số: 01 - VT</span>
					<br />
					(Ban hành theo QĐ số: 48/2006/QĐ-BTC
					<br />
					Ngày 14/9/2006 của Bộ trưởng BTC)
				</div>
			</div>
			<div class="title text-center form-group">
				<h1>Phiếu Nhập Kho</h1>
				<br />
				<span>
					Ngày
					<span id="day">
						<fmt:formatDate pattern="dd" value="${note.dateCreate}" />
					</span>
					Tháng
					<span id="month">
						<fmt:formatDate pattern="MM" value="${note.dateCreate}" />
					</span>
					Năm
					<span id="year">
						<fmt:formatDate pattern="yyyy" value="${note.dateCreate}" />
					</span>
				</span>
				<br />
				<span>
					Số....
					<span id="id-number"></span>
				</span>
			</div>
			<div class="info form-group">
				<span>- Họ và tên người giao: ${note.nameDeliver}</span>
				<br />
				<span>
					- Theo phiếu nhập số ${note.goodsorder.goodsOrderId } ngày
					<fmt:formatDate pattern="dd" value="${note.goodsorder.dateCreate}" />
					tháng
					<fmt:formatDate pattern="MM" value="${note.goodsorder.dateCreate}" />
					năm
					<fmt:formatDate pattern="yyyy" value="${note.goodsorder.dateCreate}" />
				</span>

				<br />
				<span>- Nhập tại kho: Địa điểm: Số ${note.goodsorder.addressWareHouse }</span>
				<br />
			</div>
			<div class="print-content form-group">
				<table class="page-break">
					<thead>
						<tr>
							<th>STT</th>
							<th>Tên, nhãn hiệu quy cách, phẩm chất vật tư, dụng cụ sản phẩm, hàng hóa</th>
							<th>Đơn vị tính</th>
							<th>Số Lượng</th>
							<th>Đơn Giá</th>
							<th>Thành Tiền</th>
						</tr>
					</thead>
					<tbody>
						<t:eval expression="0" var="totalPrice" />
						<c:forEach var="item" items="${detailNote}" varStatus="i">
							<tr>
								<td>${i.index + 1 }</td>
								<td class="name">${item.detailgoodorder.products.name }</td>
								<td>${item.detailgoodorder.products.unitcalculate.name }</td>
								<td class="money">${item.detailgoodorder.quantity }</td>
								<td class="money">${item.detailgoodorder.products.price }</td>
								<t:eval expression="${totalPrice + item.detailgoodorder.products.price }" var="totalPrice" />
								<td class="money">${item.detailgoodorder.products.price  * item.quantity }</td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<td></td>
							<td>Cộng</td>
							<td>x</td>
							<td class="money">${note.goodsorder.totalQuantity }</td>
							<td class="money">${totalPrice }</td>
							<td class="money total">${note.totalMoney }</td>
						</tr>
					</tfoot>
				</table>

			</div>
			<div class="row reverse-summary form-group">
				<strong>
					- Tổng số tiền (viết bằng chữ):
					<strong class="number-to-text"></strong>
				</strong>
				<br />
				<strong>- Số chứng từ gốc kèm theo:.................................</strong>

				<div class="reverse-confirm">
					<div class="col-3">
						<strong>Người Lập Phiếu</strong>
						<br />
						(Ký, họ tên)
					</div>
					<div class="col-3">
						<strong>Người giao hàng</strong>
						<br />
						(Ký, họ tên)
					</div>
					<div class="col-3">
						<strong>Thủ kho</strong>
						<br />
						(Ký, họ tên)
					</div>
					<div class="col-3">
						<strong>
							Kế Toán trưởng
							<br />
							(Hoặc bộ phận có nhu cầu nhận)
						</strong>
						<br />
						(Ký, họ tên)
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<c:url value="/resources/js/jquery.js" />"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/jquery-ui.min.js" />"></script>
	<script src="<c:url value="/resources/js/functions.js" />"></script>
</body>
</html>