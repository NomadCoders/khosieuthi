<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Đăng Ký</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/jquery-ui.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body class="log-form">
	<div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 panel panel-primary">
		<div class="container-fluid">
			<h1>Đăng Ký Tài Khoản</h1>
			<div class="alert alert-danger h3 error-summary" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				<span class="sr-only"></span>
			</div>
			<form:form method="post" modelAttribute="registerForm" action="register" onsubmit="return checkFormValid()">
				<div class="row">
					<span>Tên Đăng Nhập</span>
					<form:input path="name" type="text" class="form-control" />
					<div class="alert alert-danger h5 error-summary" role="alert">
						<form:errors path="name" class="form-label" />
					</div>
				</div>
				<div class="row">
					<span>Mật Khẩu</span>
					<form:input path="password" type="password" class="form-control" />
					<div class="alert alert-danger h5 error-summary" role="alert">
						<form:errors path="password" class="form-label" />
					</div>
				</div>
				<div class="row">
					<span>Xác Nhận Mật Khẩu</span>
					<form:input path="rePassword" type="password" class="form-control" />
					<div class="alert alert-danger h5 error-summary" role="alert">
						<form:errors path="rePassword" class="form-label" />
					</div>
				</div>
				<div class="row">
					<span>Họ tên</span>
					<form:input path="fullName" type="text" class="form-control" />
					<div class="alert alert-danger h5 error-summary" role="alert">
						<form:errors path="fullName" class="form-label" />
					</div>
				</div>
				<!-- <div class="row">
	                    <span>Ngày tháng năm sinh</span>
	                    <input type="text" class="date-picker form-control"/>
	                </div> -->
				<div class="row">
					<span>Địa chỉ</span>
					<form:input path="address" type="text" class="form-control" />
					<div class="alert alert-danger h5 error-summary" role="alert">
						<form:errors path="address" class="form-label" />
					</div>
				</div>
				<div class="row">
					<span>Email</span>
					<form:input path="email" type="text" class="form-control" />
					<div class="alert alert-danger h5 error-summary" role="alert">
						<form:errors path="email" class="form-label" />
					</div>
				</div>
				<div class="row">
					<span>Số điện thoại</span>
					<form:input path="phoneNumber" type="text" class="form-control phone" />
					<div class="alert alert-danger h5 error-summary" role="alert">
						<form:errors path="phoneNumber" class="form-label" />
					</div>
				</div>
				<div class="row form-group">
					<button name="btnLogin" class="btn btn-primary" type="submit">Đăng Ký</button>
				</div>
				<div class="row form-group">
					<button name="btnLogin" class="btn btn-info" onclick="history.go(-1);" type="button">Trở Về</button>
				</div>
			</form:form>
		</div>
	</div>
	<script src="<c:url value="/resources/js/jquery.js" />"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/jquery-ui.min.js" />"></script>
	<script src="<c:url value="/resources/js/functions.js" />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			checkError();
		});
	</script>
</body>
</html>