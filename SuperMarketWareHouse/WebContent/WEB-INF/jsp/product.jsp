<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="col-xs-12 col-md-9 content" id="products">
	<div class="title">
		<h2 class="text-center title">Danh Sách Sản Phẩm</h2>
	</div>
	<div class="container-fluid">
		<div class="alert alert-danger h3 error-summary" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			${msg}
		</div>
		<c:choose>
			<c:when test="${listProducts.size() == 0}">
				<h3>Không có sản phẩm</h3>
			</c:when>
			<c:when test="${listProducts.size() > 0}">
				<a href='<c:url value="/products/create"/>' class="btn btn-info" style="width: 100%; font-weight: 600; text-transform: uppercase">Thêm</a>
				<div class="table-responsive scrollbar">
					<table class="table">
						<thead>
							<tr>
								<th class="col-xs-2">Tên sản phẩm</th>
								<th class="col-xs-1">Giá</th>
								<th class="col-xs-1">Số lượng tồn</th>
								<th class="col-xs-2">Nhà sản xuất</th>
								<th class="col-xs-1">Ngành hàng</th>
								<th class="col-xs-2">Nhóm hàng</th>
								<th class="col-xs-1">Tình trạng</th>
								<th class="col-xs-1">Đơn vị tính</th>
								<th class="col-xs-1">Chức năng</th>
							</tr>
						</thead>
						<tbody class="scrollbar">
							<c:forEach var="item" items="${listProducts}">
								<tr>
									<td class="col-xs-2">${item.id.name}</td>
									<td class="money col-xs-1">${item.id.price}</td>
									<td class="col-xs-1">${item.id.stock}</td>
									<td class="col-xs-2">${item.id.nameProducer}</td>
									<td class="col-xs-1">${item.id.nameCategoryProduct}</td>
									<td class="col-xs-2">${item.id.nameTypeProduct}</td>
									<c:choose>
										<c:when test="${item.id.status == true}">
											<td class="col-xs-1">Còn hàng</td>
										</c:when>

										<c:when test="${item.id.status == false}">
											<td class="col-xs-1">Hết hàng</td>
										</c:when>
									</c:choose>
									<td>${item.id.nameUnitCalculate}</td>
									<td class="col-xs-1">
										<a href='<c:url value="/products/edit/${item.id.productId}"/>' class="btn btn-warning">Sửa</a>
										<a href='<c:url value="/products/delete/${item.id.productId}"/>' class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn xóa sản phẩm?')">Xóa</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:when>
		</c:choose>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		checkError();
	});
</script>

