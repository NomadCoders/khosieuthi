<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<form:form method="post" action="information" modelAttribute="accountInfor" class="container" onsubmit="return checkFormValid()">
	<div id="user-info">
		<div class="col-xs-12 form-group">
			<div class="container-fluid">
				<ul class="nav nav-tabs nav-justified title">
					<li class="active">
						<a class="h2" href='<c:url value="/account/information"/>'>Thông Tin Tài Khoản</a>
					</li>
					<li>
						<a class="h2" href='<c:url value="/account/change-password"/>'>Thay Mật Khẩu</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="info col-sm-6 col-md-6 col-md-offset-3 form-group">
			<div class="alert h3 error-summary" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				${msg}
			</div>
			<div class="row form-group">
				<form:input path="name" class="form-control" readonly="true" />
				<div class="alert alert-danger h5 error-summary" role="alert">
					<form:errors path="name" class="form-label" />
				</div>
			</div>
			<div class="row form-group">
				<span class="">Họ Và Tên</span>
				<form:input path="fullName" class="form-control" readonly="true" />
				<div class="alert alert-danger h5 error-summary" role="alert">
					<form:errors path="fullName" class="form-label" />
				</div>
			</div>

			<div class="row form-group">
				<span class="">Email</span>
				<form:input path="email" class="form-control" readonly="true" />
				<div class="alert alert-danger h5 error-summary" role="alert">
					<form:errors path="email" class="form-label" />
				</div>
			</div>

			<div class="row form-group">
				<span class="">Số Điện Thoại</span>
				<form:input path="phoneNumber" class="form-control phone number" readonly="true" />
				<div class="alert alert-danger h5 error-summary" role="alert">
					<form:errors path="phoneNumber" class="form-label" />
				</div>
			</div>

			<div class="row form-group">
				<span class="">Địa Chỉ</span>
				<form:input path="address" class="form-control" readonly="true" />
				<div class="alert alert-danger h5 error-summary" role="alert">
					<form:errors path="address" class="form-label" />
				</div>
			</div>
			<div class="row form-group">
				<button class="btn btn-info" type="button">Thay Đổi Thông Tin</button>
				<button class="btn btn-success hide">Hoàn Thành</button>
			</div>
		</div>
	</div>
</form:form>

<script type="text/javascript">
	$(document).ready(function() {
		checkError();
	});
</script>