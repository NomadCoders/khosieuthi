<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:url value="/account/edit/${userInfor.id}" var="userActionUrl" />
<form:form method="post"  class="container" modelAttribute="userInfor" action="${userActionUrl}" onsubmit="return checkFormValid()">
	<div id="detail" class="container-fluid">
		<h1 class="text-center">Cập Nhật Thông Tin Tài Khoản</h1>
		<div class="info col-sm-6 col-md-6 form-group">
			<div class="alert alert-danger h3 error-summary" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				${msg}
			</div>
		</div>
		<div class="col-sm-6 col-md-6 form-group">
			<div class="product-image">
				<h1>Ảnh User</h1>
				<img alt="user image" src='<c:url value="/resources/img/lg-background.jpg"></c:url>' />
				<button type="button" class="btn btn-info col-sm-12 col-md-12">Cập nhật</button>
			</div>
		</div>
		<div class="info col-sm-6 col-md-6 form-group">
			<div class="row form-group">
				<span class="col-md-12 h4">Tên Đăng Nhập</span>
				<div class="col-md-8 col-sm-8">
					<form:input path="name" type="text" class="form-control" readonly="true"/>
					<span class="error text-danger error-summary"></span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-md-12 h4">Họ Và Tên</span>
				<div class="col-md-8 col-sm-8">
					<form:input path="fullName" type="text" class="form-control" />
					<span class="error text-danger h5 error-summary"><form:errors path="fullName"/> </span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-md-12 h4">Số Điện Thoại</span>
				<div class="col-md-8 col-sm-8">
					<form:input path="phoneNumber" type="text" class="form-control phone number" />
					<span class="error text-danger h5 error-summary"><form:errors path="phoneNumber"/> </span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-md-12 h4">Email</span>
				<div class="col-md-8 col-sm-8">
					<form:input path="email" type="text" class="form-control" readonly="true" />
					<span class="error text-danger"><form:errors path="email"/> </span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-md-12 h4">Địa Chỉ</span>
				<div class="col-md-8 col-sm-8">
					<form:input path="address" type="text" class="form-control" />
					<span class="error text-danger h5 error-summary"><form:errors path="address"/> </span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-md-12 h4">Tình Trạng</span>
				<div class="col-md-8 col-sm-8">
					<c:choose>
						<c:when test="${userInfor.status == true }">
							<label class="label-control">Hoạt Động</label>
						</c:when>
						<c:when test="${userInfor.status == false }">
							<label class="label-control">Đã Khóa</label>
						</c:when>
					</c:choose>
					<form:input path="status" type="hidden"/>
					<span class="error text-danger"> </span>
				</div>
			</div>
			<div class="row form-group">
				<span class="col-md-12 h4">Quyền Hạn</span>
				<div class="col-md-8 col-sm-8">
					<span>Admin</span>
					<form:radiobutton path="permission" value="true" />
					&nbsp;&nbsp;&nbsp;
					<span>Member</span>
					<form:radiobutton path="permission" value="false" />
					<span class="error text-danger"> </span>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-8 col-sm-8">
					<input type="submit" class="btn btn-info" value="Lưu" />
					<button name="btnLogin" class="btn btn-info" onclick="history.go(-1);" type="button">Trở Về</button>
				</div>
			</div>
		</div>
	</div>
</form:form>