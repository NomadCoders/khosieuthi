<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<form:form method="post" action="change-password" modelAttribute="changePassword" class="container" onsubmit="return checkFormValid()">
	<div id="user-info">
		<div class="col-xs-12 form-group">
			<div class="container-fluid">
				<ul class="nav nav-tabs nav-justified title">
					<li>
						<a class="h2" href='<c:url value="/account/information"/>'>Thông Tin Tài Khoản</a>
					</li>
					<li class="active">
						<a class="h2" href='<c:url value="/account/change-password"/>'>Thay Mật Khẩu</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="info col-sm-6 col-md-6 col-md-offset-3 form-group">
			<div class="alert alert-danger h3 error-summary" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			${msg}
			</div>
			<div class="row form-group">
				<span class="">Mật Khẩu Cũ</span>
				<form:input path="currentPassword" class="form-control" type="password" />
				<div class="alert alert-danger h5 error-summary" role="alert"><form:errors path="currentPassword"/></div>
			</div>
			<div class="row form-group">
				<span class="">Mật Khẩu Mới</span>
				<form:input path="newPassword" class="form-control" type="password" />
				<div class="alert alert-danger h5 error-summary" role="alert"><form:errors path="newPassword"/></div>
			</div>
			<div class="row form-group">
				<span class="">Nhập Lại Mật Khẩu Mới</span>
				<form:input path="rePassword" class="form-control" type="password" />
				<div class="alert alert-danger h5 error-summary" role="alert"><form:errors path="rePassword"/></div>
			</div>
			<div class="row form-group">
				<button class="btn btn-success">Hoàn Thành</button>
			</div>
		</div>
	</div>
</form:form>

<script type="text/javascript">
	$(document).ready(function() {
		checkError();
	});
</script>