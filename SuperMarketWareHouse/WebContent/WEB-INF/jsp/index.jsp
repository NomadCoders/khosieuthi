<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="col-xs-12 col-md-9 content" id="dashboard">
	<div class="container-fluid">
		<div class="panel-group">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h2 class="title">Thống kê thông tin kho</h2>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<span> Tổng sản phẩm: </span>
						</div>
						<div class="col-sm-2 col-md-2" style="text-align: right;">
							<span class="text-danger">${statistics.totalNumberProducts}</span>
						</div>
						<div class="col-sm-2 col-md-2">
							<span> Sản phẩm</span>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<span> Tổng danh mục: </span>
						</div>
						<div class="col-sm-2 col-md-2" style="text-align: right;">
							<span class="text-danger">${statistics.totalNumberCategorys}</span>
						</div>
						<div class="col-sm-2 col-md-2">
							<span> Mục</span>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<span> Tổng nhóm hàng: </span>
						</div>
						<div class="col-sm-2 col-md-2" style="text-align: right;">
							<span class="text-danger">${statistics.totalNumberTypePorducts}</span>
						</div>
						<div class="col-sm-2 col-md-2">
							<span> Nhóm</span>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<span> Tổng số lượng tài khoản: </span>
						</div>
						<div class="col-sm-2 col-md-2" style="text-align: right;">
							<span class="text-danger">${statistics.totalNumberAccounts}</span>
						</div>
						<div class="col-sm-2 col-md-2">
							<span> Tài khoản</span>
						</div>
					</div>
				</div>
			</div>
			<c:if test="${statistics.listCategories.size() > 0}">
				<c:forEach var="item" items="${statistics.listCategories}">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h2 class="title">Thống kê thông tin ${item.nameCategory}</h2>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-4 col-md-4">
									<span> Tổng số lượng sản phẩm: </span>
								</div>
								<div class="col-sm-2 col-md-2" style="text-align: right;">
									<span class="text-danger">${item.totalProducts}</span>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="progress">
										<div class="progress-bar progress-bar-striped active">
											<span></span>
										</div>
									</div>
								</div>
							</div>
							<c:forEach var="typeItem" items="${item.listTypeProducts}">
								<div class="row">
									<div class="col-sm-4 col-md-4">
										<span>${typeItem.nameTypeProducts}: </span>
									</div>
									<div class="col-sm-2 col-md-2" style="text-align: right;">
										<span class="text-danger">${typeItem.totalProducts}</span>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="progress">
											<div class="progress-bar progress-bar-striped active">
												<span></span>
											</div>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</c:forEach>
			</c:if>
		</div>
	</div>
</div>