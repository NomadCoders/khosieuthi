<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div id="import" class="container">
	<h1 class="title">Phiếu Nhập</h1>
	<form:form method="post" action="create" modelAttribute="importForm" onsubmit="return checkFormValid()">
		<div class="alert alert-danger h3 error-summary" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<form:errors path="listProducts" />
		</div>
		<div id="import-info">
			<div class="row form-group">
				<div class="col-sm-4 col-md-4">
					<label>
						Danh Sách mã đơn hàng(
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:select path="goodsOrderId" items="${goodsOrderList}" itemValue="goodsOrderId" itemLabel="goodsOrderId" class="form-control" />
						<div class="alert alert-danger h5 error-summary" role="alert"></div>
					</div>
				</div>
				<div class="col-sm-8 col-md-8">
					<label>
						Họ Tên Người Giao(
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="nameDeliver" type="text" class="form-control" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="nameDeliver" />
						</div>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-4 col-md-4">
					<label>
						Tên Đơn Vị Giao(
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="nameProducer" type="text" class="form-control" readonly="true" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="nameProducer" />
						</div>
					</div>
				</div>
				<div class="col-sm-8 col-md-8">
					<label>
						Địa Chỉ(
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="address" type="text" class="form-control" readonly="true" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="address" />
						</div>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-4 col-md-4">
					<label>
						Số Điện Thoại (
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="phoneNumber" type="text" class="form-control phone number" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="phoneNumber" />
						</div>
					</div>
				</div>
				<div class="col-sm-8 col-md-8">
					<label> Ghi Chú </label>
					<div class="col-sm-12 col-md-12">
						<form:input path="note" type="text" class="form-control" />
						<div class="alert alert-danger h5 error-summary" role="alert"></div>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row form-group">
			<!-- <button class="btn btn-success" style="width: 100%">Thêm</button> -->
			<h2>Danh Sách Sản Phẩm</h2>
		</div>
		<c:choose>
			<c:when test="${importForm.listProducts.size() == 0}">
				<h2>Không có sản phẩm</h2>
			</c:when>
			<c:when test="${importForm.listProducts.size() > 0}">
				<table id="review-order" class="table table-bordered table-responsive">
					<thead>
						<tr>
							<td>Mã Hàng</td>
							<td>Tên Mặt Hàng</td>
							<td>Đơn Vị Tính</td>
							<td>Số Lượng</td>
							<td>Đơn Giá</td>
							<td>Thành Tiền</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div id="summary">
					<span>
						Tổng Tiền
						<span class="money"></span>
						Đồng
					</span>
					<input name="hidTotalPrice" type="hidden" />
				</div>
				<div class="row">
					<button type="submit" class="btn btn-info" style="width: 100%">Hoàn thành</button>
				</div>
			</c:when>
		</c:choose>
	</form:form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		checkError();
	});
</script>