<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="col-xs-12 col-md-3 bg-primary sidebar nav nav-stacked">
	<h2 class="text-center title">Danh Mục Sản Phẩm</h2>
	<div>
		<c:forEach var="item" items="${listCategory}">
			<c:choose>
				<c:when test="${item.typeproducts.size() >0}">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<span class="glyphicon glyphicon-plus-sign collapse-custom"></span>
							<a href='<c:url value="/products/${item.categoryId}"/>' class="">${item.name}</a>
						</div>
						<div class="panel-body child-index container-fluid">
							<c:forEach var="typeProduct" items="${item.typeproducts }">
								<c:if test="${typeProduct.isDelete == false}">
									<div class="seperator"></div>
									<div class="row">
										<a href='<c:url value="/products/${item.categoryId}/${typeProduct.typeProductId}"/>'>${typeProduct.name}</a>
									</div>
								</c:if>
							</c:forEach>
							<div class="seperator"></div>
						</div>
					</div>
				</c:when>
				<c:when test="${item.typeproducts.size() == 0}">
					<li class="form-group">
						<a href="<c:url value="/products/${item.categoryId}"/>" class="btn btn-default">${item.name}</a>
					</li>
				</c:when>
			</c:choose>

		</c:forEach>
	</div>
</div>