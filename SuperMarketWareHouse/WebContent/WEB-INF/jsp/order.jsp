<%@page import="utils.ManagerLogin"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div id="order" class="container">
	<h1 class="title">TẠO ĐƠN HÀNG</h1>
	<form:form method="post" modelAttribute="goodsOrder" action="create" onsubmit="return checkFormValid()">
		<div id="buyer-info">
			<div class="row form-group">
				<div class="col-sm-6 col-md-6 col-md-offset-3">
					<div class="alert alert-danger h3 error-summary" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<form:errors path="listProducts" />
					</div>
				</div>
				<div class="col-sm-8 col-md-8">
					<label>
						Tên Người Mua (
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="nameBuyer" type="text" value="<%=ManagerLogin.getNameLogin()%>" class="form-control" readonly="true" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors patch="nameBuyer" />
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<label>
						Tên Đơn Vị (
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:select path="producerId" items="${goodsOrder.listProducers}" itemValue="producerId" itemLabel="name" class="form-control" />
						<div class="alert alert-danger h5 error-summary" role="alert"></div>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-3 col-md-4">
					<label>
						Email(
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<!-- <input type="text" name="txtBuyerEmail" class="form-control number"/>  -->
						<form:input path="email" class="form-control" />
						<div class="alert alert-danger h5 error-summary" role="alert"></div>
					</div>
				</div>
				<div class="col-sm-3 col-md-4">
					<label>
						Số Điện Thoại (
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="phoneNumber" type="text" class="form-control phone number" readonly="true" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="phoneNumber" />
						</div>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-12 col-md-12">
					<label>
						Địa Chỉ (
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<form:input path="address" type="text" class="form-control" readonly="true" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="address" />
						</div>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-12 col-md-12">
					<label>
						Địa Chỉ Kho (
						<span class="text-danger"> * </span>
						)
					</label>
					<div class="col-sm-12 col-md-12">
						<!-- <input type="text" name="txtBuyerAddress" class="form-control"/>  -->
						<form:input path="addressWareHouse" type="text" class="form-control" />
						<div class="alert alert-danger h5 error-summary" role="alert">
							<form:errors path="addressWareHouse" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row form-group">
			<div class="col-sm-8 col-md-4">
				<label>Danh Mục</label>
				<div class="col-sm-12 col-md-12">
					<form:select path="categoryId" items="${goodsOrder.listCategory}" itemValue="categoryId" itemLabel="name" class="form-control" />
				</div>
			</div>
			<div class="col-sm-8 col-md-4">
				<label>Nhóm Hàng</label>
				<div class="col-sm-12 col-md-12">
					<select class="form-control" id="productType">
					</select>
				</div>
			</div>
			<div class="col-sm-4 col-md-4">
				<label>
					Số Lượng Sản Phẩm (
					<span class="text-danger"> * </span>
					)
				</label>
				<div class="col-sm-12 col-md-12">
					<form:input path="quantity" type="number" class="form-control" min="1" />
					<!--  <input type="text" name="txtSearchProduct" class="form-control"/> -->
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-sm-8 col-md-8">
				<label>
					Tên Sản Phẩm (
					<span class="text-danger"> * </span>
					)
				</label>
				<div class="col-sm-12 col-md-12 form-group">
					<select class="form-control">
					</select>
				</div>
			</div>
			<div class="col-sm-4 col-md-4">
				<label>Ghi chú</label>
				<div class="col-sm-12 col-md-12">
					<!--  <input type="text" name="txtSearchProduct" class="form-control"/> -->
					<form:input path="note" type="text" class="form-control" />
				</div>
			</div>
		</div>
		<div class="row form-group">
			<button class="btn btn-success" style="width: 100%" type="button">Thêm</button>
		</div>
		<hr>
		<table id="review-order" class="table table-bordered table-responsive">
			<thead>
				<tr>
					<td>Mã Hàng</td>
					<td>Tên Hàng</td>
					<td>Đơn Vị Tính</td>
					<td>Số Lượng</td>
					<td>Đơn Giá</td>
					<td>Thành Tiền</td>
					<td></td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<hr>
		<div id="summary">
			<span>
				Tổng Tiền
				<span class="money"></span>
				Đồng
			</span>
			<input name="hidTotalPrice" type="hidden" />
		</div>
		<div class="row">
			<button type="submit" class="btn btn-info" style="width: 100%">Hoàn thành</button>
		</div>
	</form:form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		checkError();
	});
</script>
