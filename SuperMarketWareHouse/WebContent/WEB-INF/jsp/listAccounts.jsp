<%@page import="utils.ManagerLogin"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" uri="http://www.springframework.org/tags"%>
<t:eval expression="T(utils.ManagerLogin).getUserNameLogin()" var="userLogin"/>
<div class="col-md-9 col-sm-8 panel panel-primary content">
	<h2 class="panel-heading text-center">Danh Sách Tài Khoản</h2>
	<div class="alert alert-danger h3 error-summary" role="alert">
		<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		${msg}
	</div>
	<c:choose>
		<c:when test="${listAccounts.size() == 0}">
			<h3>Không có tài khoản</h3>
		</c:when>

		<c:when test="${listAccounts.size() > 0}">
			<a href='<c:url value="/account/register"/>' class="btn btn-info" style="width: 100%; font-weight: 600; text-transform: uppercase">Tạo Tài Khoản</a>
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th class="col-xs-2">Tên đăng nhập</th>
							<th class="col-xs-2">Họ và tên</th>
							<th class="col-xs-2">Email</th>
							<th class="col-xs-1">Số điện thoại</th>
							<th class="col-xs-2">Địa Chỉ</th>
							<th class="col-xs-1">Loại Tài Khoản</th>
							<th class="col-xs-1">Tình trạng</th>
							<th class="col-xs-1">Chức năng</th>
						</tr>
					</thead>
					<tbody class="scrollbar">
						<c:forEach var="item" items="${listAccounts}">
							<tr>
								<td class="col-xs-2">${item.name}</td>
								<td class="col-xs-2">${item.fullName}</td>
								<td class="col-xs-2">${item.email}</td>
								<td class="col-xs-1">${item.phoneNumber}</td>
								<td class="col-xs-2">${item.address}</td>
								<c:choose>
									<c:when test="${item.permission == true}">
										<td class="col-xs-1">Admin</td>
									</c:when>

									<c:when test="${item.permission == false}">
										<td class="col-xs-1">Member</td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${item.status == true}">
										<td class="col-xs-1">Hoạt động</td>
									</c:when>
	
									<c:when test="${item.status == false}">
										<td class="col-xs-1">Đã Khóa</td>
									</c:when>
								</c:choose>
								<td class="col-xs-1">
									<c:if test='${item.name != "Admin" && item.name != userLogin}'>
										<a href='<c:url value="/account/edit/${item.id}"/>' class="btn btn-warning">Sửa</a>
										<a href='<c:url value="/account/delete/${item.id}"/>' class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn xóa tài khoản?')">Xóa</a>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</c:when>
	</c:choose>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		checkError();
	});
</script>
