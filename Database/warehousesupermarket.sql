-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 08, 2016 at 09:55 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `warehousesupermarket`
--
CREATE DATABASE IF NOT EXISTS `warehousesupermarket` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `warehousesupermarket`;

DELIMITER $$

--
-- Procedures
--
DROP PROCEDURE IF EXISTS `statisticsProductInGoodsIssuedNote`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `statisticsProductInGoodsIssuedNote` (IN `yearChoose` INT, IN `monthChoose` INT, IN `categoryId` INT, OUT `totalProduct` BIGINT)  BEGIN
	if monthChoose = 0 then
		Select IFNULL(SUM(dgin.Quantity),0) into totalProduct 
        from goodsissuednote gin, detailgoodsissuednote dgin, products p 
        where YEAR(gin.DateCreate) = yearChoose And dgin.ProductId = p.ProductId And gin.id = dgin.id And p.CategoryProductId = categoryId;
		
        select tp.name, SUM(dgin.Quantity) as 'TotalProduct'
		from goodsissuednote gin, detailgoodsissuednote dgin, products p, typeproduct tp
		where YEAR(gin.DateCreate) = yearChoose And gin.id = dgin.id
		And dgin.ProductId = p.ProductId And p.ProductTypeId = tp.TypeProductId 
        And tp.CategoryProductId = categoryId
		Group by tp.TypeProductId;
	else
		Select IFNULL(SUM(dgin.Quantity),0) into totalProduct 
        from goodsissuednote gin, detailgoodsissuednote dgin, products p  
        where YEAR(gin.DateCreate) = yearChoose And MONTH(gin.DateCreate) = monthChoose 
        And dgin.ProductId = p.ProductId And gin.id = dgin.id 
        And p.CategoryProductId = categoryId;
        
        select tp.name, SUM(dgin.Quantity) as 'TotalProduct'
		from goodsissuednote gin, detailgoodsissuednote dgin, products p, typeproduct tp
		where YEAR(gin.DateCreate) = yearChoose And MONTH(gin.DateCreate) = monthChoose And gin.id = dgin.id
		And dgin.ProductId = p.ProductId And p.ProductTypeId = tp.TypeProductId And tp.CategoryProductId = categoryId
		Group by tp.TypeProductId;
    End if;
    
    
    
END$$

DROP PROCEDURE IF EXISTS `statisticsProductInGoodsReceiptNote`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `statisticsProductInGoodsReceiptNote` (IN `yearChoose` INT, IN `monthChoose` INT, IN `categoryId` INT, OUT `totalProduct` BIGINT)  BEGIN
	if monthChoose = 0 then
		Select IFNULL(SUM(dgrn.Quantity),0) into totalProduct 
        from goodsreceiptnote grn, detailgoodsreceiptnote dgrn, products p
        where YEAR(grn.DateCreate) = yearChoose And grn.id = dgrn.id
		And dgrn.ProductId = p.ProductId And p.CategoryProductId = categoryId;
        
		select tp.name, SUM(dgrn.Quantity) as 'TotalProduct'
		from goodsreceiptnote grn, detailgoodsreceiptnote dgrn, products p, typeproduct tp
		where YEAR(grn.DateCreate) = yearChoose And grn.id = dgrn.id
		And dgrn.ProductId = p.ProductId And p.ProductTypeId = tp.TypeProductId And tp.CategoryProductId = categoryId
		Group by tp.TypeProductId;
	else
		Select IFNULL(SUM(dgrn.Quantity),0) into totalProduct 
        from goodsreceiptnote grn, detailgoodsreceiptnote dgrn, products p
        where YEAR(DateCreate) = yearChoose
		And MONTH(DateCreate) = monthChoose And grn.id = dgrn.id
		And dgrn.ProductId = p.ProductId And p.CategoryProductId = categoryId;
        
        select tp.name, SUM(dgrn.Quantity) as 'TotalProduct'
		from goodsreceiptnote grn, detailgoodsreceiptnote dgrn, products p, typeproduct tp
		where YEAR(grn.DateCreate) = yearChoose And MONTH(grn.DateCreate) = monthChoose And grn.id = dgrn.id
		And dgrn.ProductId = p.ProductId And p.ProductTypeId = tp.TypeProductId And tp.CategoryProductId = categoryId
		Group by tp.TypeProductId;
    End if;
    
    
    
END$$

DELIMITER ;
--
-- Table structure for table `accounts`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FullName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Password` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PhoneNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Status` bit(1) DEFAULT NULL,
  `Permission` bit(1) DEFAULT NULL,
  `isDelete` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `accounts`:
--

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `FullName`, `Name`, `Password`, `Email`, `PhoneNumber`, `Address`, `Status`, `Permission`, `isDelete`) VALUES
(1, 'Nguyễn Trương Phi', 'Shing', 'e1adc3949ba59abbe56e057f2f883e', 'protocy@yahoo.com', '01234567890', '159 Hưng Phú', b'1', b'0', b'0'),
(2, 'Admin', 'Admin', 'e1adc3949ba59abbe56e057f2f883e', 'Admin@gmail.com', '01234567890', 'Quận 1 thành phố Hồ Chí Minh', b'1', b'1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `categoryproducts`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `categoryproducts`;
CREATE TABLE IF NOT EXISTS `categoryproducts` (
  `CategoryId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IsDelete` bit(1) DEFAULT NULL,
  PRIMARY KEY (`CategoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `categoryproducts`:
--

--
-- Dumping data for table `categoryproducts`
--

INSERT INTO `categoryproducts` (`CategoryId`, `Name`, `IsDelete`) VALUES
(1, 'Thực phẩm', b'0'),
(2, 'Hóa phẩm', b'0'),
(3, 'Đồ dùng', b'0'),
(4, 'May mặc', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `detailgoodorder`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `detailgoodorder`;
CREATE TABLE IF NOT EXISTS `detailgoodorder` (
  `GoodsOrderId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  `TotalMoney` bigint(20) DEFAULT NULL,
  `Note` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`GoodsOrderId`,`ProductId`),
  KEY `IXFK_DetailGoodOrder_GoodsOrders` (`GoodsOrderId`),
  KEY `IXFK_DetailGoodOrder_Products` (`ProductId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `detailgoodorder`:
--   `GoodsOrderId`
--       `goodsorder` -> `GoodsOrderId`
--   `ProductId`
--       `products` -> `ProductId`
--

--
-- Dumping data for table `detailgoodorder`
--

INSERT INTO `detailgoodorder` (`GoodsOrderId`, `ProductId`, `Quantity`, `Price`, `TotalMoney`, `Note`) VALUES
(1, 14, 2, 1450000, 2900000, NULL),
(1, 18, 2, 590000, 1800000, NULL),
(1, 19, 2, 890000, 1780000, NULL),
(2, 1, 2, 5500, 11000, NULL),
(2, 2, 1, 28000, 28000, NULL),
(3, 17, 4, 495000, 1180000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detailgoodsissuednote`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `detailgoodsissuednote`;
CREATE TABLE IF NOT EXISTS `detailgoodsissuednote` (
  `id` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  `TotalMoney` bigint(20) DEFAULT NULL,
  `Note` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ProductId`,`id`),
  KEY `IXFK_DetailGoodsIssuedNote_GoodsIssuedNote` (`id`),
  KEY `IXFK_DetailGoodsIssuedNote_Products` (`ProductId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `detailgoodsissuednote`:
--   `id`
--       `goodsissuednote` -> `id`
--   `ProductId`
--       `products` -> `ProductId`
--

--
-- Dumping data for table `detailgoodsissuednote`
--

INSERT INTO `detailgoodsissuednote` (`id`, `ProductId`, `Quantity`, `Price`, `TotalMoney`, `Note`) VALUES
(2, 3, 2, 16000, 32000, NULL),
(1, 6, 1, 0, 0, NULL),
(1, 12, 1, 0, 0, NULL),
(1, 40, 1, 0, 0, NULL),
(2, 40, 2, 250000, 500000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detailgoodsreceiptnote`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `detailgoodsreceiptnote`;
CREATE TABLE IF NOT EXISTS `detailgoodsreceiptnote` (
  `id` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Note` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GoodsOrderId` int(11) NOT NULL,
  PRIMARY KEY (`id`,`ProductId`),
  KEY `IXFK_DetailGoodsReceiptNote_DetailGoodOrder` (`GoodsOrderId`,`ProductId`),
  KEY `IXFK_DetailGoodsReceiptNote_GoodsReceiptNote` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `detailgoodsreceiptnote`:
--   `GoodsOrderId`
--       `detailgoodorder` -> `GoodsOrderId`
--   `ProductId`
--       `detailgoodorder` -> `ProductId`
--   `id`
--       `goodsreceiptnote` -> `id`
--

--
-- Dumping data for table `detailgoodsreceiptnote`
--

INSERT INTO `detailgoodsreceiptnote` (`id`, `ProductId`, `Quantity`, `Note`, `GoodsOrderId`) VALUES
(1, 14, 2, NULL, 1),
(1, 18, 2, NULL, 1),
(1, 19, 2, NULL, 1),
(2, 1, 2, NULL, 2),
(2, 2, 1, NULL, 2),
(3, 17, 4, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `goodsissuednote`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `goodsissuednote`;
CREATE TABLE IF NOT EXISTS `goodsissuednote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AccountId` int(11) DEFAULT NULL,
  `ReceiverName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressCompany` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressWareHouse` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `DateCreate` datetime DEFAULT NULL,
  `TotalQuantity` bigint(20) DEFAULT NULL,
  `TotalMoney` bigint(20) DEFAULT NULL,
  `Note` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_GoodsIssuedNote_Accounts` (`id`),
  KEY `fk_goodsissuednote_accounts1_idx` (`AccountId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `goodsissuednote`:
--   `AccountId`
--       `accounts` -> `id`
--

--
-- Dumping data for table `goodsissuednote`
--

INSERT INTO `goodsissuednote` (`id`, `AccountId`, `ReceiverName`, `AddressCompany`, `AddressWareHouse`, `DateCreate`, `TotalQuantity`, `TotalMoney`, `Note`) VALUES
(1, 2, 'Phi', 'Số 1 Cao Thắng quận 1 thành phố Hồ Chí Minh', '227 Nguyễn Văn Cừ', '2016-08-02 08:27:00', 3, 427000, NULL),
(2, 2, 'Phi', 'Số 1 Cao Thắng quận 1 thành phố Hồ Chí Minh', '227 Nguyễn Văn Cừ', '2016-08-01 06:30:00', 4, 532000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `goodsorder`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `goodsorder`;
CREATE TABLE IF NOT EXISTS `goodsorder` (
  `GoodsOrderId` int(11) NOT NULL AUTO_INCREMENT,
  `ProducerId` int(11) DEFAULT NULL,
  `AccountId` int(11) DEFAULT NULL,
  `TotalQuantity` bigint(20) DEFAULT NULL,
  `TotalMoney` bigint(20) DEFAULT NULL,
  `DateCreate` datetime DEFAULT NULL,
  `AddressWareHouse` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IsDone` bit(1) DEFAULT NULL,
  PRIMARY KEY (`GoodsOrderId`),
  KEY `IXFK_GoodsOrder_Accounts` (`AccountId`),
  KEY `IXFK_GoodsOrder_Producers` (`ProducerId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `goodsorder`:
--   `AccountId`
--       `accounts` -> `id`
--   `ProducerId`
--       `producers` -> `ProducerId`
--

--
-- Dumping data for table `goodsorder`
--

INSERT INTO `goodsorder` (`GoodsOrderId`, `ProducerId`, `AccountId`, `TotalQuantity`, `TotalMoney`, `DateCreate`, `AddressWareHouse`, `Email`, `Note`, `IsDone`) VALUES
(1, 5, 1, 6, 5860000, '2016-07-29 11:27:00', '159 Hưng Phú', 'test@gmail.com', NULL, b'1'),
(2, 3, 1, 3, 39000, '2016-07-30 00:11:00', '227 Nguyễn Văn Cừ', 'test@gmail.com', NULL, b'1'),
(3, 17, 1, 4, 1980000, '2016-07-30 00:25:00', '2 Cao Thắng quận 1 thành phố Hồ Chí Minh', 'test21@gmail.com', NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `goodsreceiptnote`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `goodsreceiptnote`;
CREATE TABLE IF NOT EXISTS `goodsreceiptnote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TotalQuantity` bigint(20) DEFAULT NULL,
  `TotalMoney` bigint(20) DEFAULT NULL,
  `GoodsOrderId` int(11) DEFAULT NULL,
  `NameDeliver` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateCreate` datetime DEFAULT NULL,
  `Note` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_GoodsReceiptNote_GoodsOrders` (`GoodsOrderId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `goodsreceiptnote`:
--   `GoodsOrderId`
--       `goodsorder` -> `GoodsOrderId`
--

--
-- Dumping data for table `goodsreceiptnote`
--

INSERT INTO `goodsreceiptnote` (`id`, `TotalQuantity`, `TotalMoney`, `GoodsOrderId`, `NameDeliver`, `DateCreate`, `Note`) VALUES
(1, 6, 5860000, 1, '', '2016-08-02 12:24:34', ''),
(2, 3, 39000, 2, '', '2016-08-02 12:25:21', ''),
(3, 4, 1980000, 3, 'Nguyễn Phi', '2016-08-03 16:10:44', 'Đủ sản phẩm');

-- --------------------------------------------------------

--
-- Table structure for table `producers`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `producers`;
CREATE TABLE IF NOT EXISTS `producers` (
  `ProducerId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StatusId` int(11) DEFAULT NULL,
  `Address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PhoneNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IsDelete` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ProducerId`),
  KEY `fk_producers_producersstatus1_idx` (`StatusId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `producers`:
--   `StatusId`
--       `producersstatus` -> `Id`
--

--
-- Dumping data for table `producers`
--

INSERT INTO `producers` (`ProducerId`, `Name`, `StatusId`, `Address`, `PhoneNumber`, `IsDelete`) VALUES
(1, 'Hải sản Vũng Tàu', 1, '42/8 Võ Thị Sáu, TP. Vũng Tàu', '0123456789', b'0'),
(2, 'Rau sạch Tây Ninh', 1, '23 Nguyễn Văn Linh, Hòa Thành, Tây Ninh', '0663640001', b'0'),
(3, 'Vinamilk', 1, '10 Tân Trào, phường Tân Phú, quận 7, Tp. HCM', '(08) 54 155 555', b'0'),
(4, 'Unilever', 1, '156 Nguyễn Lương Bằng, P. Tân Phú, Q. 7, TP. HCM', '08 5413 5685', b'0'),
(5, 'Việt Tiến', 1, '07 Lê Minh Xuân - Q. Tân Bình - TP Hồ Chí Minh ', '0838640800 ', b'0'),
(6, 'Vifon', 1, '913 Trường Chinh, P. Tây Thạnh, Q. Tân Phú, Tp HCM', '08 3853059', b'0'),
(7, 'Cholimex', 1, '631 - 633 Nguyễn Trãi, Phường 11, Quận 5, TP.HCM', '08 38547101', b'0'),
(8, 'Test nhà sản xuất ngưng hoạt động', 2, 'địa chỉ nhà sản xuất', 'SĐT NSX', b'0'),
(9, 'Test nhà sản xuất bị xóa', 1, 'địa chỉ nhà sản xuất', 'SĐT NSX', b'1'),
(10, 'Thực phẩm Thiên Ngọc', 1, 'Tổ 12, ấp Hồi Thạnh, X. Xuân Hiệp, H. Trà Ôn, Vĩnh Long', '0703789117', b'0'),
(11, 'Kinh Đô', 1, '138 – 142, Hai Bà Trưng, Phường Đa Kao, Quận 1, TP.HCM ', '0838270838', b'0'),
(12, 'Công Ty TNHH Red Bull Việt Nam', 1, 'Xa Lộ Hà Nội, Phường Bình Thắng, Thị Xã Dĩ An, Tỉnh Bình Dương, Việt Nam', '0838001005', b'0'),
(13, 'Sharp Việt Nam', 1, 'Tầng 3, Saigon Finance Center\r\nSố 9 Đinh Tiên Hoàng, Q.1, TP. HCM', '08 39 107 500', b'0'),
(14, 'Doanh Nghiệp Tư Nhân Nhựa Chợ Lớn', 1, '8H An Dương Vương, P. 16, Q. 8,Tp. Hồ Chí Minh (TPHCM)', '(08) 39805394 ', b'0'),
(15, 'Sunhouse Việt Nam', 1, '151 Thành Mỹ, phường 8, quận Tân Bình, TP.HCM ', '08.38691014', b'0'),
(16, 'Công ty Cổ phần Tập đoàn Thiên Long', 1, 'Lô 6-8-10-12, Đường số 3, KCN Tân Tạo Q. Bình Tân, Thành phố Hồ Chí Minh', '(+84) 8 3750 55 55 ', b'0'),
(17, 'CÔNG TY TNHH SẢN XUẤT HÀNG TIÊU DÙNG BÌNH TIÊN', 1, '22 Lý Chiêu Hoàng, Phường 10, Quận 6,Tp Hồ Chí Minh', '(08)38 753442', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `producersstatus`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `producersstatus`;
CREATE TABLE IF NOT EXISTS `producersstatus` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `producersstatus`:
--

--
-- Dumping data for table `producersstatus`
--

INSERT INTO `producersstatus` (`Id`, `Name`) VALUES
(1, 'Đang hoạt động'),
(2, 'Ngưng hoạt động');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `ProductId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  `Stock` int(11) DEFAULT NULL,
  `ProductTypeId` int(11) DEFAULT NULL,
  `CategoryProductId` int(11) DEFAULT NULL,
  `ProducerId` int(11) DEFAULT NULL,
  `IsDelete` bit(1) DEFAULT NULL,
  `Status` bit(1) DEFAULT NULL,
  `UnitCalculateId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ProductId`),
  KEY `IXFK_Products_CategoryProducts` (`CategoryProductId`),
  KEY `IXFK_Products_Producers` (`ProducerId`),
  KEY `IXFK_Products_TypeProduct` (`ProductTypeId`),
  KEY `IXFK_Products_UnitCalculate` (`UnitCalculateId`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `products`:
--   `CategoryProductId`
--       `categoryproducts` -> `CategoryId`
--   `ProducerId`
--       `producers` -> `ProducerId`
--   `ProductTypeId`
--       `typeproduct` -> `TypeProductId`
--   `UnitCalculateId`
--       `unitcalculate` -> `id`
--

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ProductId`, `Name`, `Price`, `Stock`, `ProductTypeId`, `CategoryProductId`, `ProducerId`, `IsDelete`, `Status`, `UnitCalculateId`) VALUES
(1, 'Sữa tiệt trùng Vinamilk Star dạng túi', 5500, 100, 1, 1, 3, b'0', b'1', 9),
(2, 'Sữa tươi Vinamilk 100% hương dâu hộp 180ml', 28000, 120, 1, 1, 3, b'0', b'1', 5),
(3, 'Gạo thơm lài sữa', 16000, 315, 2, 1, 10, b'0', b'1', 7),
(4, 'Gạo thơm lài', 15000, 149, 2, 1, 10, b'0', b'1', 7),
(5, 'Bánh Cosy Marie 450g', 35000, 112, 3, 1, 11, b'0', b'1', 3),
(6, 'Tương ớt SRIRACHA 520G', 27000, 156, 4, 1, 7, b'0', b'1', 1),
(7, 'Reb Bull', 9500, 216, 5, 1, 12, b'0', b'1', 2),
(8, 'Dầu gội trị gàu CLEAR bạc hà 900g', 135000, 124, 6, 2, 4, b'0', b'1', 1),
(9, 'Nước Giặt OMO Matic Túi Cửa Trước 2.7kg', 125000, 145, 7, 2, 4, b'0', b'1', 9),
(10, 'Nồi cơm điện Sharp KSZT18V 1.8 lít, 830W', 1320000, 50, 8, 3, 13, b'0', b'1', 8),
(11, 'XE ĐẨY TẬP ĐI K2 (SƯ TỬ) M1516-BB10', 350000, 24, 9, 3, 14, b'0', b'1', 8),
(12, 'Chảo chống dính sâu Sunhouse SHS-20cm', 150000, 67, 10, 3, 15, b'0', b'1', 8),
(13, 'Bút bi Thiên Long TL027', 3000, 149, 11, 3, 16, b'0', b'1', 8),
(14, 'QUẦN TÂY - 5C4004CT2-QT0', 1450000, 46, 12, 4, 5, b'0', b'1', 8),
(15, 'Áo sơ mi VIỆT NAM LT3215A-M1', 450000, 78, 13, 4, 5, b'0', b'1', 8),
(16, 'Bộ trang phục cướp biển - trẻ em', 450000, 56, 14, 4, 5, b'0', b'1', 8),
(17, 'HUNTER LITEKNIT SUMMER VIBES', 495000, 45, 15, 4, 17, b'0', b'1', 11),
(18, 'Cà vạt sọc caro', 590000, 46, 16, 4, 5, b'0', b'1', 8),
(19, 'Túi xách nam chéo vai', 890000, 12, 17, 4, 5, b'0', b'1', 8),
(20, 'Áo lót nam thun trắng', 40000, 27, 18, 4, 5, b'0', b'1', 8),
(21, 'Áo gối ôm', 40000, 48, 19, 4, 5, b'0', b'1', 8),
(22, 'Test01', 10000, 0, 1, 1, 1, b'1', b'1', 5),
(23, 'sữa đặc không đường', 24000, 78, 1, 1, 7, b'0', b'1', 2),
(24, 'Bàn chải vệ sinh', 13000, 89, 7, 2, 7, b'0', b'1', 8),
(25, 'búp bê baby', 123000, 29, 9, 3, 7, b'0', b'1', 8),
(26, 'Victorya Secret Special Version Bra', 12000000, 12, 18, 4, 7, b'0', b'1', 8),
(27, 'sữa tắm dưỡng da', 74000, 78, 6, 2, 16, b'0', b'1', 3),
(28, 'nước ép dâu', 30000, 112, 5, 1, 16, b'0', b'1', 2),
(29, 'bình thủy', 67000, 48, 8, 3, 16, b'0', b'1', 8),
(30, 'áo thun nam thiên long', 78000, 69, 12, 4, 16, b'0', b'1', 8),
(31, 'Nước tương hảo hảo', 23000, 235, 4, 1, 15, b'0', b'1', 1),
(32, 'kem đánh răng sunhouse', 43000, 142, 7, 2, 15, b'0', b'1', 8),
(33, 'sách bài tập toán', 45000, 40, 11, 3, 15, b'0', b'1', 8),
(34, 'Nón tai bèo', 34000, 27, 16, 4, 15, b'0', b'1', 8),
(35, 'váy ngắn màu kem', 590000, 59, 13, 4, 5, b'0', b'1', 8),
(36, 'cà rốt', 15000, 54, 2, 1, 2, b'0', b'1', 7),
(37, 'bột canh Vifon', 20000, 78, 4, 1, 6, b'0', b'1', 4),
(38, 'nước mắm chay Hoa Sen', 13000, 10, 4, 1, 8, b'0', b'1', 1),
(39, 'Nước ép nho', 32000, 10, 5, 1, 9, b'0', b'1', 1),
(40, 'Cá ngừ đại dương phi lê', 250000, 30, 2, 1, 1, b'0', b'1', 7),
(41, 'thịt cua đông lạnh', 345000, 22, 2, 1, 1, b'0', b'1', 7),
(42, 'củ cải trắng', 10000, 40, 2, 1, 2, b'0', b'1', 7),
(43, 'cà chua', 13000, 60, 2, 1, 2, b'0', b'1', 7),
(44, 'Hạt nêm Aji no moto', 23000, 46, 4, 1, 6, b'0', b'1', 4),
(45, 'Bánh trung thu thập cẩm', 45000, 97, 3, 1, 11, b'0', b'1', 3),
(46, 'Bếp từ sharp', 2150000, 29, 8, 3, 13, b'0', b'1', 8),
(47, 'Bộ xếp hình', 115000, 34, 9, 3, 14, b'0', b'1', 8),
(48, 'Lăn khử mùi', 89000, 123, 6, 2, 16, b'0', b'1', 3);

-- --------------------------------------------------------

--
-- Table structure for table `typeproduct`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `typeproduct`;
CREATE TABLE IF NOT EXISTS `typeproduct` (
  `TypeProductId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IsDelete` bit(1) DEFAULT NULL,
  `CategoryProductId` int(11) NOT NULL,
  PRIMARY KEY (`TypeProductId`),
  KEY `IXFK_TypeProduct_CategoryProducts` (`CategoryProductId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `typeproduct`:
--   `CategoryProductId`
--       `categoryproducts` -> `CategoryId`
--

--
-- Dumping data for table `typeproduct`
--

INSERT INTO `typeproduct` (`TypeProductId`, `Name`, `IsDelete`, `CategoryProductId`) VALUES
(1, 'Sữa và sản phẩm từ sữa', b'0', 1),
(2, 'Lương thực', b'0', 1),
(3, 'Bánh, kẹo, mứt', b'0', 1),
(4, 'Dầu ăn, nước chấm, gia vị', b'0', 1),
(5, 'Nước giải khát', b'0', 1),
(6, 'Chăm sóc sức khỏe và sắc đẹp', b'0', 2),
(7, 'Hóa phẩm, sản phẩm vệ sinh', b'0', 2),
(8, 'Điện gia dụng', b'0', 3),
(9, 'Đồ chơi', b'0', 3),
(10, 'Đồ dùng gia đình', b'0', 3),
(11, 'Văn hóa phẩm', b'0', 3),
(12, 'Quần áo nam', b'0', 4),
(13, 'Quần áo nữ', b'0', 4),
(14, 'Quần áo trẻ em', b'0', 4),
(15, 'Giày dép, vớ, lót giày', b'0', 4),
(16, 'Nón, dù, khăn tay, cà vạt, dây nịt', b'0', 4),
(17, 'Balo, túi xách', b'0', 4),
(18, 'Đồ lót', b'0', 4),
(19, 'Dệt may', b'0', 4),
(20, 'Test xóa loại sản phẩm', b'1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `unitcalculate`
--
-- Creation: Aug 06, 2016 at 12:07 PM
--

DROP TABLE IF EXISTS `unitcalculate`;
CREATE TABLE IF NOT EXISTS `unitcalculate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IsDelete` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `unitcalculate`:
--

--
-- Dumping data for table `unitcalculate`
--

INSERT INTO `unitcalculate` (`id`, `Name`, `IsDelete`) VALUES
(1, 'Chai', b'0'),
(2, 'Lon', b'0'),
(3, 'Hộp', b'0'),
(4, 'Gói', b'0'),
(5, 'Lốc', b'0'),
(6, 'Thùng', b'0'),
(7, 'Kg', b'0'),
(8, 'Cái', b'0'),
(9, 'Túi', b'0'),
(10, 'Lít', b'0'),
(11, 'Đôi', b'0'),
(12, 'bộ', b'0');

-- --------------------------------------------------------

--
-- Stand-in structure for view `viewproducts`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `viewproducts`;
CREATE TABLE IF NOT EXISTS `viewproducts` (
`ProductId` int(11)
,`Name` varchar(50)
,`Price` int(11)
,`Stock` int(11)
,`TypeProductId` int(11)
,`NameTypeProduct` varchar(50)
,`CategoryId` int(11)
,`NameCategoryProduct` varchar(50)
,`ProducerId` int(11)
,`NameProducer` varchar(50)
,`Status` bit(1)
,`NameUnitCalculate` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `viewproducts`
--
DROP TABLE IF EXISTS `viewproducts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `viewproducts`  AS  select `p`.`ProductId` AS `ProductId`,`p`.`Name` AS `Name`,`p`.`Price` AS `Price`,`p`.`Stock` AS `Stock`,`tp`.`TypeProductId` AS `TypeProductId`,`tp`.`Name` AS `NameTypeProduct`,`cp`.`CategoryId` AS `CategoryId`,`cp`.`Name` AS `NameCategoryProduct`,`pr`.`ProducerId` AS `ProducerId`,`pr`.`Name` AS `NameProducer`,`p`.`Status` AS `Status`,`uc`.`Name` AS `NameUnitCalculate` from ((((`products` `p` join `producers` `pr` on((`p`.`ProducerId` = `pr`.`ProducerId`))) join `categoryproducts` `cp` on((`p`.`CategoryProductId` = `cp`.`CategoryId`))) join `typeproduct` `tp` on((`p`.`ProductTypeId` = `tp`.`TypeProductId`))) join `unitcalculate` `uc` on((`p`.`UnitCalculateId` = `uc`.`id`))) where ((`p`.`IsDelete` = 0) and (`pr`.`IsDelete` = 0) and (`cp`.`IsDelete` = 0)) ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detailgoodorder`
--
ALTER TABLE `detailgoodorder`
  ADD CONSTRAINT `FK_DetailGoodOrder_GoodsOrders` FOREIGN KEY (`GoodsOrderId`) REFERENCES `goodsorder` (`GoodsOrderId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_DetailGoodOrder_Products` FOREIGN KEY (`ProductId`) REFERENCES `products` (`ProductId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `detailgoodsissuednote`
--
ALTER TABLE `detailgoodsissuednote`
  ADD CONSTRAINT `FK_DetailGoodsIssuedNote_GoodsIssuedNote` FOREIGN KEY (`id`) REFERENCES `goodsissuednote` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_DetailGoodsIssuedNote_Products` FOREIGN KEY (`ProductId`) REFERENCES `products` (`ProductId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `detailgoodsreceiptnote`
--
ALTER TABLE `detailgoodsreceiptnote`
  ADD CONSTRAINT `FK_DetailGoodsReceiptNote_DetailGoodOrder` FOREIGN KEY (`GoodsOrderId`,`ProductId`) REFERENCES `detailgoodorder` (`GoodsOrderId`, `ProductId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_DetailGoodsReceiptNote_GoodsReceiptNote` FOREIGN KEY (`id`) REFERENCES `goodsreceiptnote` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `goodsissuednote`
--
ALTER TABLE `goodsissuednote`
  ADD CONSTRAINT `fk_goodsissuednote_accounts1` FOREIGN KEY (`AccountId`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `goodsorder`
--
ALTER TABLE `goodsorder`
  ADD CONSTRAINT `FK_GoodsOrder_Accounts` FOREIGN KEY (`AccountId`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_GoodsOrder_Producers` FOREIGN KEY (`ProducerId`) REFERENCES `producers` (`ProducerId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `goodsreceiptnote`
--
ALTER TABLE `goodsreceiptnote`
  ADD CONSTRAINT `FK_GoodsReceiptNote_GoodsOrders` FOREIGN KEY (`GoodsOrderId`) REFERENCES `goodsorder` (`GoodsOrderId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `producers`
--
ALTER TABLE `producers`
  ADD CONSTRAINT `fk_producers_producersstatus1` FOREIGN KEY (`StatusId`) REFERENCES `producersstatus` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_Products_CategoryProducts` FOREIGN KEY (`CategoryProductId`) REFERENCES `categoryproducts` (`CategoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Products_Producers` FOREIGN KEY (`ProducerId`) REFERENCES `producers` (`ProducerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Products_TypeProduct` FOREIGN KEY (`ProductTypeId`) REFERENCES `typeproduct` (`TypeProductId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Products_UnitCalculate` FOREIGN KEY (`UnitCalculateId`) REFERENCES `unitcalculate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `typeproduct`
--
ALTER TABLE `typeproduct`
  ADD CONSTRAINT `FK_TypeProduct_CategoryProducts` FOREIGN KEY (`CategoryProductId`) REFERENCES `categoryproducts` (`CategoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
